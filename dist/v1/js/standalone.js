jQuery(document).on('isaMapLoaded', function() {
  var params   = jQuery.deparam.querystring();
  var fragment = jQuery.deparam.fragment();

  // Convert to new format
  if (jQuery.isEmptyObject(params) && !jQuery.isEmptyObject(fragment)) {
    params               = fragment;
    window.location.hash = '';
  }

  jQuery('#map').isaMap(params);
});
