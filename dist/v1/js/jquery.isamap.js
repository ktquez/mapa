(function ($) {
  'use strict';

  /*
  let notice = `
  %c  ╦╔═╗╔═╗    %cjquery.isamap: A GIS plugin from Programa de Monitoramento.
  %c  ║╚═╗╠═╣    %c©2017 Instituto Socioambiental.
  %c  ╩╚═╝╩ ╩    %chttps://www.socioambiental.org
  `;

  window.console && console.log(notice, "color:#b81d00", "color:inherit", "color:#b81d00", "color:inherit", "color:#b81d00", "color:inherit");
  */

  // Global parameters
  var majorVersion      = '1';
  var apiBasicAuth      = 'mapa:WQxilp83J/9AQA==';
  var apiKey            = 'Qqd04sojeXEpivTvLtUhndDvRC8wZ6D7';
  var maxArpsWithLimits = 25;
  var instances         = {};
  var ufs               = {};
  var geocode           = {};
  var apiDataLoadStatus = { 'ucs': false, 'tis': false, 'ufs': false };
  var arps              = { 'ucs': {}, 'tis': {} };
  var arpsByProperty    = { 'ucs': {}, 'tis': {} };
  var arpsProperties    = {
    'tis': [
      'categoria',
      'funai',
      'funasa',
      'grupo_situacao_juridica',
      'jurisdicao_legal',
      'uf',
      'presenca_isolados',
      'area',
      'populacao',
      'faixa_fronteira',
      'total_povos_residentes',
      // Unused
      //'situacao_juridica',
    ],
    'ucs': [
      'categoria',
      'instancia_responsavel',
      'jurisdicao_legal',
    ],
  };

  // blockUI stylesheet
  var blockUIStyle = {
    'font-size'       : '18px',
    'border'          : 'none',
    'backgroundColor' : 'rgb(255, 255, 255) transparent',
  };

  // Default settings
  var defaultSettings = {
    lang                   : 'pt-br',
    about                  : true,
    arps                   : null,
    map                    : null,
    proxyUrl               : null,
    center                 : [ -15, -55 ],
    zoom                   : 5,
    minZoom                : 4,
    maxZoom                : 15,
    maxBounds              : [ [-35, -98], [15, -14] ],
    expandedGroups         : [ ],
    groupExcluded          : [ ],
    layers                 : [ 'tis.limits', 'ucs.limitsEstaduais', 'ucs.limitsFederais', 'jurisdicao.amlegal' ],
    layerExcluded          : [ ],
    layerDefinitions       : null,
    layerDefinitionsExtend : false,
    baseLayer              : 'base.topographic',

    // We can use either the API Gateway or the direct Geo URL; for the first case we have to ensure
    // the apiKey is sent using the '//token' param at each layer definition; the API Gateway endpoint
    // must accept '//token' as a valid auth param.
    //
    // Right now using the API Gateway is not working as L.esri still access some URLs not using the
    // token parameter, such as /query endpoints. A more robust sollution would involve a public
    // Geo Proxy hosted somewhere like https://mapa.eco.br/proxy or fix L.esri.
    //
    // If you want to use the direct Geo Service, you'll have to keep commented all '//token' params from
    // layer definitions.
    //gisServer            : 'https://api.socioambiental.org/geo/v1/webadaptor1/rest/services/',
    gisServer              : 'https://geo.socioambiental.org/webadaptor1/rest/services/',

    baseUrl                : 'https://mapa.eco.br/',        //'https://mapa.socioambiental.org/',
    imagesUrl              : 'https://mapa.eco.br/images/', //'https://mapa.socioambiental.org/images/', // dev: 'https://gitlab.com/socioambiental/jquery.isamap/raw/feature/leaflet/images/'
    geocodeUrl             : 'https://maps.googleapis.com/maps/api/geocode/json?address=',
    searchControl          : true,
    embedControl           : true,
    techNoteControl        : true,
    hashChange             : true,
    urlChange              : true,
    debug                  : false,
    logo                   : null,
    logo_width             : null,
    logo_href              : null,
    overlayCss             : {
      'font-size': '18px',
    },
  }

  // Localized messages
  var messages = {
    'pt-br': {
      'attribution'                          : 'Instituto Socioambiental - ISA',
      'about'                                : 'Sobre',
      'basemap'                              : 'Mapa Base',
      'topographic'                          : 'Topográfico',
      'communitary'                          : 'Comunitário',
      'loading'                              : 'Carregando dados...',
      'indigenous_lands'                     : 'Terras Indígenas',
      'conservation_areas'                   : 'Unidades de Conservação',
      'points'                               : ' : pontos',
      'limits'                               : ' : contornos',
      'humanitary'                           : 'Humanitário',
      'satelitte'                            : 'Satélite',
      'imagery'                              : 'Imagens',
      'gray'                                 : 'Cinza',
      'darkgray'                             : 'Cinza escuro',
      'state'                                : 'Estaduais',
      'federal'                              : 'Federais',
      'infrastructure'                       : 'Infraestrutura',
      'energy'                               : 'Energia',
      'mining'                               : 'Mineração',
      'petroleum'                            : 'Óleo e gás',
      'jurisdiction'                         : 'Jurisdição',
      'amlegal'                              : 'Amazônia Legal',
      'environment'                          : 'Ambiente',
      'biomes'                               : 'Biomas',
      'caves'                                : 'Cavernas',
      'vegetation'                           : 'Vegetação',
      'watersheds'                           : 'Bacias ANA (Níveis 1 e 2)',
      'otto'                                 : 'Otto Bacias (Níveis 1 a 3)',
      'fire'                                 : 'Fogo (últ. 24 horas)',
      'fires'                                : 'Queimadas',
      'fire_density'                         : 'Fogo (últ. 24 horas) (mancha)',
      'deforestation'                        : 'Desmatamento',
      'streets'                              : 'Ruas',
      'hybrid'                               : 'Híbrido',
      'terrain'                              : 'Terreno',
      'cus'                                  : 'UCs',
      'mosaics'                              : 'Mosaicos e outros',
      'mosaics_corridors'                    : 'Mosaicos e Corredores',
      'mosaic'                               : 'Mosaico',
      'corridor'                             : 'Corredor',
      'until'                                : 'até',
      'search'                               : 'Buscar Áreas Protegidas',
      'search_categories'                    : 'Digite nomes, categorias, UFs...',
      'no_results'                           : 'Nenhum resultado encontrado',
      'no_info'                              : 'Sem informação',
      'embed_oembed'                         : 'oEmbed',
      'embed_code'                           : 'Incorporar',
      'embed_url'                            : 'Endereço deste mapa',
      'share'                                : 'Compartilhar',
      'share_text_title'                     : 'Mapa compartilhado',
      'mining_interest'                      : 'Interesse em pesquisar',
      'mining_availability'                  : 'Em pesquisa / disponibilidade',
      'mining_request'                       : 'Solicitação de extração',
      'mining_authorized'                    : 'Autorização para extração',
      'petroleum_dev'                        : 'Desenvolvimento',
      'petroleum_prod'                       : 'Produção',
      'petroleum_exp'                        : 'Exploração',
      'petroleum_lic'                        : 'Em licitação',
      'biomes_amazon'                        : 'Amazônia',
      'biomes_caatinga'                      : 'Caatinga',
      'biomes_cerrado'                       : 'Cerrado',
      'biomes_atlantic'                      : 'Mata Atlântica',
      'biomes_pampa'                         : 'Pampa',
      'biomes_wetland'                       : 'Pantanal',
      'vegetation_campinarana'               : 'Campinarana',
      'vegetation_contact'                   : 'Contato entre tipos vegetacionais',
      'vegetation_veld'                      : 'Estepe',
      'vegetation_decidual'                  : 'Floresta estacional decidual',
      'vegetation_semidecidual'              : 'Floresta estacional semi-decidual',
      'vegetation_open_ombrophylous'         : 'Floresta ombrófila aberta',
      'vegetation_dense_ombrophylous'        : 'Floresta ombrófila densa',
      'vegetation_mixed_ombrophylous'        : 'Floresta ombrófila mista',
      'vegetation_pioneer_formations'        : 'Formações pioneiras',
      'vegetation_ecological_refugees'       : 'Refúgio ecológico',
      'vegetation_savanna'                   : 'Savana',
      'vegetation_veldt_savanna'             : 'Savana estépica',
      'basin_amazon'                         : 'Amazônica',
      'basin_araguaia_tocantins'             : 'Araguaia / Tocantins',
      'basin_east_atlantic'                  : 'Atlântico Leste',
      'basin_western_northeast'              : 'Atlântico Nordeste Ocidental',
      'basin_atlantic_southeast'             : 'Atlântico Sudeste',
      'basin_south_atlantico'                : 'Atlântico Sul',
      'basin_paraguay'                       : 'Paraguai',
      'basin_parana'                         : 'Paraná',
      'basin_parnaiba'                       : 'Parnaíba',
      'basin_sao_francisco'                  : 'São Francisco',
      'basin_uruguay'                        : 'Uruguai',
      'tis.categoria'                        : 'Terras Indígenas - Categoria',
      'tis.faixa_fronteira'                  : 'Terras Indígenas - Faixa de fronteira',
      'tis.funai'                            : 'Terras Indígenas - FUNAI',
      'tis.funasa'                           : 'Terras Indígenas - SESAI',
      'tis.grupo_situacao_juridica'          : 'Terras Indígenas - Situação Jurídica',
      'tis.situacao_juridica'                : 'Terras Indígenas - Situação Jurídica',
      'tis.jurisdicao_legal'                 : 'Terras Indígenas - Jurisdição Legal',
      'tis.municipio'                        : 'Terras Indígenas - Município',
      'tis.presenca_isolados'                : 'Terras Indígenas - Presença de Isolados',
      'tis.uf'                               : 'Terras Indígenas - por Estado (UF)',
      'tis.total_povos_residentes'           : 'Terras Indígenas - total de Povos residentes',
      'tis.populacao'                        : 'Terras Indígenas - população',
      'tis.area'                             : 'Terras Indígenas - por área',
      'tis.identificacao'                    : 'Em identificação',
      'tis.identificada'                     : 'Identificada',
      'tis.declarada'                        : 'Declarada',
      'tis.homologada'                       : 'Homologada',
      'tis.reservada'                        : 'Reservada',
      'tis.restricao'                        : 'Com restrição de uso a não índios',
      //'tis.marker.funasa'                    : 'Funasa',
      //'tis.marker.funai'                     : 'Funai',
      //'tis.marker.presenca_isolados'         : 'Presença de isolados',
      //'tis.marker.faixa_fronteira'           : 'Faixa de fronteira',
      //'tis.marker.pretenssao_mineraria'      : 'Pretensão minerária',
      //'tis.marker.categoria'                 : 'Categoria',
      'tis.marker.jurisdicao_legal'          : 'Jurisdição Legal',
      'tis.marker.populacao_arp'             : 'População',
      'tis.marker.grupo_situacao_juridica'   : 'Situação jurídica',
      'tis.marker.area_oficial'              : 'Área oficial (ha)',
      //'tis.marker.total_povos_residentes'    : 'Total de povos residentes',
      'ucs.categoria'                        : 'Unidades de Conservação - Categoria',
      'ucs.instancia_responsavel'            : 'Unidades de Conservação - Instância Responsável',
      'ucs.jurisdicao_legal'                 : 'Unidades de Conservação - Jurisdição Legal',
      'ucs.marker.nome_completo'             : 'Nome', // API SisARP v0
      'ucs.marker.nome_arp'                  : 'Nome',
      'ucs.marker.descricao_categoria'       : 'Categoria',
      'ucs.marker.instancia_responsavel'     : 'Instância Responsável',
      'ucs.marker.jurisdicao_legal'          : 'Jurisdição Legal',
      'population'                           : {
           0                                 : 'sem informação',
          25                                 : 'de    1 a   25',
          50                                 : 'de   26 a   50',
         100                                 : 'de   51 a  100',
         200                                 : 'de  101 a  200',
         500                                 : 'de  201 a  500',
        1000                                 : 'de  501 a 1000',
        5000                                 : 'de 1001 a 5000',
        5001                                 : 'mais de   5001',
      },
      'area'                                 : {
              0                              : 'sem informação',
          10000                              : 'até  10 mil ha',
          50000                              : 'de   10.001 a  50 mil ha',
         250000                              : 'de   50.001 a 250 mil ha',
         500000                              : 'de  250.001 a 500 mil ha',
        1500000                              : 'de  500.001 a 1.5 milhão de ha',
        1500001                              : 'mais de       1.5 milhão de ha',
      },
    },
    'en': {
      'attribution'                          : 'Instituto Socioambiental - ISA',
      'about'                                : 'About',
      'basemap'                              : 'Base Map',
      'topographic'                          : 'Topographic',
      'communitary'                          : 'Communitary',
      'loading'                              : 'Loading data...',
      'indigenous_lands'                     : 'Indigenous Lands',
      'conservation_areas'                   : 'Conservation Areas',
      'points'                               : ' : points',
      'limits'                               : ' : limits',
      'humanitary'                           : 'Humanitary',
      'satelitte'                            : 'Satelitte',
      'imagery'                              : 'Imagery',
      'gray'                                 : 'Gray',
      'darkgray'                             : 'Dark gray',
      'state'                                : 'State',
      'federal'                              : 'Federal',
      'infrastructure'                       : 'Infrastructure',
      'energy'                               : 'Energy',
      'mining'                               : 'Mining',
      'petroleum'                            : 'Oil and gas',
      'jurisdiction'                         : 'Jurisdiction',
      'amlegal'                              : 'Legal Amazon',
      'environment'                          : 'Environment',
      'biomes'                               : 'Biomes',
      'caves'                                : 'Caves',
      'vegetation'                           : 'Vegetation',
      'watersheds'                           : 'Watersheds ANA (Levels 1 and 2)',
      'otto'                                 : 'Watersheds Otto (Levels 1 to 3)',
      'fire'                                 : 'Fire (last 24 hours)',
      'fires'                                : 'Fire',
      'fire_density'                         : 'Fire (last 24 hours) (density)',
      'deforestation'                        : 'Deforestation',
      'streets'                              : 'Streets',
      'hybrid'                               : 'Hybrid',
      'terrain'                              : 'Terrain',
      'cus'                                  : 'CUs',
      'mosaics'                              : 'Mosaics and others',
      'mosaics_corridors'                    : 'Mosaics and Corridors',
      'mosaic'                               : 'Mosaic',
      'corridor'                             : 'Corridor',
      'until'                                : 'until',
      'search'                               : 'Search Protected Areas',
      'search_categories'                    : 'Type names, categories, states...',
      'no_results'                           : 'No results found',
      'no_info'                              : 'No information',
      'share'                                : 'Share',
      'share_text_title'                     : 'Shared map',
      'embed_oembed'                         : 'oEmbed',
      'embed_code'                           : 'Embed code',
      'embed_url'                            : 'This map address',
      'mining_interest'                      : 'Interest in research',
      'mining_availability'                  : 'Under research or availability',
      'mining_request'                       : 'Extraction requests',
      'mining_authorized'                    : 'Extraction authorized',
      'petroleum_dev'                        : 'Development',
      'petroleum_prod'                       : 'Production',
      'petroleum_exp'                        : 'Exploration',
      'petroleum_lic'                        : 'Under auction',
      'biomes_amazon'                        : 'Amazon',
      'biomes_caatinga'                      : 'Caatinga',
      'biomes_cerrado'                       : 'Cerrado (Brazilian Savanna)',
      'biomes_atlantic'                      : 'Atlantic Forest',
      'biomes_pampa'                         : 'Pampa',
      'biomes_wetland'                       : 'Wetland',
      'vegetation_campinarana'               : 'Campinarana',
      'vegetation_contact'                   : 'Contact between vegetation types',
      'vegetation_veld'                      : 'Veld',
      'vegetation_decidual'                  : 'Floresta estacional decidual',
      'vegetation_semidecidual'              : 'Floresta estacional semi-decidual',
      'vegetation_open_ombrophylous'         : 'Floresta ombrófila aberta',
      'vegetation_dense_ombrophylous'        : 'Floresta ombrófila densa',
      'vegetation_mixed_ombrophylous'        : 'Floresta ombrófila mista',
      'vegetation_pioneer_formations'        : 'Pioneer formations',
      'vegetation_ecological_refugees'       : 'Ecological refugee',
      'vegetation_savanna'                   : 'Savanna',
      'vegetation_veldt_savanna'             : 'Veldt savanna',
      'basin_amazon'                         : 'Amazon',
      'basin_araguaia_tocantins'             : 'Araguaia / Tocantins',
      'basin_east_atlantic'                  : 'East Atlantic',
      'basin_western_northeast'              : 'Western Northeast Atlantic',
      'basin_atlantic_southeast'             : 'Atlantic Southeast',
      'basin_south_atlantico'                : 'South Atlantic',
      'basin_paraguay'                       : 'Paraguai',
      'basin_parana'                         : 'Paraná',
      'basin_parnaiba'                       : 'Parnaíba',
      'basin_sao_francisco'                  : 'São Francisco',
      'basin_uruguay'                        : 'Uruguay',
      'tis.categoria'                        : 'Indigenous Lands - Categories',
      'tis.faixa_fronteira'                  : 'Indigenous Lands - Along borders',
      'tis.funai'                            : 'Indigenous Lands - FUNAI',
      'tis.funasa'                           : 'Indigenous Lands - SESAI',
      'tis.grupo_situacao_juridica'          : 'Indigenous Lands - Juridical Status',
      'tis.situacao_juridica'                : 'Indigenous Lands - Juridical Status',
      'tis.jurisdicao_legal'                 : 'Indigenous Lands - Legal Jurisdiction',
      'tis.municipio'                        : 'Indigenous Lands - Municipalities',
      'tis.presenca_isolados'                : 'Indigenous Lands - Isolated',
      'tis.uf'                               : 'Indigenous Lands - by State (UF)',
      'tis.total_povos_residentes'           : 'Indigenous Lands - total living Peoples',
      'tis.populacao'                        : 'Indigenous Lands - population',
      'tis.area'                             : 'Indigenous Lands - by area',
      'tis.identificacao'                    : 'In identification',
      'tis.identificada'                     : 'Identified',
      'tis.declarada'                        : 'Declared',
      'tis.homologada'                       : 'Ratified',
      'tis.reservada'                        : 'Reserved',
      'tis.restricao'                        : 'With usage restriction to non-indigenous',
      //'tis.marker.funasa'                    : 'Funasa',
      //'tis.marker.funai'                     : 'Funai',
      //'tis.marker.presenca_isolados'         : 'Has isolated peoples',
      //'tis.marker.faixa_fronteira'           : 'Along borders',
      //'tis.marker.pretenssao_mineraria'      : 'Mining Applications',
      //'tis.marker.categoria'                 : 'Category',
      'tis.marker.jurisdicao_legal'          : 'Legal Jurisdiction',
      'tis.marker.populacao_arp'             : 'Population',
      'tis.marker.grupo_situacao_juridica'   : 'Legal Situation',
      'tis.marker.area_oficial'              : 'Official Area (ha)',
      //'tis.marker.total_povos_residentes'    : 'Total living peoples',
      'ucs.categoria'                        : 'Conservation Units - Category',
      'ucs.instancia_responsavel'            : 'Conservation Units - Responsible Instance',
      'ucs.jurisdicao_legal'                 : 'Conservation Units - Legal Jurisdiction',
      'ucs.marker.nome_completo'             : 'Name', // API SisARP v0
      'ucs.marker.nome_arp'                  : 'Name',
      'ucs.marker.descricao_categoria'       : 'Category',
      'ucs.marker.instancia_responsavel'     : 'Responsible instance',
      'ucs.marker.jurisdicao_legal'          : 'Legal jurisdiction',
      'population'                           : {
           0                                 : 'no information',
          25                                 : 'from    1 to 25',
          50                                 : 'from   26 to 50',
         100                                 : 'from   51 to 100',
         200                                 : 'from  101 to 200',
         500                                 : 'from  201 to 500',
        1000                                 : 'from  501 to 1000',
        5000                                 : 'from 1001 to 5000',
        5001                                 : 'more than    5001',
      },
      'area'                                 : {
              0                              : 'no information',
          10000                              : 'up to 10k ha',
          50000                              : 'from   10,001 to  50k ha',
         250000                              : 'from   50,001 to 250k ha',
         500000                              : 'from  250,001 to 500k ha',
        1500000                              : 'from  500,001 to 1,5 million ha',
        1500001                              : 'more than        1,5 million ha',
      },
    },
    'es': {
      'attribution'                          : 'Instituto Socioambiental - ISA',
      'about'                                : 'Sobre',
      'basemap'                              : 'Mapa Base',
      'topographic'                          : 'Topográfico',
      'communitary'                          : 'Comunitario',
      'loading'                              : 'Cargando datos...',
      'indigenous_lands'                     : 'Tierras Indigenas',
      'conservation_areas'                   : 'Unidades de Conservación',
      'points'                               : ' : puntos',
      'limits'                               : ' : contornos',
      'humanitary'                           : 'Humanitario',
      'satelitte'                            : 'Satélite',
      'imagery'                              : 'Imageria',
      'gray'                                 : 'Cinza',
      'darkgray'                             : 'Cinza escuro',
      'state'                                : 'De los Estados',
      'federal'                              : 'Federales',
      'infrastructure'                       : 'Infraestructura',
      'energy'                               : 'Energía',
      'mining'                               : 'Mineración',
      'petroleum'                            : 'Oleo y gas',
      'jurisdiction'                         : 'Jurisdición',
      'amlegal'                              : 'Amazonia Legal',
      'environment'                          : 'Ambiente',
      'biomes'                               : 'Biomass',
      'caves'                                : 'Cuevas',
      'vegetation'                           : 'Vegetación',
      'watersheds'                           : 'Cuencas ANA (Niveis 1 e 2)',
      'otto'                                 : 'Cuencas Otto (Niveis 1 a 3)',
      'fire'                                 : 'Fogo (ult. 24 horas)',
      'fires'                                : 'Fogo',
      'fire_density'                         : 'Fogo (ult. 24 horas) (densidad)',
      'deforestation'                        : 'Deforestación',
      'streets'                              : 'Calles',
      'hybrid'                               : 'Hibrido',
      'terrain'                              : 'Terreno',
      'cus'                                  : 'UCs',
      'mosaics'                              : 'Mosaicos y otros',
      'mosaics_corridors'                    : 'Mosaicos y Corridores',
      'mosaic'                               : 'Mosaico',
      'corridor'                             : 'Corredor',
      'until'                                : 'hasta',
      'search'                               : 'Buscar Areas Protegidas',
      'search_categories'                    : 'Escriba nombres, categorias, Estados...',
      'no_results'                           : 'Ningún resultado encontrado',
      'no_info'                              : 'Sin información',
      'embed_oembed'                         : 'oEmbed',
      'embed_code'                           : 'Incorporar',
      'embed_url'                            : 'Dirección deste mapa',
      'share'                                : 'Compartir',
      'share_text_title'                     : 'Mapa compartido',
      'mining_interest'                      : 'Interesse en búsqueda',
      'mining_availability'                  : 'En búsqueda / disponibilidad',
      'mining_request'                       : 'Solicitación de extración',
      'mining_authorized'                    : 'Autorización para extración',
      'petroleum_dev'                        : 'Desarollo',
      'petroleum_prod'                       : 'Produción',
      'petroleum_exp'                        : 'Exploración',
      'petroleum_lic'                        : 'En licitación',
      'biomes_amazon'                        : 'Amazonia',
      'biomes_caatinga'                      : 'Caatinga',
      'biomes_cerrado'                       : 'Cerrado',
      'biomes_atlantic'                      : 'Mata Atlantica',
      'biomes_pampa'                         : 'Pampa',
      'biomes_wetland'                       : 'Pantanal',
      'vegetation_campinarana'               : 'Campinarana',
      'vegetation_contact'                   : 'Contacto entre tipos vegetacionais',
      'vegetation_veld'                      : 'Estepa',
      'vegetation_decidual'                  : 'Floresta estacional decidual',
      'vegetation_semidecidual'              : 'Floresta estacional semi-decidual',
      'vegetation_open_ombrophylous'         : 'Floresta ombrófila abierta',
      'vegetation_dense_ombrophylous'        : 'Floresta ombrófila densa',
      'vegetation_mixed_ombrophylous'        : 'Floresta ombrófila mista',
      'vegetation_pioneer_formations'        : 'Formaciones pioneras',
      'vegetation_ecological_refugees'       : 'Refugio ecologico',
      'vegetation_savanna'                   : 'Savana',
      'vegetation_veldt_savanna'             : 'Savana estepica',
      'basin_amazon'                         : 'Amazonica',
      'basin_araguaia_tocantins'             : 'Araguaia / Tocantins',
      'basin_east_atlantic'                  : 'Atlantico Leste',
      'basin_western_northeast'              : 'Atlantico Nordeste Ocidental',
      'basin_atlantic_southeast'             : 'Atlantico Sudeste',
      'basin_south_atlantico'                : 'Atlantico Sul',
      'basin_paraguay'                       : 'Paraguai',
      'basin_parana'                         : 'Paraná',
      'basin_parnaiba'                       : 'Parnaíba',
      'basin_sao_francisco'                  : 'São Francisco',
      'basin_uruguay'                        : 'Uruguai',
      'tis.categoria'                        : 'Tierras Indigenas - Categoria',
      'tis.faixa_fronteira'                  : 'Tierras Indigenas - De la frontera',
      'tis.funai'                            : 'Tierras Indigenas - FUNAI',
      'tis.funasa'                           : 'Tierras Indigenas - SESAI',
      'tis.grupo_situacao_juridica'          : 'Tierras Indigenas - Situación Juridica',
      'tis.situacao_juridica'                : 'Tierras Indigenas - Situación Juridica',
      'tis.jurisdicao_legal'                 : 'Tierras Indigenas - Jurisdición Legal',
      'tis.municipio'                        : 'Tierras Indigenas - Municipio',
      'tis.presenca_isolados'                : 'Tierras Indigenas - Presecia de Aislados',
      'tis.uf'                               : 'Tierras Indigenas - por Estado (UF)',
      'tis.total_povos_residentes'           : 'Tierras Indigenas - total de Pueblos residentes',
      'tis.populacao'                        : 'Tierras Indigenas - populación',
      'tis.area'                             : 'Tierras Indigenas - por area',
      'tis.identificacao'                    : 'En identificación',
      'tis.identificada'                     : 'Identificada',
      'tis.declarada'                        : 'Declarada',
      'tis.homologada'                       : 'Homologada',
      'tis.reservada'                        : 'Reservada',
      'tis.restricao'                        : 'Con restrición de uso a non-indígenas',
      //'tis.marker.funasa'                    : 'Funasa',
      //'tis.marker.funai'                     : 'Funai',
      //'tis.marker.presenca_isolados'         : 'Presencia de Aislados',
      //'tis.marker.faixa_fronteira'           : 'Faixa de fronteira',
      //'tis.marker.pretenssao_mineraria'      : 'Pretención mineraria',
      //'tis.marker.categoria'                 : 'Categoria',
      'tis.marker.jurisdicao_legal'          : 'Jurisdición Legal',
      'tis.marker.populacao_arp'             : 'Populación',
      'tis.marker.grupo_situacao_juridica'   : 'Situación juridica',
      'tis.marker.area_oficial'              : 'Área oficial (ha)',
      //'tis.marker.total_povos_residentes'    : 'Total de povos residentes',
      'ucs.categoria'                        : 'Unidades de Conservación - Categoria',
      'ucs.instancia_responsavel'            : 'Unidades de Conservación - Instancia Responsable',
      'ucs.jurisdicao_legal'                 : 'Unidades de Conservación - Jurisdición Legal',
      'ucs.marker.nome_completo'             : 'Nome', // API SisARP v0
      'ucs.marker.nome_arp'                  : 'Nome',
      'ucs.marker.descricao_categoria'       : 'Categoria',
      'ucs.marker.instancia_responsavel'     : 'Instancia Responsable',
      'ucs.marker.jurisdicao_legal'          : 'Jurisdición Legal',
      'population'                           : {
           0                                 : 'sin información',
          25                                 : 'de    1 a   25',
          50                                 : 'de   26 a   50',
         100                                 : 'de   51 a  100',
         200                                 : 'de  101 a  200',
         500                                 : 'de  201 a  500',
        1000                                 : 'de  501 a 1000',
        5000                                 : 'de 1001 a 5000',
        5001                                 : 'mais de   5001',
      },
      'area'                                 : {
              0                              : 'sin información',
          10000                              : 'até  10 mil ha',
          50000                              : 'de   10.001 a  50 mil ha',
         250000                              : 'de   50.001 a 250 mil ha',
         500000                              : 'de  250.001 a 500 mil ha',
        1500000                              : 'de  500.001 a 1.5 millón de ha',
        1500001                              : 'mas de        1.5 millón de ha',
      },
    },
  }

  function mapLoadError(info) {
    var message = 'Error loading map';
    message     = info != '' ? message + ': ' + info : message;

    jQuery('.isamap').unblock();
    jQuery('.isamap').block({
      message : '<span>'+ message + '</span>',
      css     : blockUIStyle,
    });
  }

  /**
   * Load an isaMap instance, making sure needed resources are available.
   */
  function loadMetadata() {
    $(document).on('isaMapLoadedMetadata', function() {
      // Check if we have all the needed resources.
      //if (Object.getOwnPropertyNames(arps.tis).length > 0 && Object.getOwnPropertyNames(arps.ucs).length > 0 && Object.getOwnPropertyNames(ufs).length > 0) {
      if (apiDataLoadStatus.ufs == true && apiDataLoadStatus.tis == true && apiDataLoadStatus.ucs == true) {
        jQuery('.isamap').unblock();
        $(document).trigger('isaMapLoaded');
      }
    });

    // Make sure that ARP metadata is available
    getUfMetadata();
    getIndineousLandsMetadata();
    getConservationAreasMetadata();
  }

  /**
   * Retrieve UF Metadata.
   */
  function getUfMetadata() {
    if (Object.getOwnPropertyNames(ufs).length > 0) {
      $(document).trigger('isaMapLoadedMetadata');
      return;
    }

    jQuery.ajax({
      //xhrFields: {
      //  withCredentials: true,
      //},
      //headers: {
      //  'Authorization': 'Basic ' + btoa(apiBasicAuth),
      //},
      //data: {
      //  'tipo': 'uf',
      //},
      //url: "https://api.socioambiental.org/sisarp/v1/listbox.json",
      url: defaultSettings.baseUrl + "/data/sisarp/v1/ufs.json",
      error: function() {
        mapLoadError('cannot get UF metadata.');
      },
    }).done(function(result) {
      for (var uf in result.listbox_uf) {
        ufs[result.listbox_uf[uf].descricao_uf] = result.listbox_uf[uf].nome_uf;
      }

      apiDataLoadStatus.ufs = true;
      $(document).trigger('isaMapLoadedMetadata');
    });
  }

  /**
   * Retrieve Indigenous Lands Metadata from SisARP API.
   */
  function getIndineousLandsMetadata() {
    if (Object.getOwnPropertyNames(arps.tis).length > 0) {
      $(document).trigger('isaMapLoadedMetadata');
      return;
    }

    jQuery.ajax({
      //xhrFields: {
      //  withCredentials: true,
      //},
      //headers: {
      //  'Authorization': 'Basic ' + btoa(apiBasicAuth),
      //},
      //url: "https://api.socioambiental.org/sisarp/v1/terra_indigena.json",
      url: defaultSettings.baseUrl + "/data/sisarp/v1/tis.json",
      error: function() {
        mapLoadError('cannot get Indigenous Lands metadata.');
      },
    }).done(function(result) {
      for (var i = 0; i < result.content.info_geral.length; i++) {
        var id = result.content.info_geral[i].id;

        // Exclude unplotted TIs
        if (result.content.info_geral[i].plotagem == 'Não') {
          continue;
        }

        arps.tis[id] = result.content.info_geral[i];

        // Build properties
        for (var property in arpsProperties.tis) {
          var currentProperty = arpsProperties.tis[property];

          if (arpsByProperty.tis[currentProperty] == undefined) {
            arpsByProperty.tis[currentProperty] = {};
          }

          // Exeption handling
          if (currentProperty == 'uf') {
            for (var municipality in arps.tis[id].municipio) {
              var uf = arps.tis[id].municipio[municipality].uf;

              if (arpsByProperty.tis[currentProperty][uf] == undefined) {
                arpsByProperty.tis[currentProperty][uf] = [];
              }

              arpsByProperty.tis[currentProperty][uf].push(id);
            }
          }
          if (currentProperty == 'populacao') {
            var n = arps.tis[id].populacao_arp;

            if (n == 0 || n == null) {
              var range = 0;
            }
            else if (n <= 25) {
              var range = 25;
            }
            else if (n >= 26 && n <= 50) {
              var range = 50;
            }
            else if (n >= 51 && n <= 100) {
              var range = 100;
            }
            else if (n >= 101 && n <= 200) {
              var range = 200;
            }
            else if (n >= 201 && n <= 500) {
              var range = 500;
            }
            else if (n >= 501 && n <= 1000) {
              var range = 1000;
            }
            else if (n >= 1001 && n <= 5000) {
              var range = 5000;
            }
            else if (n >= 5001) {
              var range = 5001;
            }

            if (arpsByProperty.tis[currentProperty][range] == undefined) {
              arpsByProperty.tis[currentProperty][range] = [];
            }

            arpsByProperty.tis[currentProperty][range].push(id);
          }
          if (currentProperty == 'area') {
            var n = arps.tis[id].area_oficial;

            if (n == 0 || n == null) {
              var range = 0;
            }
            else if (n <= 10000) {
              var range = 10000;
            }
            else if (n >= 10001 && n <= 50000) {
              var range = 50000;
            }
            else if (n >=  50001 && n <= 250000) {
              var range = 250000;
            }
            else if (n >= 250001 && n <= 500000) {
              var range = 500000;
            }
            else if (n >=  500001 && n <= 1500000) {
              var range = 1500000;
            }
            else if (n >= 1500001) {
              var range = 1500001;
            }

            if (arpsByProperty.tis[currentProperty][range] == undefined) {
              arpsByProperty.tis[currentProperty][range] = [];
            }

            arpsByProperty.tis[currentProperty][range].push(id);
          }
          else if (arps.tis[id][currentProperty] == undefined || arps.tis[id][currentProperty] == null) {
            continue;
          }
          else {
            // Default case
            if (arpsByProperty.tis[currentProperty][arps.tis[id][currentProperty]] == undefined) {
              arpsByProperty.tis[currentProperty][arps.tis[id][currentProperty]] = [];
            }

            arpsByProperty.tis[currentProperty][arps.tis[id][currentProperty]].push(id);
          }
        }
      }

      apiDataLoadStatus.tis = true;
      $(document).trigger('isaMapLoadedMetadata');
    });
  }

  /**
   * Retrieve Conservation Units Metadata from SisARP API.
   * Requires jquery.soap
   */
  function getConservationAreasMetadata() {
    if (Object.getOwnPropertyNames(arps.ucs).length > 0) {
      $(document).trigger('isaMapLoadedMetadata');
      return;
    }

    jQuery.ajax({
      // Get only confirmed UCs (status = 2)
      //url: "https://api.socioambiental.org/sisarp/v2/uc?status=2&apikey=" + apiKey,
      url: defaultSettings.baseUrl + "/data/sisarp/v2/ucs.json",
      error: function() {
        mapLoadError('cannot get Conservation Areas metadata.');
      },
    }).done(function(result) {
      //var assembly = result.data;
      var assembly = result.ucs;

      // Build an array with id_arp as index
      for (var i in assembly) {
        // Exclude unplotted UCs
        if (assembly[i].plotagem == 'Não') {
          continue;
        }

        var id       = assembly[i].id;
        arps.ucs[id] = assembly[i];

        // Build properties
        for (var property in arpsProperties.ucs) {
          var currentProperty = arpsProperties.ucs[property];

          if (arpsByProperty.ucs[currentProperty] == undefined) {
            arpsByProperty.ucs[currentProperty] = {};
          }

          if (arps.ucs[id][currentProperty] == undefined || arps.ucs[id][currentProperty] == null) {
            continue;
          }

          // Exception handling
          if (currentProperty == 'categoria' && arps.ucs[id].descricao_categoria != undefined) {
            var propertyName = arps.ucs[id][currentProperty] + ' - ' + arps.ucs[id].descricao_categoria;
          }
          else {
            var propertyName = arps.ucs[id][currentProperty];
          }

          if (arpsByProperty.ucs[currentProperty][propertyName] == undefined) {
            arpsByProperty.ucs[currentProperty][propertyName] = [];
          }

          arpsByProperty.ucs[currentProperty][propertyName].push(id);
        }
      }

      apiDataLoadStatus.ucs = true;
      $(document).trigger('isaMapLoadedMetadata');
    });

    //jQuery.soap({
    //  url:     'https://api.socioambiental.org/sisarp/v0/?wsdl',
    //  method:  'getListUc',
    //  timeout: 10000,

    //  data: {
    //    nome      : null,
    //    categoria : '!RPPN',
    //    uf        : null,
    //    status    : 'confirmado',
    //    inst_resp : '!municipal',
    //    grupo     : null,
    //    amlegal   : 0,
    //    snuc      : 1,
    //  },

    //  HTTPHeaders: {
    //    'Authorization': 'Basic ' + btoa(apiBasicAuth),
    //  },

    //  success: function (soapResponse) {
    //    var response = soapResponse.toJSON();
    //    var items    = response['#document']['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ns1:getListUcResponse'].return.item;
    //    var assembly = { };

    //    // Remove SOAP eloquence
    //    for (var i = 0; i < items.length; i++) {
    //      assembly[i] = { };

    //      for (var j in items[i].item) {
    //        assembly[i][items[i].item[j].key['#text']] = items[i].item[j].value['#text'];
    //      }
    //    }

    //    // Build an array with id_arp as index
    //    for (var i in assembly) {
    //      // Exclude unplotted UCs
    //      if (assembly[i].plotagem == 'Não') {
    //        continue;
    //      }

    //      var id       = assembly[i].id;
    //      arps.ucs[id] = assembly[i];

    //      // Build properties
    //      for (var property in arpsProperties.ucs) {
    //        var currentProperty = arpsProperties.ucs[property];

    //        if (arpsByProperty.ucs[currentProperty] == undefined) {
    //          arpsByProperty.ucs[currentProperty] = {};
    //        }

    //        if (arps.ucs[id][currentProperty] == undefined || arps.ucs[id][currentProperty] == null) {
    //          continue;
    //        }

    //        // Exception handling
    //        if (currentProperty == 'categoria' && arps.ucs[id].descricao_categoria != undefined) {
    //          var propertyName = arps.ucs[id][currentProperty] + ' - ' + arps.ucs[id].descricao_categoria;
    //        }
    //        else {
    //          var propertyName = arps.ucs[id][currentProperty];
    //        }

    //        if (arpsByProperty.ucs[currentProperty][propertyName] == undefined) {
    //          arpsByProperty.ucs[currentProperty][propertyName] = [];
    //        }

    //        arpsByProperty.ucs[currentProperty][propertyName].push(id);
    //      }
    //    }

    //    $(document).trigger('isaMapLoadedMetadata');
    //  },

    //  error: function (soapResponse) {
    //    console.debug(soapResponse);
    //  }
    //});

    return;
  }

  /**
   * Custom methods for ISA maps.
   */
  function myIsaMap(settings) {
    // Global parameters
    var self              = this;
    var controls          = null;
    var zoom              = null;
    var embedControl      = null;
    var searchControl     = null;
    var layers            = null;
    var encodedSettings   = '';
    var myMap             = {};
    var initialSettings   = jQuery.extend(true, {}, settings);;
    var mapCenter         = settings.center;
    var mapZoom           = settings.zoom;
    var minZoom           = settings.minZoom;
    var maxZoom           = settings.maxZoom;
    var maxBounds         = L.latLngBounds(settings.maxBounds);
    var blockUI           = [];
    var statusMessages    = [];
    var activeMapEntities = [];
    var trackingEvents    = {};
    //var mainLayerIds      = {};
    self.methods          = {};

    // Debug facility
    self.methods.debug = function(message) {
      if (settings.debug == true) {
        window.console && console.debug('[jquery.isamap] [#' + settings.mapId + ']', message);
      }
    }

    self.methods.message = function(key) {
      if (messages[settings.lang][key] != undefined) {
        return messages[settings.lang][key];
      }
    }

    self.methods.getSettings = function() {
      return settings;
    }

    self.methods.dumpSettings = function() {
      self.methods.debug(settings);
    }

    self.methods.getLayers = function() {
      return layers;
    }

    // Return locale coded used by Number.prototype.toLocaleString() depending on current language.
    //
    // See https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Number/toLocaleString
    //     https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Intl#Locale_identification_and_negotiation
    //     https://tools.ietf.org/html/rfc5646
    self.methods.getLocale = function() {
      if (settings.lang == 'pt-br') {
        return 'pt-BR';
      }

      return settings.lang;
    }

    self.methods.htmlTableRow = function(key, value) {
      // Format numbers
      if (!isNaN(value)) {
        value = Number(value);
        value = value.toLocaleString(self.methods.getLocale());
      }

      var content  = "<tr>";
      content     += "<td>" + key + ":</td><td> " + value + "  </td>";
      content     += "</tr>";

      return content;
    }

    self.methods.htmlTable = function(title, items) {
      var empty   = true;
      var content = '<div>';

      content += '<table class="table">';
      content += "<tr><th colspan=\"2\">" + title + "</th></tr>";

      for (var key in items) {
        if (items[key] != null) {
          empty    = false;
          content += self.methods.htmlTableRow(key, items[key]);
        }
      }

      content += '</table>';
      content += '</div></div>';

      if (empty == true) {
        return '';
      }

      return content;
    }

    self.methods.truncateString = function(string, maxLen) {
      if (string.length > maxLen) {
        return string.slice(0, maxLen) + '...';
      }
      else {
        return string;
      }
    }

    self.methods.blockUI = function(params) {
      if (jQuery.unblockUI == undefined) {
        return;
      }

      if (params.name == undefined) {
        params.name = 'default';
      }

      if (blockUI[params.name] == undefined || blockUI[params.name] != true) {
        blockUI[params.name] = true;

        if (settings.overlaySelector != undefined) {
          jQuery(settings.overlaySelector).block(params);
        }
        else {
          jQuery.blockUI(params);
        }
      }
    }

    self.methods.unblockUI = function(name) {
      if (jQuery.unblockUI == undefined) {
        return;
      }

      if (name == undefined) {
        name = 'default';
      }

      if (blockUI[name] != undefined && blockUI[name] == true) {
        delete blockUI[name];
      }

      if (blockUI.length == 0) {
        if (settings.overlaySelector != undefined) {
          jQuery(settings.overlaySelector).unblock();
        }
        else {
          jQuery.unblockUI();
        }
      }
    }

    self.methods.showOverlay = function(name) {
      if (jQuery.unblockUI == undefined) {
        return;
      }

      if (settings.disableOverlay != undefined && settings.disableOverlay == true) {
        return;
      }

      //if (jQuery.browser.msie === false || jQuery.browser.msie === undefined) {
        var css;
        var message = settings.overlayMessage;

        if (settings.overlayCss != undefined) {
          css = settings.overlayCss;
        }
        else {
          css = { };
        }

        var params = {
          name:    name,
          message: message,
          css:     css,
        };

        if (settings.overlayTimeout != undefined && settings.overlayTimeout != false) {
          params.timeout = timeout;
        }

        self.methods.blockUI(params);
      //}
    }

    self.methods.myMap = function() {
      return myMap;
    }

    self.methods.createMap = function() {
      self.methods.debug('Creating map #' + settings.mapId + '...');

      // Create the map object
      myMap = L.map(settings.mapId, {
        center            : mapCenter,
        zoom              : mapZoom,
        maxZoom           : maxZoom,
        maxBounds         : maxBounds,
        zoomControl       : false,
        fullscreenControl : {
          pseudoFullscreen: false,
        },
      });

      // Set baselayer
      if (settings.baseLayer != null) {
        var split = settings.baseLayer.split('.');

        self.methods.debug('Setting ' + settings.baseLayer + ' as base layer...');

        if (baseLayers[split[0]]['layers'][split[1]].layer != undefined) {
          myMap.addLayer(baseLayers[split[0]]['layers'][split[1]].layer);
        }
        else {
          baseLayers.base.layers['topographic'].layer.addTo(myMap);
        }
      }

      // Layer preprocessing
      for (var group in layers) {
        // Set slider IDs
        if (layers[group].slider) {
          layers[group].sliderId = group;

          // Set slider initial from value
          for (var layer in layers[group].layers) {
            if (settings.layers.indexOf(group + '.' + layer) != -1) {
              layers[group].slider.from = layer;
            }
          }
        }

        // Register events
        for (var layer in layers[group].layers) {
          if (layers[group].layers[layer].layer != undefined) {

            if (layers[group].layers[layer].click != undefined) {
              self.methods.debug('Registering click event for Layer ' + group + '.' + layer + '...');
              layers[group].layers[layer].layer.on('click', layers[group].layers[layer].click);
            }

            if (layers[group].layers[layer].dblclick != undefined) {
              self.methods.debug('Registering dblclick event for Layer ' + group + '.' + layer + '...');
              layers[group].layers[layer].layer.on('dblclick', layers[group].layers[layer].dblclick);
            }

            if (layers[group].layers[layer].mousedown != undefined) {
              self.methods.debug('Registering mousedown event for Layer ' + group + '.' + layer + '...');
              layers[group].layers[layer].layer.on('mousedown', layers[group].layers[layer].mousedown);
            }

            if (layers[group].layers[layer].mouseover != undefined) {
              self.methods.debug('Registering mouseover event for Layer ' + group + '.' + layer + '...');
              layers[group].layers[layer].layer.on('mouseover', layers[group].layers[layer].mouseover);
            }

            if (layers[group].layers[layer].mouseout != undefined) {
              self.methods.debug('Registering mouseout event for Layer ' + group + '.' + layer + '...');
              layers[group].layers[layer].layer.on('mouseout', layers[group].layers[layer].mouseout);
            }

            if (layers[group].layers[layer].popupopen != undefined) {
              self.methods.debug('Registering popupopen event for Layer ' + group + '.' + layer + '...');
              layers[group].layers[layer].layer.on('popupopen', layers[group].layers[layer].popupopen);
            }

            if (layers[group].layers[layer].popupclose != undefined) {
              self.methods.debug('Registering popupclose event for Layer ' + group + '.' + layer + '...');
              layers[group].layers[layer].layer.on('popupclose', layers[group].layers[layer].popupclose);
            }

            if (layers[group].layers[layer].contextmenu != undefined) {
              self.methods.debug('Registering contextmenu event for Layer ' + group + '.' + layer + '...');
              layers[group].layers[layer].layer.on('contextmenu', layers[group].layers[layer].contextmenu);
            }

            if (layers[group].layers[layer].bindPopup != undefined) {
              self.methods.debug('Binding a popup for Layer ' + group + '.' + layer + '...');
              layers[group].layers[layer].layer.bindPopup(layers[group].layers[layer].bindPopup);
            }

            // Block UI while loading for layers implementing the 'loading' event
            layers[group].layers[layer].layer.on('loading', function() {
              //self.methods.showOverlay(group + '.' + layer);
              self.methods.statusMessageLoading({ name: group + '.' + layer });
            });

            // Unblock UI when loaded for layers implementing the 'load' event
            layers[group].layers[layer].layer.on('load', function() {
              //self.methods.unblockUI(group + '.' + layer);
              self.methods.statusMessageReady(group + '.' + layer);
            });
          }
        }
      }

      // Add map controls
      controls = L.Control.styledLayerControl(
          baseLayers, layers,
          {
            collapsed:       true,
            container_width: '250px',
            position:        'topleft',
            group_maxHeight: '300px',
            debug:           settings.debug,
          }).addTo(myMap);

      // Pointer position
      L.control.mousePosition({
        position:    'bottomright',
        emptyString: 'Sem posição...',
        prefix:      'Coordenadas: ',
        separator:   ','
      }).addTo(myMap);

      // Adds scale ruller
      L.control.scale({
        position:       'bottomright',
        metric:         true,
        imperial:       false,
        updateWhenIdle: true
      }).addTo(myMap);

      // Zoom control
      zoom = L.control.zoom({
        position: 'bottomright',
      }).addTo(myMap);

      // Watermark control
      self.methods.watermarkControl();

      // Legend control
      self.methods.legendControl();

      // Status control
      self.methods.statusControl();

      // Technical note
      if (L.control.technicalnote != undefined) {
        if (settings.techNoteControl == true || settings.techNoteControl == 'true') {
          L.control.technicalnote({
            lang:     settings.lang,
            position: 'topright',
            collapsed: true,
          }).addTo(myMap);
        }
      }

      // Embed widget
      self.methods.embedCodeControl();

      // Identify features from all layers
      // See https://esri.github.io/esri-leaflet/examples/identifying-features.html
      myMap.on('click', function (e) {
        var content     = '';
        var hashContent = {};

        for (var group in layers) {
          for (var layer in layers[group].layers) {
            if (layers[group].layers[layer].layer != undefined) {
              if (layers[group].layers[layer].identify != undefined) {
                if (!myMap.hasLayer(layers[group].layers[layer].layer)) {
                  continue;
                }

                self.methods.debug('Identifying Layer ' + group + '/' + layer + '...');

                // A closure here preserves the current element
                (function(element) {
                  // Force identification only of the layer in question
                  // See https://developers.arcgis.com/rest/services-reference/identify-map-service-.htm
                  //     https://esri.github.io/esri-leaflet/api-reference/tasks/identify-features.html#options
                  var layers = 'visible:' + element.layer.options.layers.join(',');

                  element.layer.identify().on(myMap).at(e.latlng).layers(layers).run(function(error, featureCollection) {
                    var result = element.identify(error, featureCollection);

                    if (result != false) {
                      var hash = btoa(result);

                      // Workaround to avoid duplicated content if layers identify the same thing
                      if (hashContent[hash] == undefined) {
                        content           += result;
                        hashContent[hash]  = true;
                      }
                    }

                    if (content != undefined && content != 'undefined') {
                      self.methods.popup(e.latlng, content);
                    }
                  });
                })(layers[group].layers[layer]);
              }
            }
          }
        }
      });

      // Register events for state tracking
      self.methods.mapStateTracking();
    }

    self.methods.watermarkControl = function() {
      L.Control.Watermark = L.Control.extend({
        onAdd: function(map) {
          var div   = L.DomUtil.create('div');
          var link  = L.DomUtil.create('a',    '', div);
          var span  = L.DomUtil.create('span', '', div);
          var about = L.DomUtil.create('a',    '', div);
          var img   = L.DomUtil.create('img',  '', link);

          img.src         = settings.logo       != null ? settings.logo       : settings.imagesUrl + '/logos/microisa.png';
          img.style.width = settings.logo_width != null ? settings.logo_width : '94px';
          link.href       = settings.logo_href  != null ? settings.logo_href  : 'https://www.socioambiental.org/';

          // Target blank is used mainly for iframe compatibility
          link.setAttribute('target', '_blank');

          if (settings.about != false) {
            span.innerHTML  = ' | ';

            //about.href     = settings.baseUrl + 'v' + majorVersion + '/sobre';
            about.href       = 'https://mapa.socioambiental.org/sobre';
            about.innerHTML  = self.methods.message('about');

            // Target blank is used mainly for iframe compatibility
            about.setAttribute('target', '_blank');
          }

          L.DomEvent.disableScrollPropagation(div);
          L.DomEvent.disableClickPropagation(div);
          L.DomEvent.on(div, 'wheel', L.DomEvent.stopPropagation);
          L.DomEvent.on(div, 'click', L.DomEvent.stopPropagation);

          return div;
        },
      });

      L.control.watermark = function(opts) {
        return new L.Control.Watermark(opts);
      }

      L.control.watermark({ position: 'bottomleft' }).addTo(myMap);
    }

    self.methods.statusControl = function() {
      L.Control.Status = L.Control.extend({
        onAdd: function(map) {
          var div  = L.DomUtil.create('div');
          var span = L.DomUtil.create('span', '', div);
          span.id  = settings.mapId + '_status';

          return div;
        },
      });

      L.control.status = function(opts) {
        return new L.Control.Status(opts);
      }

      L.control.status({ position: 'bottomleft' }).addTo(myMap);
    }

    self.methods.statusMessageLoading = function(params) {
      if (params == undefined) {
        params = {};
      }

      if (params.name == undefined) {
        params.name = 'default';
      }

      if (statusMessages[params.name] == undefined || statusMessages[params.name] != true) {
        statusMessages[params.name] = true;

        jQuery('#' + settings.mapId + '_status').html(settings.overlayMessage);
      }
    }

    self.methods.statusMessageReady = function(name) {
      if (name == undefined) {
        name = 'default';
      }

      if (statusMessages[name] != undefined && statusMessages[name] == true) {
        delete statusMessages[name];
      }

      if (statusMessages.length == 0) {
        jQuery('#' + settings.mapId + '_status').html('');
      }
    }

    self.methods.legendControl = function() {
      var legendElements = {
        collapseSimple   : true,
        detectStretched  : true,
        collapsedOnInit  : true,
        collapsed        : true,
        position         : 'topright',
        visibleIcon      : 'icon icon-eye',
        hiddenIcon       : 'icon icon-eye-slash',
        //defaultOpacity : 0.5,
        legends          : [],
      };

      // Compile legend definitions
      for (var group in layers) {
        for (var layer in layers[group].layers) {
          if (layers[group].layers[layer].layer != undefined) {
            if (layers[group].layers[layer].legend != undefined) {
              legendElements.legends.push({
                name     : layers[group].layers[layer].description,
                layer    : layers[group].layers[layer].layer,
                elements : layers[group].layers[layer].legend,
              });
            }
          }
        }
      }

      var legend = L.control.htmllegend(legendElements);

      myMap.addControl(legend);
    }

    self.methods.embedUrl = function() {
      return settings.baseUrl + 'v' +  majorVersion + '/' + ((encodedSettings != '') ? '#' + encodedSettings : '');
    }

    self.methods.embedCode = function() {
      return '<iframe src="' + decodeURIComponent(self.methods.embedUrl()) + '" height="300px" width="100%" allowfullscreen frameborder="0" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>';
    }

    self.methods.embedOEmbed = function(format) {
      if (format == null) {
        var format = 'json';
      }

      var url = encodeURIComponent(self.methods.embedUrl());

      return settings.baseUrl + 'oembed/?url=' + url + '&maxheight=300&maxwidth=500&format=' + format;
    }

    self.methods.embedUpdate = function() {
      if (settings.embedControl == false || settings.embedControle == 'false') {
        return;
      }

      // This approach refreshes the control everytime
      //myMap.removeControl(embedControl);
      //myMap.addControl(embedControl);

      // This approach only updates embed/share codes
      var addr   = self.methods.embedUrl();
      var embed  = self.methods.embedCode();
      var oEmbed = self.methods.embedOEmbed();

      jQuery('#' + settings.mapId + '-isamap-embed-code-textarea-oembed').text(oEmbed);
      jQuery('#' + settings.mapId + '-isamap-embed-code-textarea-code').text(embed);
      jQuery('#' + settings.mapId + '-isamap-embed-code-textarea-url').text(addr);
      jQuery('#' + settings.mapId + '-isamap-embed-code-link').attr('href', addr);
      jQuery('#' + settings.mapId + '-isamap-embed-code-fb').attr('data-href', addr);

      jQuery('#oembed-json').attr('href', oEmbed);
      jQuery('#oembed-xml').attr('href', self.methods.embedOEmbed('xml'));
    }

    self.methods.embedCodeControl = function() {
      if (settings.embedControl == false || settings.embedControl == 'false') {
        return;
      }

      L.Control.EmbedCode = L.Control.SmallWidget.extend({
        _icon: 'fa-share',
        //_icon: 'fa-code',
        _name: 'embed-code',

        render: function(map) {
          var addr  = self.methods.embedUrl();
          var embed = self.methods.embedCode();

          // Caption
          var captionShare       = L.DomUtil.create('h3', 'small-widget-caption', this._container);
          captionShare.innerHTML = self.methods.message('share');

          // Share links
          var share = L.DomUtil.create('div', 'share', this._container);

          var twitter  = L.DomUtil.create('a', 'twitter-share-button', share);
          twitter.href = 'https://twitter.com/intent/tweet?text=' + self.methods.message('share_text_title');
          twitter.setAttribute('data-size', 'small');

          // Target blank is used mainly for ifram compatibility
          twitter.setAttribute('target', '_blank');

          //var gPlus    = L.DomUtil.create('div', 'g-plus', share);
          //gPlus.setAttribute('data-action',      'share');
          //gPlus.setAttribute('data-annotation',  'none');

          //var linkedIn = L.DomUtil.create('script', '', share);
          //linkedIn.setAttribute('type', 'IN/Share');

          var fb = L.DomUtil.create('div', 'fb-like', share);
          fb.id  = settings.mapId + '-isamap-embed-code-fb';
          fb.setAttribute('data-href',   addr);
          fb.setAttribute('data-layout', 'button');
          fb.setAttribute('data-action', 'like');
          fb.setAttribute('data-share',  'true');

          // Input for copying the map URL
          var captionUrl = L.DomUtil.create('h3',       '',                                                this._container);
          var inputUrl   = L.DomUtil.create('textarea', '',                                                this._container);

          if (Clipboard.isSupported()) {
            var copyUrl = L.DomUtil.create('button',    'isamap-embed-control-clipboard fa fa-clipboard',  this._container);
          }

          var link     = L.DomUtil.create('a',          'isamap-embed-control-link',                       this._container);
          var linkIcon = L.DomUtil.create('i',          'fa fa-external-link',                             link);

          // Input for copying the map embed code
          var captionCode = L.DomUtil.create('h3',       '',                                               this._container);
          var inputCode   = L.DomUtil.create('textarea', '',                                               this._container);

          if (Clipboard.isSupported()) {
            var copyCode = L.DomUtil.create('button',   'isamap-embed-control-clipboard fa fa-clipboard',  this._container);
          }

          // Input for copying the map oEmbed code
          //var captionOEmbed = L.DomUtil.create('h3',       'isamap-embed-control-oembed-caption',          this._container);
          //var inputOEmbed   = L.DomUtil.create('textarea', '',                                             this._container);

          //if (Clipboard.isSupported()) {
          //  var copyOEmbed = L.DomUtil.create('button',  'isamap-embed-control-clipboard fa fa-clipboard', this._container);
          //}

          // Clipboard support with https://clipboardjs.com
          //inputOEmbed.id = settings.mapId + '-isamap-embed-code-textarea-oembed';
          inputCode.id   = settings.mapId + '-isamap-embed-code-textarea-code';
          inputUrl.id    = settings.mapId + '-isamap-embed-code-textarea-url';
          link.id        = settings.mapId + '-isamap-embed-code-link';

          // Target blank is used mainly for iframe compatibility
          link.setAttribute('target', '_blank');

          //inputOEmbed.setAttribute('rows', 1);
          inputUrl.setAttribute('rows',  1);
          inputCode.setAttribute('rows', 1);

          if (Clipboard.isSupported()) {
            //copyOEmbed.setAttribute('aria-hidden', true);
            //copyOEmbed.setAttribute('data-clipboard-target', '#' + inputOEmbed.id);
            copyCode.setAttribute('aria-hidden', true);
            copyCode.setAttribute('data-clipboard-target',   '#' + inputCode.id);
            copyUrl.setAttribute('aria-hidden', true);
            copyUrl.setAttribute('data-clipboard-target',    '#' + inputUrl.id);
          }

          // Use innetText to prevent entity conversion
          //captionOEmbed.innerHTML = self.methods.message('embed_oembed') + ': ';
          captionCode.innerHTML   = self.methods.message('embed_code')   + ': ';
          captionUrl.innerHTML    = self.methods.message('embed_url')    + ': ';
          inputCode.innerText     = embed;
          inputUrl.innerText      = addr;
          link.href               = addr;
        },

        customToggle: function () {
          if (this.clipboard == undefined && Clipboard.isSupported()) {
            //this.clipboard = new Clipboard('.isamap-embed-control-clipboard');

            //this.clipboard.on('success', function(e) {
            //  self.methods.debug('Clipboard action ' + e.action + ', text ' + e.text + ', trigger ' + e.trigger);
            //  e.clearSelection();
            //});

            //this.clipboard.on('error', function(e) {
            //  self.methods.debug('Clipboard error ' + e.action + ', trigger ' + e.trigger);
            //});

            // Implementation via clipboard.js is not working as the click event is not being fired:
            // just try debugging 'listenClick'and 'onClick' keys from _createClass at Clipboard
            // variable from clipboard.js
            //
            // So here we use a small workaround.
            //
            // https://stackoverflow.com/questions/400212/how-do-i-copy-to-the-clipboard-in-javascript
            jQuery('.isamap-embed-control-clipboard').on('click', function() {
              var target       = jQuery(this).attr('data-clipboard-target');
              var copyTextarea = document.querySelector(target);

              copyTextarea.select();

              try {
                var successful = document.execCommand('copy');
                var msg        = successful ? 'successful' : 'unsuccessful';

                self.methods.debug('Copying text command was ' + msg);
              } catch (err) {
                self.methods.debug('Oops, unable to copy to clipboard');
              }
            });

          }

          // Twitter widget
          if (window.twttr == undefined) {
            window.twttr = (function(d, s, id) {
              var js, fjs = d.getElementsByTagName(s)[0], t = window.twttr || {};
              if (d.getElementById(id)) return t;

              js     = d.createElement(s);
              js.id  = id;
              js.src = "https://platform.twitter.com/widgets.js";

              fjs.parentNode.insertBefore(js, fjs);

              t._e    = [];
              t.ready = function(f) {
                t._e.push(f);
              };

              return t;
            }(document, "script", "twitter-wjs"));
          }

          // Facebook SDK
          (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];

            if (d.getElementById(id)) return;

            js     = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.5";

            fjs.parentNode.insertBefore(js, fjs);
          }(document, 'script', 'facebook-jssdk'));
        },
      });

      L.control.embedcode = function(opts) {
        return new L.Control.EmbedCode(opts);
      }

      embedControl = L.control.embedcode({ position: 'topright' }).addTo(myMap);
    }

    self.methods.searchControl = function() {
      if (settings.searchControl == false || settings.searchControl == 'false') {
        return;
      }

      L.Control.SearchControl = L.Control.SmallWidget.extend({
        _icon: 'fa-search',
        _name: 'search-control',

        render: function(map) {
          // Caption
          var caption       = L.DomUtil.create('h3', 'small-widget-caption', this._container);
          caption.innerHTML = self.methods.message('search');

          // Clear button
          this._buttonClear = L.DomUtil.create('i', 'fa fa-trash-o', this._container);

          // Search box
          var select  = L.DomUtil.create('select', 'chosen-select', this._container);
          select.id = settings.mapId + '-leaflet-arp-selection';
          select.setAttribute('name',             'leaflet-arp-selection');
          select.setAttribute('multiple',         '');
          select.setAttribute('data-placeholder', self.methods.message('search_categories'));

          // Populate select box
          var items     = {};
          var optGroups = {};
          optGroups.ti  = L.DomUtil.create('optgroup', '', select);
          optGroups.uc  = L.DomUtil.create('optgroup', '', select);

          optGroups.ti.setAttribute('label', self.methods.message('indigenous_lands'));
          optGroups.uc.setAttribute('label', self.methods.message('conservation_areas'));

          // TIs by name
          for (var i in arps.tis) {
            items[i] = L.DomUtil.create('option', '', optGroups.ti);
            items[i].setAttribute('value', arps.tis[i].id);
            items[i].innerHTML = arps.tis[i].categoria + ' ' + self.methods.truncateString(arps.tis[i].nome_ti, 45);

            // Select ARPs currently present on settings.arps
            if (settings.arps != null && settings.arps.indexOf(arps.tis[i].id.toString()) != -1) {
              items[i].setAttribute('selected', '');
            }
          }

          // UCs by name
          for (var i in arps.ucs) {
            items[i] = L.DomUtil.create('option', '', optGroups.uc);
            items[i].setAttribute('value', arps.ucs[i].id);

            // Used with API SisARP v0
            //items[i].innerHTML = self.methods.truncateString(arps.ucs[i].nome_completo, 45);

            // Used with API SisARP v2
            items[i].innerHTML = self.methods.truncateString(arps.ucs[i].categoria + ' ' + arps.ucs[i].nome_arp, 45);

            // Selected those ARPs that are present on settings.arps
            if (settings.arps != null && settings.arps.indexOf(arps.ucs[i].id.toString()) != -1) {
              items[i].setAttribute('selected', '');
            }
          }

          // ARP Groups
          for (var group in arps) {
            for (var property in arpsProperties[group]) {
              var currentProperty = arpsProperties[group][property];
              var groupProperty   = group + '.' + currentProperty;

              optGroups[groupProperty] = L.DomUtil.create('optgroup', '', select);
              optGroups[groupProperty].setAttribute('label', self.methods.message(groupProperty));

              for (var propertyItems in arpsByProperty[group][currentProperty]) {
                var currentIndex = group + '.' + arpsByProperty[group][currentProperty];

                items[currentIndex] = L.DomUtil.create('option', '', optGroups[groupProperty]);
                items[currentIndex].setAttribute('value', groupProperty + '.' + propertyItems);

                // Exception handling
                if (group == 'tis') {
                  if (currentProperty == 'uf') {
                    var propertyName = ufs[propertyItems] + ' - ' + propertyItems;
                  }
                  else if (currentProperty == 'populacao') {
                    var populationMsg = self.methods.message('population');
                    var propertyName  = populationMsg[propertyItems];
                  }
                  else if (currentProperty == 'area') {
                    var populationMsg = self.methods.message('area');
                    var propertyName  = populationMsg[propertyItems];
                  }
                  else {
                    if (propertyItems != null && propertyItems != undefined && propertyItems != '') {
                      var propertyName = propertyItems;
                    }
                    else {
                      var propertyName = self.methods.message('no_info');
                    }
                  }
                }
                else {
                  if (propertyItems != null && propertyItems != undefined && propertyItems != '') {
                    var propertyName = propertyItems;
                  }
                  else {
                    var propertyName = self.methods.message('no_info');
                  }
                }

                items[currentIndex].innerHTML = propertyName + ' (' + arpsByProperty[group][currentProperty][propertyItems].length + ')';

                // Select optGroups currently present on settings.arps
                if (settings.layers != null && settings.layers.indexOf(groupProperty + '.' + propertyItems) != -1) {
                  items[currentIndex].setAttribute('selected', '');
                }
              }
            }
          }

          // Keep chosen updated
          //L.DomEvent.on(this._container, 'click', function() {
          //  jQuery('#' + settings.mapId + '-leaflet-arp-selection').trigger('chosen:updated');
          //});

          // Clear button behavior
          L.DomEvent.on(this._buttonClear, 'click', function() {
            // Thanks https://stackoverflow.com/questions/11365212/how-do-i-reset-a-jquery-chosen-select-option-with-jquery#11365340
            jQuery('#' + settings.mapId + '-leaflet-arp-selection').val('').trigger('chosen:updated');
            jQuery('#' + settings.mapId + '-leaflet-arp-selection').trigger('chosen:open');

            self.methods.removeArpsByGroup();
            self.methods.restoreDefaultBounds();
          });
        },

        customToggle: function () {
          if (L.DomUtil.hasClass(this._container, 'closed')) {
            jQuery('#' + settings.mapId + '-leaflet-arp-selection').chosen({
              'search_contains'                 : true,
              'include_group_label_in_selected' : true,
              'no_results_text'                 : self.methods.message('no_results'),
            });

            // Activate chosen on toggle
            //jQuery('#' + settings.mapId + '-leaflet-arp-selection .chosen-search-input').focus();
            //jQuery('#' + settings.mapId + '-leaflet-arp-selection .chosen-search-input').text('');
            //jQuery('#' + settings.mapId + '-leaflet-arp-selection').trigger('chosen:activate');
            jQuery('#' + settings.mapId + '-leaflet-arp-selection').trigger('chosen:open');

            jQuery('#' + settings.mapId + '-leaflet-arp-selection').change(function() {
              var ids          = [];
              var showingGroup = false;

              // Remove all ARPs
              //self.methods.removeArpsById();

              // Remove all ARP group layers
              self.methods.removeArpsByGroup();

              if (this.multiple == false) {
                ids.push(this.value);
              }
              else {
                for (var i in this.selectedOptions) {
                  if (this.selectedOptions[i].value != undefined) {
                    var value = this.selectedOptions[i].value;

                    if (parseInt(value)) {
                      ids.push(value);
                    }
                    else {
                      var split    = value.split('.');
                      showingGroup = true;

                      self.methods.showLayer(split[0], split[1] + '.' + split[2]);
                    }
                  }
                }
              }

              // Make sure chosen is updated: it was observed that if chosen-drop element has
              // position set other than "absolute" then some elements might not be updated.
              jQuery('#' + settings.mapId + '-leaflet-arp-selection').trigger('chosen:updated');

              // Do not work on empty selection
              if (ids.length != 0) {
                // Due to performance concerns, do not plot limits if there are too many selected ARPs
                if (ids.length <= maxArpsWithLimits) {
                  self.methods.addArpsById({ arps: ids, type: 'markers', show: true, fitBounds: true });
                  self.methods.addArpsById({ arps: ids, type: 'limits',  show: true });
                }
                else {
                  self.methods.addArpsById({ arps: ids, type: 'markers', show: true });
                }
              }
              else if (showingGroup == false) {
                // If there are no remaining ARPs, set zoom and center to initial settings
                self.methods.restoreDefaultBounds();
              }
            });
          }
        },
      });

      L.control.searchcontrol = function(opts) {
        return new L.Control.SearchControl(opts);
      }

      searchControl = L.control.searchcontrol({ position: 'topright' }).addTo(myMap);
    }

    self.methods.addLayerIntoSettings = function(group, layer) {
      if (group == 'arps') {
        return;
      }

      var entry = group + '.' + layer;

      if (settings.layers.indexOf(entry) == -1) {
        settings.layers.push(entry);
        //activeMapEntities.push(e.layer._leaflet_id);
      }

      // Update encoded settings
      self.methods.updateEncodedSettings();
    }

    self.methods.hideLayerFromSettings = function(group, layer) {
      var entry = group + '.' + layer;
      var index = settings.layers.indexOf(entry);

      if (index != -1) {
        settings.layers.splice(index, 1);
        //activeMapEntities.splice(activeIndex, 1);
      }

      // Update encoded settings
      self.methods.updateEncodedSettings();
    }

    self.methods.mapStateTracking = function() {
      // Build table of layer IDs
      //for (var group in layers) {
      //  for (var layer in layers[group].layers) {
      //    if (layers[group].layers[layer].layer != undefined) {
      //      if (layers[group].layers[layer].layer._leaflet_id != undefined) {
      //        mainLayerIds[layers[group].layers[layer].layer._leaflet_id] = false;
      //      }
      //    }
      //  }
      //}

      // Base layer
      trackingEvents.baselayerchange = myMap.on('baselayerchange', function(e) {
        for (var group in baseLayers) {
          for (var layer in baseLayers[group].layers) {
            if (baseLayers[group].layers[layer].layer._leaflet_id == e.id) {
              settings.baseLayer = group + '.' + layer;
            }
          }
        }

        // Update encoded settings
        self.methods.updateEncodedSettings();
      });

      // Zoom
      trackingEvents.zoomend = myMap.on('zoomend', function(e) {
        settings.zoom = myMap.getZoom();

        // Update encoded settings
        self.methods.updateEncodedSettings();
      });

      // Center
      trackingEvents.moveend = myMap.on('moveend', function(e) {
        var center      = myMap.getCenter();
        settings.center = [ center.lat, center.lng ];

        // Update encoded settings
        self.methods.updateEncodedSettings();
      });

      trackingEvents.overlayadd = myMap.on('overlayadd', function(e) {
        // Check if is a main layer
        //if (mainLayerIds[e.layer._leaflet_id] == undefined) {
        //  return;
        //}
        //else if (mainLayerIds[e.layer._leaflet_id] == true) {
        //  return;
        //}
        //else {
        //  mainLayerIds[e.layer._leaflet_id] = true;
        //}

        // Use a simple array to speed up search process
        //if (activeMapEntities.indexOf(e.layer._leaflet_id) != -1) {
        //  self.methods.debug('Layer already tracked:' + e.layer._leaflet_id);
        //  return;
        //}

        for (var group in layers) {
          // Skip arps group
          if (group == 'arps') {
            continue;
          }

          for (var layer in layers[group].layers) {
            if (layers[group].layers[layer].layer._leaflet_id == e.layer._leaflet_id) {
              self.methods.addLayerIntoSettings(group, layer);
            }
          }
        }

        // Update encoded settings
        //self.methods.updateEncodedSettings();
      });

      // Layer remove
      trackingEvents.overlayremove = myMap.on('overlayremove', function(e) {
        // Check if is a main layer
        //if (mainLayerIds[e.layer._leaflet_id] == undefined) {
        //  return;
        //}
        //else if (mainLayerIds[e.layer._leaflet_id] == false) {
        //  return;
        //}
        //else {
        //  mainLayerIds[e.layer._leaflet_id] = false;
        //}

        // Use a simple array to speed up search process
        //var activeIndex = activeMapEntities.indexOf(e.layer._leaflet_id);
        //if (activeIndex == -1) {
        //  return;
        //}

        for (var group in layers) {
          // Skip arps group
          if (group == 'arps') {
            continue;
          }

          for (var layer in layers[group].layers) {
            if (layers[group].layers[layer].layer._leaflet_id == e.layer._leaflet_id) {
              self.methods.hideLayerFromSettings(group, layer);
            }
          }
        }

        // Update encoded settings
        //self.methods.updateEncodedSettings();
      });
    }

    self.methods.updateEncodedSettings = function() {
      // Encode just some settings
      var options = [ 'lang', 'arps', 'layers', 'baseLayer', 'center', 'zoom', 'minZoom', 'maxZoom' ]
      var encoded = { };

      for (var option in options) {
        if (settings[options[option]] != undefined) {
          encoded[options[option]] = settings[options[option]];
        }
      }

      encodedSettings = jQuery.param(encoded);

      // Refresh control
      self.methods.embedUpdate();

      // Update location: hash version
      //if (settings.hashChange == true) {
      //  window.location.hash = encodedSettings;
      //}
      // Update location: hash version
      if (settings.urlChange == true) {
        var title = messages[settings.lang].name;
        var url   = window.location.protocol + '//' + window.location.hostname + window.location.pathname + '?' + encodedSettings;

        // See https://stackoverflow.com/questions/824349/modify-the-url-without-reloading-the-page#3354511
        //     https://stackoverflow.com/questions/2494213/changing-window-location-without-triggering-refresh
        //window.location.href = url;
        window.history.replaceState({}, title, url);
      }
    }

    self.methods.popup = function(latlng, content) {
      if (content == '') {
        return;
      }

      var popup = L.popup({
        maxHeight: 200,
      }).setLatLng(latlng)
        .setContent(content)
        .openOn(myMap);
    }

    self.methods.restoreDefaultBounds = function() {
      self.methods.debug('Restoring initial center and zoom...');
      myMap.setZoom(initialSettings.zoom);
      myMap.panTo(L.latLng(defaultSettings.center));
    }

    self.methods.showLayer = function(group, layer) {
      if (layers[group] == undefined || layers[group].layers[layer] == undefined) {
        return;
      }

      self.methods.debug('Showing Layer ' + group + '.' + layer + '...');

      // Pan out
      //
      // This also captures all loaded features as L.esri.featureLayer load event
      // fires only for features added in the current bounds.
      //
      // This is not always the case, so why we keep it optional.
      if (layers[group].layers[layer].panOut == true || layers[group].layers[layer].fitBounds == true) {
        myMap.setZoom(minZoom);
        myMap.panTo(L.latLng(mapCenter));
      }

      // Tell the user we're loading
      //self.methods.showOverlay(group + '.' + layer);
      self.methods.statusMessageLoading({ name: group + '.' + layer });

      layers[group].layers[layer].layer.once('load', function(e) {
        // Tell the user we're ready
        //self.methods.unblockUI(group + '.' + layer);
        self.methods.statusMessageReady(group + '.' + layer);

        if (layers[group].layers[layer].fitBounds != undefined && layers[group].layers[layer].fitBounds == true) {
          self.methods.debug('Fit bounds for ' + group + '.' + layer + '...');

          //myMap.flyToBounds(layers[group].layers[layer].bounds);
          myMap.fitBounds(layers[group].layers[layer].bounds);
        }
      });

      //if (self.methods.hasLayer(group, layer)) {
      //  if (layers[group].layers[layer].layer.options.opacity != undefined) {
      //    var opacity = layers[group].layers[layer].layer.options.opacity;
      //  }
      //  else {
      //    var opacity = 1;
      //  }

      //  layers[group].layers[layer].layer.setOpacity(opacity);
      //}
      //else {
      //  layers[group].layers[layer].layer.addTo(myMap);
      //}
      layers[group].layers[layer].layer.addTo(myMap);

      // Update settings.layers since addTo does not trigger and overlayadd event
      self.methods.addLayerIntoSettings(group, layer);
    }

    self.methods.hideLayer = function(group, layer) {
      self.methods.debug('Removing Layer ' + group + '.' + layer +'...');

      //layers[group].layers[layer].layer.setOpacity(0);
      myMap.removeLayer(layers[group].layers[layer].layer);

      // Update settings.layers since hideLayer does not trigger and overlayremove event
      self.methods.hideLayerFromSettings(group, layer);
    }

    self.methods.removeBasicArpLayers = function() {
      self.methods.hideLayer('tis', 'limits');
      self.methods.hideLayer('tis', 'markers');
      self.methods.hideLayer('ucs', 'markersEstaduais');
      self.methods.hideLayer('ucs', 'markersFederais');
      self.methods.hideLayer('ucs', 'limitsEstaduais');
      self.methods.hideLayer('ucs', 'limitsFederais');
    }

    self.methods.removeArpsByGroup = function(group) {
      self.methods.removeBasicArpLayers();

      if (group == undefined || group == null) {
        var groups = [ 'tis', 'ucs', 'arps' ];

        // Handle special case
        settings.arps = [];
        self.methods.updateEncodedSettings();
      }
      else {
        var groups = [ group ];
      }

      for (var group in groups) {
        if (layers[groups[group]] != undefined) {
          for (var layer in layers[groups[group]].layers) {
            // Hide only marker layers that are currently shown
            //if (layers[groups[group]].layers[layer].layer.options.pointToLayer != undefined && self.methods.hasLayer(groups[group], layer)) {
            if (self.methods.hasLayer(groups[group], layer)) {
              self.methods.hideLayer(groups[group], layer);
            }
          }
        }
      }
    }

    // Initialize a map
    self.methods.initialize = function() {
      // Config conversion
      // Force urlChange to true if hashChange is true, set to false otherwise.
      // The new param is urlChange, but we should honor and give preference to the old one.
      if (settings.hashChange == true) {
        settings.urlChange = true;
      }
      else {
        settings.urlChange = false;
      }

      // Set layer definition
      if (settings.layerDefinitions != null) {
        if (settings.layerDefinitionsExtend == true) {
          layers = jQuery.extend(true, {}, settings.layerDefinitions, defaultLayers);
        }
        else {
          layers = settings.layerDefinitions;
        }
      }
      else {
        layers = defaultLayers;
      }

      // Exclude some groups
      if (settings.groupExcluded != [ ]) {
        for (var excluded in settings.groupExcluded) {
          if (layers[settings.groupExcluded[excluded]] != undefined ) {
            delete layers[settings.groupExcluded[excluded]];
          }
        }
      }

      // Exclude some layers
      if (settings.layersExcluded != [ ]) {
        for (var excluded in settings.layerExcluded) {
          var split = settings.layerExcluded[excluded].split('.');

          if (layers[split[0]] != undefined && layers[split[0]].layers[split[1]] != undefined) {
            delete layers[split[0]].layers[split[1]];
          }
        }
      }

      // Set control configuration
      if (settings.expandedGroups.length != 0) {
        for (var g = 0; g < settings.expandedGroups.length; g++) {
          if (layers[settings.expandedGroups[g]].expanded != undefined) {
            layers[settings.expandedGroups[g]].expanded = true;
          }
        }
      }

      // Ensure we have an 'arps' group
      if (layers.arps == undefined) {
        layers.arps = {
          groupName: 'ARPs',
          expanded : false,
          layers   : { },
        };
      }

      // Create the map
      self.methods.createMap();

      // Search control is initialized here as it depends on metadata
      self.methods.searchControl();

      // Display ARPs
      if (settings.arps != null && settings.arps != false && settings.arps.length != 0) {
        // Due to performance concerns, do not plot limits if there are too many selected ARPs
        if (settings.arps.length <= maxArpsWithLimits) {
          self.methods.addArpsById({ arps: settings.arps, type: 'markers', show: true, fitBounds: true });
          self.methods.addArpsById({ arps: settings.arps, type: 'limits',  show: true });
        }
        else {
          self.methods.addArpsById({ arps: settings.arps, type: 'markers', show: true });
        }
      }
      else {
        // Convert to array
        settings.arps = [];
      }

      // Add ARP group layers (ti.uf.AM etc)
      for (var group in arpsByProperty) {
        for (var subgroup in arpsByProperty[group]) {
          for (var layer in arpsByProperty[group][subgroup]) {
            self.methods.addArpsByGroup({
              group : group,
              name  : subgroup + '.' + layer,
              show  : false,
            });
          }
        }
      }

      // Display custom layer selection
      for (var l = 0; l < settings.layers.length; l++) {
        var split = settings.layers[l].split('.');

        if (split[0] != undefined && split[1] != undefined) {
          if (split[2] != undefined) {
            self.methods.showLayer(split[0], split[1] + '.' + split[2]);
          }
          else {
            self.methods.showLayer(split[0], split[1]);
          }
        }
      }

      // Trigger initialization event
      jQuery(settings.overlaySelector).trigger('isaMapReady');
    }

    self.methods.hasLayer = function(group, layer) {
      if (layers[group] == undefined) {
        return false;
      }

      if (layers[group].layers[layer] == undefined) {
        return false;
      }

      if (myMap.hasLayer(layers[group].layers[layer].layer)) {
        return true;
      }

      return false;
    }

    self.methods.hasArp = function(id, type) {
      layerId = type + '_' + id;

      if (layers.arps.layers[layerId] != undefined) {
        return true;
      }

      return false;
    }

    // See https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort?v=example
    self.methods.sortNumber = function(a,b) {
      return a - b;
    }

    self.methods.tiIcon = function(data) {
      if (data != undefined && data.grupo_situacao_juridica != undefined) {
        if (data.grupo_situacao_juridica == 'Registrada no CRI e/ou SPU' || data.grupo_situacao_juridica == 'Homologada') {
          return settings.imagesUrl + '/ti/ti_homologada.png';
        }
        else if (data.grupo_situacao_juridica == 'Declarada') {
          return settings.imagesUrl + '/ti/ti_declarada.png';
        }
        else if (data.grupo_situacao_juridica == 'Identificada') {
          return settings.imagesUrl + '/ti/ti_identificada.png';
        }
        else if (data.grupo_situacao_juridica == 'Em Identificação') {
          return settings.imagesUrl + '/ti/ti_identificacao.png';
        }
        else if (data.grupo_situacao_juridica == 'Reservada') {
          return settings.imagesUrl + '/ti/ti_reservada.png';
        }
        else if (data.grupo_situacao_juridica == 'Com restrição de uso a não índios') {
          return settings.imagesUrl + '/ti/ti_restricao.png';
        }
        else {
          return settings.imagesUrl + '/ti/ti_square.png';
        }
      }
      else {
        return settings.imagesUrl + '/ti/ti_square.png';
      }
    }

    self.methods.buildArpWhere = function(params) {
      var id       = null;
      params.where = '';

      if (params.whereCondition == undefined) {
        params.whereCondition = ' OR ';
      }

      // By sorting the array we can make sure we always get the same hash
      params.arps.sort(self.methods.sortNumber);

      // Build query conditions
      for (var i = 0; i < params.arps.length; i++) {
        id            = params.arps[i];
        params.where += 'id_arp=' + id + params.whereCondition;
      }

      // Remove trailing 'OR' from query builder
      params.where = params.where.substring(0, params.where.length - 3);

      return params.where;
    }

    self.methods.addArpsByGroup = function(params) {
      // Check if group is defined
      if (layers[params.group] == undefined) {
        layers[params.group] = { 'layers': {}};
      }

      // Check if it's already defined
      if (layers[params.group].layers[params.name] != undefined) {
        if (params.show != undefined && params.show == true) {
          self.methods.showLayer(params.group, params.name);
        }

        return;
      }

      // Determine ARP IDs
      var split   = params.name.split('.');
      params.arps = arpsByProperty[params.group][split[0]][split[1]];

      // Build where condition
      params.where = self.methods.buildArpWhere(params);

      // Build layer
      self.methods.addArpLayer(params);
    }

    self.methods.addArpsById = function(params) {
      // Set group
      params.group = 'arps';

      // Set type and base name
      if (params.type == undefined) {
        params.type = 'markers';
        params.name = 'markers';
      }
      else {
        params.name = params.type;
      }

      if (params.arps == undefined || params.arps.length == 0) {
        return;
      }

      for (var i = 0; i < params.arps.length; i++) {
        var id = params.arps[i];

        if (!jQuery.isNumeric(id)) {
          continue;
        }

        // Build unique layer name
        params.name += '_' + id;

        // Update settings.arps for state tracking
        if (settings.arps.indexOf(id) == -1) {
          settings.arps.push(id);
        }
      }

      // Check if it's already defined
      if (layers[params.group].layers[params.name] != undefined) {
        if (params.show != undefined && params.show == true) {
          self.methods.showLayer(params.group, params.name);
        }

        return;
      }

      // Build where condition
      params.where = self.methods.buildArpWhere(params);

      // Build layer
      self.methods.addArpLayer(params);
    }

    self.methods.addArpLayer = function(params) {
      // Check if group is defined
      if (layers[params.group] == undefined) {
        layers[params.group] = { 'layers': {}};
      }

      // Check if it's already defined
      if (layers[params.group].layers[params.name] != undefined) {
        if (params.show != undefined && params.show == true) {
          self.methods.showLayer(params.group, params.name);
        }

        return;
      }

      // Set type and base name
      if (params.type == undefined) {
        params.type = 'markers';
      }

      //self.methods.debug('Registering Layer ' + params.group + '.' + params.name + '...');

      var layerName = 'arps' + params.type.substring(0, 1).toUpperCase() + params.type.substring(1).toLowerCase();
      var service   = self.methods.gisLayerUrl(layerName);

      // Define layer
      if (params.type == 'markers') {
        layers[params.group].layers[params.name] = {
          description: params.group + '.' + params.name,
          layer: L.esri.featureLayer({
            url          : service,
            where        : params.where,
            opacity      : 1,
            proxy        : settings.proxyUrl,
            cacheLayers  : true,
            //token        : apiKey,
            pointToLayer : function (geojson, latlng) {
              var id = geojson.properties.id_arp;

              if (geojson.properties.tipo_aps == 'TI') {
                //var icon     = settings.imagesUrl + '/ti/ti_feather.png';
                //var iconSize = [22, 28];

                var icon     = self.methods.tiIcon(arps.tis[geojson.properties.id_arp]);
                var iconSize = [32, 32];
              }
              else if (geojson.properties.tipo_aps == 'UCE') {
                var icon     = settings.imagesUrl + '/uc/estadual.png';
                var iconSize = [16, 24];
              }
              else if (geojson.properties.tipo_aps == 'UCF') {
                var icon     = settings.imagesUrl + '/uc/federal.png';
                var iconSize = [16, 24];
              }

              if (params.fitBounds != undefined && params.fitBounds == true) {
                layers[params.group].layers[params.name].fitBounds = true;

                // Here we use area_isa instead of official area because it's more
                // compatible with the plots and hence it's a better reference
                // to calculate boundaries
                if (arps.tis[id] != undefined) {
                  var area = arps.tis[id].area_isa;
                }
                else if (arps.ucs[id] != undefined) {
                  var area = arps.ucs[id].area_isa;
                }

                // We used this as an area estimate/average when we did not had
                // area information
                //var toBounds = 100000;

                // From hectare get an average distance in meters
                var toBounds = Math.sqrt(area) * 10^4;

                // Then get a bigger area to make sure the ARP will fit the screen
                toBounds = toBounds * 25;

                layers[params.group].layers[params.name].bounds.extend(L.latLng(latlng.lat, latlng.lng).toBounds(toBounds));
              }

              var tooltip = self.methods.arpPopupContents({ properties: { id_arp: id } }, false);

              return L.marker(latlng, {
                icon: L.icon({
                  iconUrl:  icon,
                  iconSize: iconSize,
                }),
              }).bindTooltip(tooltip);
            },
          }),
          click: function(e) {
            var content = self.methods.arpPopupContents(e.layer.feature);

            e.layer.bindPopup(content);
            e.layer.openPopup();
          },
          // Create a new empty Leaflet bounds object
          bounds: L.latLngBounds([]),
        };
      }
      else {
        layers[params.group].layers[params.name] = {
          description: params.group + '.' + params.name,
          layer: L.esri.featureLayer({
            url     : service,
            proxy   : settings.proxyUrl,
            //token   : apiKey,
            where   : params.where,
            opacity : 0.4,
            style: function(feature) {
              if (feature.properties.tipo_arps === 'TI') {
                return {color: '#FF0000', weight: 3 };
              }
              else if (feature.properties.tipo_arps === 'UCF') {
                return {color: '#A59237', weight: 3 };
              }
              else if (feature.properties.tipo_arps === 'UCE') {
                return {color: '#4B832D', weight: 3 };
              }
            },
          }),
        };
      }

      // Ensure we pan out
      layers[params.group].layers[params.name].panOut = true;

      // Register load event
      if (params.load != undefined) {
        layers[params.group].layers[params.name].layer.on('load', params.load);
      }

      // Register mouseover event
      if (params.mouseover != undefined) {
        layers[params.group].layers[params.name].layer.on('mouseover', params.mouseover);
      }

      // Register mouseout event
      if (params.mouseout != undefined) {
        layers[params.group].layers[params.name].layer.on('mouseout', params.mouseout);
      }

      // Register popupopen event
      if (params.popupopen != undefined) {
        layers[params.group].layers[params.name].layer.on('popupopen', params.popupopen);
      }

      // Register popupclose event
      if (params.popupclose != undefined) {
        layers[params.group].layers[params.name].layer.on('popupclose', params.popupclose);
      }

      // Register click event
      if (layers[params.group].layers[params.name].click != undefined) {
        layers[params.group].layers[params.name].layer.on('click', layers[params.group].layers[params.name].click);
      }
      else if (params.click != undefined) {
        layers[params.group].layers[params.name].layer.on('click', params.click);
      }

      if (params.show != undefined && params.show == true) {
        // Add to map: on sucess, layers.arps.layers[layerId].layer._layers._leaflet_id is set
        //layers[params.group].layers[params.name].layer.addTo(myMap);
        self.methods.showLayer(params.group, params.name);
      }
    }

    self.methods.geocode = function(params) {
      if (params.address == undefined) {
        return false;
      }

      if (geocode[params.address] != undefined) {
        return geocode[params.address];
      }

      $.ajax({
        dataType: 'json',
        url:       settings.geocodeUrl + '/' + params.address,
        success:   function(response) {
          if (response.status != "OK") {
            return;
          }

          geocode[params.address] = response.results;

          if (params.callback != undefined) {
            params.callback(params, response.results);
          }
        }
      });
    }

    self.methods.removeArpsById = function(arps, type) {
      if (layers.arps == undefined) {
        return;
      }

      if (type == undefined) {
        type = 'markers';
      }

      // If no argument is passed, operate in all available ARPs.
      if (arps == undefined || arps == null) {
        self.methods.debug('Removing all ARP layers...');

        for (name in layers.arps.layers) {
          self.methods.hideLayer('arps', name);
        }

        self.methods.removeBasicArpLayers();

        // Update state tracking
        settings.arps = [];
      }
      else {
        var name = null;

        // By sorting the array we can make sure we always get the same hash
        arps.sort(self.methods.sortNumber);

        for (var i = 0; i < arps.length; i++) {
          var id = arps[i];
          name   = type + '_' + id;
          index  = settings.arps.indexOf(id);

          // Update settings.arps for state tracking
          if (index != -1) {
            settings.arps.splice(index, 1);
          }

          if (layers.arps.layers[name] != undefined) {
            self.methods.hideLayer('arps', name);
          }
        }

        // Already done by self.methods.hideLayer
        //self.methods.updateEncodedSettings();
      }
    }

    self.methods.gisLayerUrl = function(layer) {
      if (gisLayers[layer] != undefined) {
        return gisLayers[layer].url + '/' + gisLayers[layer].layer;
      }
    }

    // Define main GIS Server
    var gisServer = settings.gisServer;

    // Define main GIS Services
    var gisServices = {
      'tematicos'     : gisServer + 'tematicos',
      'desmatamento'  : gisServer + 'tematicos/desmatamento/MapServer',
      'monitoramento' : gisServer + 'monitoramento',
      'arps'          : gisServer + 'monitoramento/arps/MapServer',
      'tis'           : gisServer + 'monitoramento/tis/MapServer',
      'ucs'           : gisServer + 'monitoramento/ucs/MapServer',
    };

    // Define main GIS Layers
    var gisLayers = {
      'arpsMarkers'                   : { url : gisServices.arps,                                    layer : 0 },
      'arpsLimits'                    : { url : gisServices.arps,                                    layer : 1 },
      'tisMarkers'                    : { url : gisServices.tis,                                     layer : 2 },
      'tisLimits'                     : { url : gisServices.tis,                                     layer : 0 },
      'ucsMarkersEstaduais'           : { url : gisServices.ucs,                                     layer : 1 },
      'ucsMarkersFederais'            : { url : gisServices.ucs,                                     layer : 4 },
      'ucsLimitsEstaduais'            : { url : gisServices.ucs,                                     layer : 2 },
      'ucsLimitsFederais'             : { url : gisServices.ucs,                                     layer : 5 },
      'ucsMosaicosCorredores'         : { url : gisServices.ucs,                                     layer : 6 },
      'energia'                       : { url : gisServices.tematicos + '/energia/MapServer',        layer : 0 },
      'mineracao'                     : { url : gisServices.tematicos + '/mineracao/MapServer',      layer : 0 },
      'petroleo'                      : { url : gisServices.tematicos + '/petroleo/MapServer',       layer : 0 },
      'amlegal'                       : { url : gisServices.tematicos + '/LimiteAmazonia/MapServer', layer : 0 },
      'biomas'                        : { url : gisServices.tematicos + '/vegetacao/MapServer',      layer : 1 },
      'cavernas'                      : { url : gisServices.tematicos + '/cavernas/MapServer',       layer : 0 },
      'fitofisionomia'                : { url : gisServices.tematicos + '/vegetacao/MapServer',      layer : 2 },
      'bacias'                        : { url : gisServices.tematicos + '/bacias/MapServer',         layer : 1 },
      'ottobacias'                    : { url : gisServices.tematicos + '/bacias/MapServer',         layer : 2 },
      'focos'                         : { url : gisServices.tematicos + '/focos/MapServer',          layer : 0 },
    };

    var baseLayers = {
      'base': {
        groupName: self.methods.message('basemap'),
        expanded: false,
        layers: {
          topographic: {
            description: self.methods.message('topographic'),
            layer: L.esri.basemapLayer('Topographic', {
              minZoom      : minZoom,
              maxZoom      : maxZoom,
              detectRetina : true,
              attribution  : '&copy; <a href="https://www.arcgis.com/home/index.html">Esri</a>'
            }),
          },
          gray: {
            description: self.methods.message('gray'),
            layer: L.esri.basemapLayer('Gray', {
              minZoom      : minZoom,
              maxZoom      : maxZoom,
              detectRetina : true,
              attribution  : '&copy; <a href="https://www.arcgis.com/home/index.html">Esri</a>'
            }),
          },
          darkgray: {
            description: self.methods.message('darkgray'),
            layer: L.esri.basemapLayer('DarkGray', {
              minZoom      : minZoom,
              maxZoom      : maxZoom,
              detectRetina : true,
              attribution  : '&copy; <a href="https://www.arcgis.com/home/index.html">Esri</a>'
            }),
          },
          /*
          imagery: {
            description: self.methods.message('imagery'),
            layer: L.esri.basemapLayer('Imagery', {
              minZoom      : minZoom,
              maxZoom      : maxZoom,
              detectRetina : true,
              attribution  : '&copy; <a href="https://www.arcgis.com/home/index.html">Esri</a>'
            }),
          },
          terrain: {
            description: self.methods.message('terrain'),
            layer: L.esri.basemapLayer('Terrain', {
              minZoom      : minZoom,
              maxZoom      : maxZoom,
              detectRetina : true,
              attribution  : '&copy; <a href="https://www.arcgis.com/home/index.html">Esri</a>'
            }),
          },
          community: {
            description: self.methods.message('communitary'),
            layer: L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
              minZoom      : minZoom,
              maxZoom      : maxZoom,
              detectRetina : true,
              attribution  : '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors'
              }
            ),
          },
          streets: {
            description: self.methods.message('streets'),
            layer: L.esri.basemapLayer('Streets', {
              minZoom      : minZoom,
              maxZoom      : maxZoom,
              detectRetina : true,
              attribution  : '&copy; <a href="https://www.arcgis.com/home/index.html">Esri</a>'
            }),
          },
          humanitary: {
            description: self.methods.message('humanitary'),
            layer: L.tileLayer('https://tile-{s}.openstreetmap.fr/hot/{z}/{x}/{y}.png', {
              minZoom      : minZoom,
              maxZoom      : maxZoom,
              detectRetina : true,
              attribution  : '&copy; <a href="https://hotosm.org/">HOT</a>'
            }),
          },
          */
          satellite: {
            description: self.methods.message('satelitte'),
            layer: L.esri.basemapLayer('Imagery', {
              minZoom      : minZoom,
              maxZoom      : maxZoom,
              detectRetina : true,
              attribution  : '&copy; <a href="https://www.arcgis.com/home/index.html">Esri</a>'
            }),
          },
          /**
           * https://stackoverflow.com/questions/9394190/leaflet-map-api-with-google-satellite-layer
           * https://github.com/Leaflet/Leaflet/blob/master/FAQ.md#data-providers
           * https://developers.google.com/maps/terms#10-license-restrictions
           */
          /*
          googleSatellite: {
            description: self.methods.message('satelitte') + ' (Google)',
            layer: L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',{
              minZoom      : minZoom,
              maxZoom      : maxZoom,
              detectRetina : true,
              subdomains   : ['mt0','mt1','mt2','mt3'],
              attribution  : '&copy; <a href="https://maps.google.com">Google</a>'
            }),
          },
          googleStreets: {
            description: self.methods.message('streets') + ' (Google)',
            layer: L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',{
              minZoom      : minZoom,
              maxZoom      : maxZoom,
              subdomains   : ['mt0','mt1','mt2','mt3'],
              detectRetina : true,
              attribution  : '&copy; <a href="https://maps.google.com">Google</a>'
            }),
          },
          googleHybrid: {
            description: self.methods.message('hybrid') + ' (Google)',
            layer: L.tileLayer('http://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}',{
              minZoom      : minZoom,
              maxZoom      : maxZoom,
              subdomains   : ['mt0','mt1','mt2','mt3'],
              detectRetina : true,
              attribution  : '&copy; <a href="https://maps.google.com">Google</a>'
            }),
          },
          googleTerrain: {
            description: self.methods.message('terrain') + ' (Google)',
            layer: L.tileLayer('http://{s}.google.com/vt/lyrs=p&x={x}&y={y}&z={z}',{
              minZoom      : minZoom,
              maxZoom      : maxZoom,
              subdomains   : ['mt0','mt1','mt2','mt3'],
              detectRetina : true,
              attribution  : '&copy; <a href="https://maps.google.com">Google</a>'
            }),
          },
          */
        },
      },
    };

    self.methods.arpPopupContents = function(feature, complete) {
      if (feature.properties.id_arp == undefined) {
        return '';
      }

      var id    = feature.properties.id_arp;
      var table = {};

      if (arps.tis[id] != undefined) {
        // Target blank is used mainly for ifram compatibility
        var name = arps.tis[id].categoria + ' ' + arps.tis[id].nome_ti;
        var link = '<a href="https://ti.socioambiental.org/' + settings.lang + '/terras-indigenas/' + id + '" target="_blank">' + name + '</a>';
        var type = 'tis';
      }
      else if (arps.ucs[id] != undefined) {
        // Target blank is used mainly for ifram compatibility
        //var name = arps.ucs[id].nome_completo; // API SisARP v0 field name
        var name = arps.ucs[id].nome_arp;
        var link = '<a href="https://uc.socioambiental.org/' + settings.lang + '/arp/' + id + '" target="_blank">' + name + '</a>';
        var type = 'ucs';
      }
      else {
        self.methods.debug('Info not available for ARP ' + id);
        return self.methods.message('no_info');
      }

      // Get fields along with i18 labels
      for (var field in arps[type][id]) {
        if (messages[settings.lang][type + '.marker.' + field] != undefined) {
          table[messages[settings.lang][type + '.marker.' + field]] = arps[type][id][field];
        }
      }

      if (complete == true || complete == undefined) {
        return '<div class="isamap-identify isamap-identify-arp">' + self.methods.htmlTable(link, table) + '</div>';
      }
      else {
        return name;
      }
    }

    // Layer definitions
    var defaultLayers = {
      tis: {
        groupName: self.methods.message('indigenous_lands'),
        expanded: false,
        layers: {
          limits: {
            description: self.methods.message('indigenous_lands') + self.methods.message('limits'),
            layer: L.esri.dynamicMapLayer({
              url:     gisLayers.tisLimits.url,
              proxy:   settings.proxyUrl,
              //token:   apiKey,
              layers:  [ gisLayers.tisLimits.layer ],
              opacity: 1,
            }),
            identify: function (error, featureCollection) {
              if (error || featureCollection.features.length === 0) {
                return false;
              } else {
                var content = self.methods.arpPopupContents(featureCollection.features[0]);

                return content;
              }
            },
            legend: [
              {
                html: '',
                style: {
                  "opacity"          : "1",
                  "background-color" : "#ff5500",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              }
            ],
          },
          markers: {
            description: self.methods.message('indigenous_lands') + self.methods.message('points'),
            layer: L.esri.featureLayer({
              url:          self.methods.gisLayerUrl('tisMarkers'),
              proxy:        settings.proxyUrl,
              //token:        apiKey,
              attribution:  self.methods.message('attribution'),
              pointToLayer: function (geojson, latlng) {
                return L.marker(latlng, {
                  icon: L.icon({
                    //iconUrl: settings.imagesUrl + '/ti/ti_feather.png',
                    //iconSize: [22, 28],
                    iconUrl:  self.methods.tiIcon(arps.tis[geojson.properties.id_arp]),
                    iconSize: [32, 32],
                  }),
                }).bindTooltip(self.methods.arpPopupContents(geojson, false));
              },
            }),
            click: function(e) {
              var content = self.methods.arpPopupContents(e.layer.feature);

              e.layer.bindPopup(content);
              e.layer.openPopup();
            },
            //legend: [
            //  {
            //    //label: self.methods.message('indigenous_lands') + self.methods.message('points'),
            //    //html: '<img src="'+ settings.imagesUrl + '/ti/ti_feather.png" />',
            //    html: '',
            //    style: {
            //      //"background-image" : "url('" + settings.imagesUrl + "/ti/ti_feather.png')",
            //      //"width"            : "22px",
            //      //"height"           : "28px"
            //      "background-image" : "url('" + settings.imagesUrl + "/ti/ti_square.png')",
            //      "width"            : "32px",
            //      "height"           : "32px"
            //    },
            //  }
            //],
            legend: [
              {
                label: self.methods.message('tis.identificacao'),
                html: '<img src="' + settings.imagesUrl + '/ti/ti_identificacao.png">',
              },
              {
                label: self.methods.message('tis.identificada'),
                html: '<img src="' + settings.imagesUrl + '/ti/ti_identificada.png">',
              },
              {
                label: self.methods.message('tis.declarada'),
                html: '<img src="' + settings.imagesUrl + '/ti/ti_declarada.png">',
              },
              {
                label: self.methods.message('tis.reservada'),
                html: '<img src="' + settings.imagesUrl + '/ti/ti_reservada.png">',
              },
              {
                label: self.methods.message('tis.restricao'),
                html: '<img src="' + settings.imagesUrl + '/ti/ti_restricao.png">',
              },
              {
                label: self.methods.message('tis.homologada'),
                html: '<img src="' + settings.imagesUrl + '/ti/ti_homologada.png">',
              },
              {
                label: self.methods.message('no_info'),
                html: '<img src="' + settings.imagesUrl + '/ti/ti_square.png">',
              }
            ],
          },
        },
      },
      ucs: {
        groupName: self.methods.message('conservation_areas'),
        expanded: false,
        layers: {
          limitsEstaduais: {
            description: self.methods.message('cus') + ' ' + self.methods.message('state') + self.methods.message('limits'),
            layer: L.esri.dynamicMapLayer({
              url:     gisLayers.ucsLimitsEstaduais.url,
              proxy:   settings.proxyUrl,
              //token:   apiKey,
              layers:  [ gisLayers.ucsLimitsEstaduais.layer ],
              opacity: 1,
            }),
            identify: function (error, featureCollection) {
              if (error || featureCollection.features.length === 0) {
                return false;
              } else {
                var content = self.methods.arpPopupContents(featureCollection.features[0]);

                return content;
              }
            },
            legend: [
              {
                html: '',
                style: {
                  "opacity"          : "1",
                  "background-color" : "#ff73df",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              }
            ],
          },
          limitsFederais: {
            description: self.methods.message('cus') + ' ' + self.methods.message('federal') +  self.methods.message('limits'),
            layer: L.esri.dynamicMapLayer({
              url:     gisLayers.ucsLimitsFederais.url,
              proxy:   settings.proxyUrl,
              //token:   apiKey,
              layers:  [ gisLayers.ucsLimitsFederais.layer ],
              opacity: 1,
            }),
            identify: function (error, featureCollection) {
              if (error || featureCollection.features.length === 0) {
                return false;
              } else {
                var content = self.methods.arpPopupContents(featureCollection.features[0]);

                return content;
              }
            },
            legend: [
              {
                html: '',
                style: {
                  "opacity"          : "1",
                  "background-color" : "#ffff00",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              }
            ],
          },
          markersEstaduais: {
            description:  self.methods.message('cus') + ' ' + self.methods.message('state') + self.methods.message('points'),
            layer: L.esri.featureLayer({
              url:          self.methods.gisLayerUrl('ucsMarkersEstaduais'),
              proxy:        settings.proxyUrl,
              //token:        apiKey,
              // Restrict markers only to State UCs inside the Legal Amazon
              //where:        'CONSEST_ISA_PL.amazonia = 1',
              attribution:  self.methods.message('attribution'),
              pointToLayer: function (geojson, latlng) {
                // This layer has a weird field naming scheme and we fix it manually to adapt to our standar
                geojson.properties.id_arp = geojson.properties['pontos_aps_isa_pt.id_arp'];

                return L.marker(latlng, {
                  icon: L.icon({
                    iconUrl: settings.imagesUrl + '/uc/estadual.png',
                    iconSize: [16, 24],
                  }),
                }).bindTooltip(self.methods.arpPopupContents(geojson, false));
              },
            }),
            click: function(e) {
              // This layer has a weird field naming scheme and we fix it manually to adapt to our standar
              e.layer.feature.properties.id_arp = e.layer.feature.properties['pontos_aps_isa_pt.id_arp'];
              var content = self.methods.arpPopupContents(e.layer.feature);

              e.layer.bindPopup(content);
              e.layer.openPopup();
            },
            legend: [
              {
                //label: self.methods.message('cus') + ' ' + self.methods.message('state') + self.methods.message('points'),
                //html: '<img src="'+ settings.imagesUrl + '/uc/estadual.png" />',
                html: '',
                style: {
                  "background-image" : "url('" + settings.imagesUrl + "/uc/estadual.png')",
                  "width"            : "16px",
                  "height"           : "24px"
                },
              }
            ],
          },
          markersFederais: {
            description: self.methods.message('cus') + ' ' + self.methods.message('federal') + self.methods.message('points'),
            layer: L.esri.featureLayer({
              url:          self.methods.gisLayerUrl('ucsMarkersFederais'),
              proxy:        settings.proxyUrl,
              //token:        apiKey,
              attribution:  self.methods.message('attribution'),
              pointToLayer: function (geojson, latlng) {
                return L.marker(latlng, {
                  icon: L.icon({
                    iconUrl: settings.imagesUrl + '/uc/federal.png',
                    iconSize: [16, 24],
                  }),
                }).bindTooltip(self.methods.arpPopupContents(geojson, false));
              },
            }),
            click: function(e) {
              var content = self.methods.arpPopupContents(e.layer.feature);

              e.layer.bindPopup(content);
              e.layer.openPopup();
            },
            legend: [
              {
                //label: self.methods.message('cus') + ' ' + self.methods.message('federal') + self.methods.message('points'),
                //html: '<img src="'+ settings.imagesUrl + '/uc/federal.png" />',
                html: '',
                style: {
                  "background-image" : "url('" + settings.imagesUrl + "/uc/federal.png')",
                  "width"            : "16px",
                  "height"           : "24px"
                },
              }
            ],
          },
      /*
        },
      },
      mosaics: {
        groupName: self.methods.message('mosaics_corridors'),
        expanded: false,
        layers: {
      */
          ucsMosaicosOutros: {
            //description: self.methods.message('mosaics_corridors'),
            description: self.methods.message('mosaics'),
            layer: L.esri.dynamicMapLayer({
              url:     gisLayers.ucsMosaicosCorredores.url,
              proxy:   settings.proxyUrl,
              //token:   apiKey,
              layers:  [ gisLayers.ucsMosaicosCorredores.layer ],
              opacity: 1,
            }),
            identify: function (error, featureCollection) {
              if (error || featureCollection.features.length === 0) {
                return false;
              } else {
                var attributes = featureCollection.features[0].properties;

                if (attributes.nome_mos == undefined) {
                  return '';
                }

                var content = self.methods.htmlTable(attributes.nome_mos, {
                  'Categoria' : attributes.categoria,
                  'Área (ha)' : attributes.area_ha,
                  'Status'    : attributes.status,
                  //'ID'      : attributes.id_mos,
                });

                return '<div class="isamap-identify isamap-identify-mosaicos">' + content + '</div>';
              }
            },
            legend: [
              {
                label: self.methods.message('mosaic'),
                html: '',
                style: {
                  "opacity"          : "1",
                  "background-color" : "#ff0000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: self.methods.message('corridor'),
                html: '',
                style: {
                  "opacity"          : "1",
                  "background-color" : "#0000ff",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
            ],
          },
          ramsar: {
            description: 'Sítios RAMSAR',
            layer: omnivore.kml(settings.baseUrl + 'data/ramsar/ramsar.kml').on('click', function(e) {
              self.methods.popup(e.latlng, e.layer.feature.properties.description);
            }),
          },
        }
      },
      infra: {
        groupName: self.methods.message('infrastructure'),
        expanded: false,
        layers: {
          energia: {
            description: self.methods.message('energy'),
            layer: L.esri.Cluster.featureLayer({
              url:          self.methods.gisLayerUrl('energia'),
              proxy:        settings.proxyUrl,
              //token:        apiKey,
              attribution   : 'ANEEL / MME',
              opacity:      0.8,
              polygonOptions: {
                color       : '#e57b08',
                weight      : 4,
                opacity     : 1,
                fillOpacity : 0.5
              },
              spiderfyOnMaxZoom: false,
              disableClusteringAtZoom: 9,
              iconCreateFunction: function (cluster) {
                var childCount = cluster.getChildCount();
                var c          = ' marker-cluster-';

                if (childCount < 10) {
                  c += 'small';
                } else if (childCount < 100) {
                  c += 'medium';
                } else {
                  c += 'large';
                }

                return new L.DivIcon({ html: '<div><img src="' + settings.imagesUrl + '/misc/energia.png"> <span><strong>' + childCount + '</strong></span></div>', className: 'energy-cluster marker-cluster' + c, iconSize: new L.Point(40, 40) });
              },
              pointToLayer: function (geojson, latlng) {
                return L.marker(latlng, {
                  icon: L.icon({
                    iconUrl: settings.imagesUrl + '/misc/energia.png',
                    iconSize: [22, 25],
                  }),
                });
              },
            }),
            click: function(e) {
              var attributes = e.layer.feature.properties;
              var content    = self.methods.htmlTable(attributes.categoria + " " + attributes.nomeobra, {
                'Combustivel':    attributes.tipocombustivel,
                'Estágio':        attributes.estagio,
                'Potência':       attributes.potenciakw,
                'Rio':            attributes.rio,
                'Coordenadas':    attributes.latitudegms + " " + attributes.longitudegms,
                'Município':      attributes.municipio1,
                'Ato Legal':      attributes.atolegal,
                'Processo ANEEL': attributes.processoaneel,
                'Proprietario':   attributes.proprietario,
              });

              e.layer.bindPopup(content);
              e.layer.openPopup();
            },
            legend: [
              {
                //label: self.methods.message('energy'),
                //html: '<img src="'+ settings.imagesUrl + '/misc/energia.png" />',
                html: '',
                style: {
                  "opacity"          : "0.8",
                  "background-image" : "url('" + settings.imagesUrl + "/misc/energia.png')",
                  "width"            : "22px",
                  "height"           : "25px"
                },
              }
            ],
          },
          mineracao: {
            description: self.methods.message('mining'),
            layer: L.esri.dynamicMapLayer({
              url:     gisLayers.mineracao.url,
              proxy:   settings.proxyUrl,
              //token:   apiKey,
              layers:  [ gisLayers.mineracao.layer ],
              opacity: 0.50,
            }),
            identify: function (error, featureCollection) {
              if (error || featureCollection.features.length === 0) {
                return false;
              } else {
                var attributes = featureCollection.features[0].properties;
                var content    = self.methods.htmlTable(self.methods.message('mining'), {
                  'Processo':   attributes.processo,
                  'Fase':       attributes.fase,
                  'Ano':        attributes.ano,
                  'Área (ha)':  attributes.area_ha,
                  'Substância': attributes.subs,
                  'Uso':        attributes.uso,
                  'UF':         attributes.uf,
                });

                return '<div class="isamap-identify isamap-identify-mineracao">' + content + '</div>';
              }
            },
            legend: [
              {
                label: self.methods.message('mining_interest'),
                html: '',
                style: {
                  "opacity"          : "0.5",
                  "background-color" : "#ffff00",
                  "border"           : "1px solid #ffff8f",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: self.methods.message('mining_availability'),
                html: '',
                style: {
                  "opacity"          : "0.5",
                  "background-color" : "#e58fff",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: self.methods.message('mining_request'),
                html: '',
                style: {
                  "opacity"          : "0.5",
                  "background-color" : "#bbff33",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: self.methods.message('mining_authorized'),
                html: '',
                style: {
                  "opacity"          : "0.5",
                  "background-color" : "#ff3333",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              /*
              {
                label: self.methods.message('mining_interest'),
                html: '<img src="' + settings.imagesUrl + '/mineracao/interesse-em-pesquisar.png">',
              },
              {
                label: self.methods.message('mining_availability'),
                html: '<img src="' + settings.imagesUrl + '/mineracao/em-pesquisa-disponibilidade.png">',
              },
              {
                label: self.methods.message('mining_request'),
                html: '<img src="' + settings.imagesUrl + '/mineracao/solicitacao-de-extracao.png">',
              },
              {
                label: self.methods.message('mining_authorized'),
                html: '<img src="' + settings.imagesUrl + '/mineracao/autorizacao-para-extracao.png">',
              }
              */
            ],
          },
          oleogas: {
            description: self.methods.message('petroleum'),
            layer: L.esri.dynamicMapLayer({
              url:     gisLayers.petroleo.url,
              proxy:   settings.proxyUrl,
              //token:   apiKey,
              layers:  [ gisLayers.petroleo.layer ],
              opacity: 0.45,
            }),
            identify: function (error, featureCollection) {
              if (error || featureCollection.features.length === 0) {
                return false;
              } else {
                var attributes = featureCollection.features[0].properties;
                var content    = self.methods.htmlTable(self.methods.message('petroleum'), {
                  'Área (ha)': attributes.area_oficial_ha,
                  'Bacia':     attributes.bacia,
                  'Companhia': attributes.cia,
                  'Fluido':    attributes.fluido,
                  'Nome':      attributes.nome,
                  'Rodada':    attributes.rodada,
                  'Situação':  attributes.situacao,
                });

                return '<div class="isamap-identify isamap-identify-petroleo">' + content + '</div>';
              }
            },
            legend: [
              {
                label: self.methods.message('petroleum_prod'),
                html: '',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#ff0000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: self.methods.message('petroleum_exp'),
                html: '',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#ff8000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: self.methods.message('petroleum_dev'),
                html: '',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#ffbf00",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: self.methods.message('petroleum_lic'),
                html: '',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#ffff00",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              /*
              {
                label: self.methods.message('petroleum_dev'),
                html: '<img src="' + settings.imagesUrl + '/petroleo/petroleo-dev.png">',
              },
              {
                label: self.methods.message('petroleum_prod'),
                html: '<img src="' + settings.imagesUrl + '/petroleo/petroleo-prod.png">',
              },
              {
                label: self.methods.message('petroleum_exp'),
                html: '<img src="' + settings.imagesUrl + '/petroleo/petroleo-exp.png">',
              },
              {
                label: self.methods.message('petroleum_lic'),
                html: '<img src="' + settings.imagesUrl + '/petroleo/petroleo-lic.png">',
              }
              */
            ],
          },
        },
      },
      jurisdicao: {
        groupName: self.methods.message('jurisdiction'),
        expanded: false,
        layers: {
          amlegal: {
            //layer: L.esri.featureLayer({
            //  url:   gisServices.tematicos + '/LimiteAmazonia/MapServer/0',
            //  //token: apiKey,
            //  proxy: settings.proxyUrl,
            //}),
            description: self.methods.message('amlegal'),
            layer: L.esri.dynamicMapLayer({
              url:     gisLayers.amlegal.url,
              proxy:   settings.proxyUrl,
              //token:   apiKey,
              layers:  [ gisLayers.amlegal.layer ],
              opacity: 0.35,
            }),
            legend: [
              {
                html: '',
                style: {
                  "opacity"          : "0.35",
                  "background-color" : "#b0f58d",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              }
            ],
          },
        },
      },
      ambiente: {
        groupName: self.methods.message('environment'),
        expanded: false,
        layers: {
          biomas: {
            description: self.methods.message('biomes'),
            layer: L.esri.dynamicMapLayer({
              url:     gisLayers.biomas.url,
              proxy:   settings.proxyUrl,
              //token:   apiKey,
              layers:  [ gisLayers.biomas.layer ],
              opacity: 0.35,
            }),
            //bindPopup: function (error, featureCollection) {
            //  if (error || featureCollection.features.length === 0) {
            //    return false;
            //  } else {
            //    var attributes = featureCollection.features[0].properties;
            //    var content    = self.methods.htmlTable('Bioma', {
            //      'Nome':      attributes.nome,
            //      'Área (ha)': attributes.area_ha,
            //    });

            //    return content;
            //  }
            //},
            identify: function (error, featureCollection) {
              if (error || featureCollection.features.length === 0) {
                return false;
              } else {
                for (var i in featureCollection.features) {
                  if (featureCollection.features[i].properties.nome != undefined) {
                    var content    = self.methods.htmlTable(self.methods.message('biomes'), {
                      'Nome':      featureCollection.features[i].properties.nome,
                      'Área (ha)': featureCollection.features[i].properties.area_ha,
                    });

                    return '<div class="isamap-identify isamap-identify-biomas">' + content + '</div>';
                  }
                }
              }
            },
            legend: [
              {
                label: self.methods.message('biomes_amazon'),
                html: '',
                style: {
                  "opacity"          : "0.35",
                  "background-color" : "#267300",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: self.methods.message('biomes_caatinga'),
                html: '',
                style: {
                  "opacity"          : "0.35",
                  "background-color" : "#e6e600",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: self.methods.message('biomes_cerrado'),
                html: '',
                style: {
                  "opacity"          : "0.35",
                  "background-color" : "#ffaa00",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: self.methods.message('biomes_atlantic'),
                html: '',
                style: {
                  "opacity"          : "0.35",
                  "background-color" : "#38a800",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: self.methods.message('biomes_pampa'),
                html: '',
                style: {
                  "opacity"          : "0.35",
                  "background-color" : "#a3ff73",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: self.methods.message('biomes_wetland'),
                html: '',
                style: {
                  "opacity"          : "0.35",
                  "background-color" : "#00a9e6",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              /*
              {
                label: self.methods.message('biomes_amazon'),
                html: '<img src="' + settings.imagesUrl + '/biomas/amazonia.png">',
              },
              {
                label: self.methods.message('biomes_caatinga'),
                html: '<img src="' + settings.imagesUrl + '/biomas/caatinga.png">',
              },
              {
                label: self.methods.message('biomes_cerrado'),
                html: '<img src="' + settings.imagesUrl + '/biomas/cerrado.png">',
              },
              {
                label: self.methods.message('biomes_atlantic'),
                html: '<img src="' + settings.imagesUrl + '/biomas/mata_atlantica.png">',
              },
              {
                label: self.methods.message('biomes_pampa'),
                html: '<img src="' + settings.imagesUrl + '/biomas/pampa.png">',
              },
              {
                label: self.methods.message('biomes_wetland'),
                html: '<img src="' + settings.imagesUrl + '/biomas/pantanal.png">',
              },
              */
            ],
          },
          cavernas: {
            description: self.methods.message('caves'),
            layer: L.esri.featureLayer({
              url:          self.methods.gisLayerUrl('cavernas'),
              proxy:        settings.proxyUrl,
              //token:        apiKey,
              attribution:  'CECAV / ICMBio / MMA',
              opacity:      0.45,
              pointToLayer: function (geojson, latlng) {
                return L.marker(latlng, {
                  icon: L.icon({
                    iconUrl: settings.imagesUrl + '/misc/caverna.png',
                    iconSize: [12, 11],
                  }),
                });
              },
            }),
            click: function(e) {
              var attributes = e.layer.feature.properties;
              var content    = self.methods.htmlTable(attributes.caverna, {
                'State':    attributes.uf,
                'Locality': attributes.localidade,
              });

              e.layer.bindPopup(content);
              e.layer.openPopup();
            },
            legend: [
              {
                //label: self.methods.message('caves'),
                //html: '<img src="' + settings.imagesUrl + '/misc/caverna.png">',
                html: '',
                style: {
                  "opacity"          : "0.45",
                  "background-image" : "url('" + settings.imagesUrl + "/misc/caverna.png')",
                  "width"            : "12px",
                  "height"           : "11px"
                },
              }
            ],
          },
          fitofisionomia: {
            description: self.methods.message('vegetation'),
            layer: L.esri.dynamicMapLayer({
              url:     gisLayers.fitofisionomia.url,
              proxy:   settings.proxyUrl,
              //token:   apiKey,
              layers:  [ gisLayers.fitofisionomia.layer ],
              opacity: 0.45,
            }),
            //bindPopup: function (error, featureCollection) {
            //  if (error || featureCollection.features.length === 0) {
            //    return false;
            //  } else {
            //    var attributes = featureCollection.features[0].properties;
            //    var content    = self.methods.htmlTable('Fitofisionomia', {
            //      'Fitofisionomia': attributes.grupo1,
            //      'Fitofisionomia': attributes.Fitofisionomia,
            //    });

            //    return content;
            //  }
            //},
            identify: function (error, featureCollection) {
              if (error || featureCollection.features.length === 0) {
                return false;
              } else {
                var content = '';
                var levels  = {};

                /*
                var content_nv0 = null;
                var content_nv1 = null;
                var content_nv3 = null;
                */

                for (var i in featureCollection.features) {
                  //content += self.methods.htmlTable('Fitofisionomia', {
                  //  'Nível 0': featureCollection.features[i].properties.Legenda_nv0,
                  //  'Nível 1': featureCollection.features[i].properties.Legenda_nv1,
                  //  'Nível 3': featureCollection.features[i].properties.Legenda_nv3,
                  //});

                  /*
                  if (featureCollection.features[i].properties.Legenda_nv0 != undefined) {
                    content_nv0 = featureCollection.features[i].properties.Legenda_nv0;
                  }

                  if (featureCollection.features[i].properties.Legenda_nv1 != undefined) {
                    content_nv1 = featureCollection.features[i].properties.Legenda_nv1;
                  }

                  if (featureCollection.features[i].properties.Legenda_nv3 != undefined) {
                    content_nv3 = featureCollection.features[i].properties.Legenda_nv3;
                  }
                  */

                  if (featureCollection.features[i].properties.Fitofisionomia != undefined) {
                    levels['Nível ' + i] = featureCollection.features[i].properties.Fitofisionomia;
                  }
                }

                /*
                content += self.methods.htmlTable('Fitofisionomia', {
                  'Nível 0': content_nv0,
                  'Nível 1': content_nv1,
                  'Nível 3': content_nv3,
                });
                */

                content += self.methods.htmlTable('Fitofisionomia', levels);

                return '<div class="isamap-identify isamap-identify-fitofisionomia">' + content + '</div>';
              }
            },
            legend: [
              {
                label: self.methods.message('vegetation_campinarana'),
                html: '',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#d7995e",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: self.methods.message('vegetation_contact'),
                html: '',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#fffe39",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: self.methods.message('vegetation_veld'),
                html: '',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#ffb898",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: self.methods.message('vegetation_decidual'),
                html: '',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#b9b933",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: self.methods.message('vegetation_semidecidual'),
                html: '',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#8f9033",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: self.methods.message('vegetation_open_ombrophylous'),
                html: '',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#79fe35",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: self.methods.message('vegetation_dense_ombrophylous'),
                html: '',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#6ec664",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: self.methods.message('vegetation_mixed_ombrophylous'),
                html: '',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#b5ff8e",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: self.methods.message('vegetation_pioneer_formations'),
                html: '',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#3373b9",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: self.methods.message('vegetation_ecological_refugees'),
                html: '',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#f0f7a9",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: self.methods.message('vegetation_savanna'),
                html: '',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#ffbb34",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: self.methods.message('vegetation_veldt_savanna'),
                html: '',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#b98d34",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              /*
              {
                label: self.methods.message('vegetation_campinarana'),
                html: '<img src="' + settings.imagesUrl + '/fitofisionomia/campinarana.png">',
              },
              {
                label: self.methods.message('vegetation_contact'),
                html: '<img src="' + settings.imagesUrl + '/fitofisionomia/contato.png">',
              },
              {
                label: self.methods.message('vegetation_veld'),
                html: '<img src="' + settings.imagesUrl + '/fitofisionomia/estepe.png">',
              },
              {
                label: self.methods.message('vegetation_decidual'),
                html: '<img src="' + settings.imagesUrl + '/fitofisionomia/floresta-estacional-decidual.png">',
              },
              {
                label: self.methods.message('vegetation_semidecidual'),
                html: '<img src="' + settings.imagesUrl + '/fitofisionomia/floresta-estacional-semidecidual.png">',
              },
              {
                label: self.methods.message('vegetation_open_ombrophylous'),
                html: '<img src="' + settings.imagesUrl + '/fitofisionomia/floresta-ombrofila-aberta.png">',
              },
              {
                label: self.methods.message('vegetation_dense_ombrophylous'),
                html: '<img src="' + settings.imagesUrl + '/fitofisionomia/floresta-ombrofila-densa.png">',
              },
              {
                label: self.methods.message('vegetation_mixed_ombrophylous'),
                html: '<img src="' + settings.imagesUrl + '/fitofisionomia/floresta-ombrofila-mista.png">',
              },
              {
                label: self.methods.message('vegetation_pioneer_formations'),
                html: '<img src="' + settings.imagesUrl + '/fitofisionomia/formacoes-pioneiras.png">',
              },
              {
                label: self.methods.message('vegetation_ecological_refugees'),
                html: '<img src="' + settings.imagesUrl + '/fitofisionomia/refugio-ecologico.png">',
              },
              {
                label: self.methods.message('vegetation_savanna'),
                html: '<img src="' + settings.imagesUrl + '/fitofisionomia/savana.png">',
              },
              {
                label: self.methods.message('vegetation_veldt_savanna'),
                html: '<img src="' + settings.imagesUrl + '/fitofisionomia/savana-estepica.png">',
              }
              */
            ],
          },
          /*
          bacias: {
            description: self.methods.message('watersheds'),
            layer: L.esri.dynamicMapLayer({
              url:     gisLayers.bacias.url,
              proxy:   settings.proxyUrl,
              //token:   apiKey,
              layers:  [ gisLayers.bacias.layer ],
              opacity: 0.45,
            }),
            //bindPopup: function (error, featureCollection) {
            //  if (error || featureCollection.features.length === 0) {
            //    return false;
            //  } else {
            //    var attributes = featureCollection.features[0].properties;
            //    var content    = self.methods.htmlTable('Bacia', {
            //      'Bacia':  attributes.bacia,
            //      'Bacia':  attributes['BACIA (NIVEL 2)'],
            //      'Região': attributes.rg_hidro,
            //      'Região': attributes['REGIÃO HIDROGRÁFICA (NIVEL 1)'],
            //    });

            //    return content;
            //  }
            //},
            identify: function (error, featureCollection) {
              if (error || featureCollection.features.length === 0) {
                return false;
              } else {
                var content = '';

                for (var i in featureCollection.features) {
                  if (featureCollection.features[i].properties.nunivotto1 != undefined) {
                    content += self.methods.htmlTable('Bacia Nível 1', {
                      'Nome':      featureCollection.features[i].properties['Nome da Bacia'],
                      'Área (ha)': featureCollection.features[i].properties.area_ha,
                    });
                  }
                  else if (featureCollection.features[i].properties['Nível Otto 2'] != undefined) {
                    content += self.methods.htmlTable('Bacia Nível 2', {
                      'Nome':      featureCollection.features[i].properties['Nome da Bacia'],
                      'Área (ha)': featureCollection.features[i].properties.area_ha,
                    });
                  }
                  else if (featureCollection.features[i].properties.nunivotto3 != undefined) {
                    content += self.methods.htmlTable('Bacia Nível 3', {
                      'Nome':      featureCollection.features[i].properties['Nome da Bacia'],
                      'Área (ha)': featureCollection.features[i].properties.area_ha,
                    });
                  }
                }

                return '<div class="isamap-identify isamap-identify-bacias">' + content + '</div>';
              }
            },
            legend: [
              {
                label: self.methods.message('basin_amazon'),
                html: '<img src="' + settings.imagesUrl + '/bacias/bacia-amazonica.png">',
              },
              {
                label: self.methods.message('basin_araguaia_tocantins'),
                html: '<img src="' + settings.imagesUrl + '/bacias/bacia-araguaia-tocantins.png">',
              },
              {
                label: self.methods.message('basin_east_atlantic'),
                html: '<img src="' + settings.imagesUrl + '/bacias/bacia-atlantico-leste.png">',
              },
              {
                label: self.methods.message('basin_western_northeast'),
                html: '<img src="' + settings.imagesUrl + '/bacias/bacia-atlantico-nordeste-ocidental.png">',
              },
              {
                label: self.methods.message('basin_atlantic_southeast'),
                html: '<img src="' + settings.imagesUrl + '/bacias/bacia-atlantico-sudeste.png">',
              },
              {
                label: self.methods.message('basin_south_atlantico'),
                html: '<img src="' + settings.imagesUrl + '/bacias/bacia-atlantico-sul.png">',
              },
              {
                label: self.methods.message('basin_paraguay'),
                html: '<img src="' + settings.imagesUrl + '/bacias/bacia-paraguai.png">',
              },
              {
                label: self.methods.message('basin_parana'),
                html: '<img src="' + settings.imagesUrl + '/bacias/bacia-parana.png">',
              },
              {
                label: self.methods.message('basin_parnaiba'),
                html: '<img src="' + settings.imagesUrl + '/bacias/bacia-parnaiba.png">',
              },
              {
                label: self.methods.message('basin_sao_francisco'),
                html: '<img src="' + settings.imagesUrl + '/bacias/bacia-sao-francisco.png">',
              },
              {
                label: self.methods.message('basin_uruguay'),
                html: '<img src="' + settings.imagesUrl + '/bacias/bacia-uruguai.png">',
              }
            ],
          },
          */
          ottobacias: {
            description: self.methods.message('otto'),
            layer: L.esri.dynamicMapLayer({
              url:     gisLayers.ottobacias.url,
              proxy:   settings.proxyUrl,
              //token:   apiKey,
              layers:  [ gisLayers.ottobacias.layer ],
              opacity: 0.45,
            }),
            identify: function (error, featureCollection) {
              if (error || featureCollection.features.length === 0) {
                return false;
              } else {
                var content = '';

                for (var i in featureCollection.features) {
                  if (featureCollection.features[i].properties.nunivotto1 != undefined) {
                    content += self.methods.htmlTable('Bacia Nível 1', {
                      'Nome':      featureCollection.features[i].properties['Nome da Bacia'],
                      'Área (ha)': featureCollection.features[i].properties.area_ha,
                    });
                  }
                  else if (featureCollection.features[i].properties['Nível Otto 2'] != undefined) {
                    content += self.methods.htmlTable('Bacia Nível 2', {
                      'Nome':      featureCollection.features[i].properties['Nome da Bacia'],
                      'Área (ha)': featureCollection.features[i].properties.area_ha,
                    });
                  }
                  else if (featureCollection.features[i].properties.nunivotto3 != undefined) {
                    content += self.methods.htmlTable('Bacia Nível 3', {
                      'Nome':      featureCollection.features[i].properties['Nome da Bacia'],
                      'Área (ha)': featureCollection.features[i].properties.area_ha,
                    });
                  }
                }

                return '<div class="isamap-identify isamap-identify-ottobacias">' + content + '</div>';
              }
            },
          },
        },
      },
      queimadas: {
        groupName: self.methods.message('fires'),
        expanded: false,
        layers: {
          focos: {
            description: self.methods.message('fire'),
            layer: L.esri.Cluster.featureLayer({
              url:          self.methods.gisLayerUrl('focos'),
              proxy:        settings.proxyUrl,
              //token:        apiKey,
              attribution:  '<a href="http://www.inpe.br/">INPE</a>',
              // Using only the reference satelite
              // See http://www.inpe.br/queimadas/portal/informacoes/perguntas-frequentes
              where:        "satelite = 'AQUA_M-T'",
              polygonOptions: {
                color       : 'red',
                weight      : 4,
                opacity     : 1,
                fillOpacity : 0.5
              },
              spiderfyOnMaxZoom: false,
              disableClusteringAtZoom: 9,
              iconCreateFunction: function (cluster) {
                var childCount = cluster.getChildCount();
                var c          = ' marker-cluster-';

                if (childCount < 10) {
                  c += 'small';
                } else if (childCount < 100) {
                  c += 'medium';
                } else {
                  c += 'large';
                }

                return new L.DivIcon({ html: '<div><img src="' + settings.imagesUrl + '/misc/focos.png"> <span><strong>' + childCount + '</strong></span></div>', className: 'fire-cluster energy-cluster marker-cluster' + c, iconSize: new L.Point(40, 40) });
              },
              pointToLayer: function (geojson, latlng) {
                return L.marker(latlng, {
                  icon: L.icon({
                    iconUrl: settings.imagesUrl + '/misc/focos.png',
                    iconSize: [22, 23],
                  }),
                });
              },
            }),
            click: function(e) {
              var lang       = settings.lang;
              var attributes = e.layer.feature.properties;

              // Date is UTC unix timestamp with miliseconds
              // We have to convert it to human-readable date and sum up with the timezone offset
              //
              // See http://forums.esri.com/Thread.asp?c=158&f=2397&t=299440
              var zone    = new Date();
              var offset  = zone.getTimezoneOffset() * 60 * 1000;
              var date    = new Date(attributes.data + offset);
              var day     = date.getDate();
              var month   = date.getMonth() + 1;
              var year    = date.getFullYear();

              // Simple locale formatting
              if (lang == 'en') {
                var format = month + '/' + day + '/' + year;
              }
              else {
                var format = day + '/' + month + '/' + year;
              }

              var content = self.methods.htmlTable('Foco de calor', {
                'Coordenadas': attributes.latgms + ' - ' + attributes.longgms,
                'Data':        format,
                'Satélite':    attributes.satelite,
                //'Vegetação': attributes.vegetacao,
              });

              e.layer.bindPopup(content);
              e.layer.openPopup();
            },
            legend: [
              {
                //label: self.methods.message('fire'),
                //html: '<img src="'+ settings.imagesUrl + '/misc/focos.png" />',
                html: '',
                style: {
                  "opacity"          : "1",
                  "background-image" : "url('" + settings.imagesUrl + "/misc/focos.png')",
                  "width"            : "22px",
                  "height"           : "23px"
                },
              }
            ],
          },
          focosDensidade: {
            description: self.methods.message('fire_density'),
            layer: L.esri.Heat.featureLayer({
              url         : self.methods.gisLayerUrl('focos'),
              proxy       : settings.proxyUrl,
              //token       : apiKey,
              // Using only the reference satelite
              // See http://www.inpe.br/queimadas/portal/informacoes/perguntas-frequentes
              where       : "satelite = 'AQUA_M-T'",
              fields      : ['oid'],
              minOpacity  : 0.4,
              maxZoom     : 15,
              max         : 0.9,
              radius      : 20,
              blur        : 10,
              gradient    : {0.5: 'gold', 0.7: 'orange', 0.9: '#bd0026'},
              attribution : '<a href="http://www.inpe.br/">INPE</a>'
            }),
            legend: [
              {
                html: '',
                style: {
                  "background-color" : "#bd0026",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              }
            ],
          },
        },
      },
      desmatamento: {
        groupName: self.methods.message('deforestation'),
        expanded: false,
        // This will be autoset by the group ID during initialization
        //sliderId: 'desmatamento',
        slider: {
          type             : 'single',
          from             : 0,
          to               : 2017,
          grid             : true,
          keyboard         : true,
          groupIndex       : 6,
          groupPrefix      : 'd',
          prettify_enabled : true,
        },
        layers: {
          0: {
            description: "0000",
            layer: L.esri.dynamicMapLayer({
              url:     gisServices.desmatamento,
              proxy:   settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 9999 ],
              opacity: 0.45,
            }),
          },
          1: {
            description: '2000',
            layer: L.esri.dynamicMapLayer({
              url:     gisServices.desmatamento,
              proxy:   settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 0 ],
              opacity: 0.45,
            }),
            legend: [
              {
                html: self.methods.message('deforestation') + ' 2000',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#800000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
            ],
          },
          2: {
            description: '2001',
            layer: L.esri.dynamicMapLayer({
              url:     gisServices.desmatamento,
              proxy:   settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 1 ],
              opacity: 0.45,
            }),
            legend: [
              {
                html: self.methods.message('deforestation') + ' 2001',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#800000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
            ],
          },
          3: {
            description: '2002',
            layer: L.esri.dynamicMapLayer({
              url:     gisServices.desmatamento,
              proxy:   settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 2 ],
              opacity: 0.45,
            }),
            legend: [
              {
                html: self.methods.message('deforestation') + ' 2002',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#800000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
            ],
          },
          4: {
            description: '2003',
            layer: L.esri.dynamicMapLayer({
              url:     gisServices.desmatamento,
              proxy:   settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 3 ],
              opacity: 0.45,
            }),
            legend: [
              {
                html: self.methods.message('deforestation') + ' 2003',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#800000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
            ],
          },
          5: {
            description: '2004',
            layer: L.esri.dynamicMapLayer({
              url:     gisServices.desmatamento,
              proxy:   settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 4 ],
              opacity: 0.45,
            }),
            legend: [
              {
                html: self.methods.message('deforestation') + ' 2004',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#800000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
            ],
          },
          6: {
            description: '2005',
            layer: L.esri.dynamicMapLayer({
              url:     gisServices.desmatamento,
              proxy:   settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 5 ],
              opacity: 0.45,
            }),
            legend: [
              {
                html: self.methods.message('deforestation') + ' 2005',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#800000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
            ],
          },
          7: {
            description: '2006',
            layer: L.esri.dynamicMapLayer({
              url:     gisServices.desmatamento,
              proxy:   settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 6 ],
              opacity: 0.45,
            }),
            legend: [
              {
                html: self.methods.message('deforestation') + ' 2006',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#800000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
            ],
          },
          8: {
            description: '2007',
            layer: L.esri.dynamicMapLayer({
              url:     gisServices.desmatamento,
              proxy:   settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 7 ],
              opacity: 0.45,
            }),
            legend: [
              {
                html: self.methods.message('deforestation') + ' 2007',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#800000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
            ],
          },
          9: {
            description: '2008',
            layer: L.esri.dynamicMapLayer({
              url:     gisServices.desmatamento,
              proxy:   settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 8 ],
              opacity: 0.45,
            }),
            legend: [
              {
                html: self.methods.message('deforestation') + ' 2008',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#800000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
            ],
          },
          10: {
            description: '2009',
            layer: L.esri.dynamicMapLayer({
              url:     gisServices.desmatamento,
              proxy:   settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 9 ],
              opacity: 0.45,
            }),
            legend: [
              {
                html: self.methods.message('deforestation') + ' 2009',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#800000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
            ],
          },
          11: {
            description: '2010',
            layer: L.esri.dynamicMapLayer({
              url:     gisServices.desmatamento,
              proxy:   settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 10 ],
              opacity: 0.45,
            }),
            legend: [
              {
                html: self.methods.message('deforestation') + ' 2010',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#800000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
            ],
          },
          12: {
            description: '2011',
            layer: L.esri.dynamicMapLayer({
              url:     gisServices.desmatamento,
              proxy:   settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 11 ],
              opacity: 0.45,
            }),
            legend: [
              {
                html: self.methods.message('deforestation') + ' 2011',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#800000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
            ],
          },
          13: {
            description: '2012',
            layer: L.esri.dynamicMapLayer({
              url:     gisServices.desmatamento,
              proxy:   settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 12 ],
              opacity: 0.45,
            }),
            legend: [
              {
                html: self.methods.message('deforestation') + ' 2012',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#800000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
            ],
          },
          14: {
            description: '2013',
            layer: L.esri.dynamicMapLayer({
              url:     gisServices.desmatamento,
              proxy:   settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 13 ],
              opacity: 0.45,
            }),
            legend: [
              {
                html: self.methods.message('deforestation') + ' 2013',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#800000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
            ],
          },
          15: {
            description: '2014',
            layer: L.esri.dynamicMapLayer({
              url:     gisServices.desmatamento,
              proxy:   settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 14 ],
              opacity: 0.45,
            }),
            legend: [
              {
                html: self.methods.message('deforestation') + ' 2014',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#800000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
            ],
          },
          16: {
            description: '2015',
            layer: L.esri.dynamicMapLayer({
              url:     gisServices.desmatamento,
              proxy:   settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 15 ],
              opacity: 0.45,
            }),
            legend: [
              {
                html: self.methods.message('deforestation') + ' 2015',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#800000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
            ],
          },
          17: {
            description: '2016',
            layer: L.esri.dynamicMapLayer({
              url:     gisServices.desmatamento,
              proxy:   settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 16 ],
              opacity: 0.45,
            }),
            legend: [
              {
                html: self.methods.message('deforestation') + ' 2016',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#800000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
            ],
          },
          18: {
            description: '2017',
            layer: L.esri.dynamicMapLayer({
              url:     gisServices.desmatamento,
              proxy:   settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 17 ],
              opacity: 0.45,
            }),
            legend: [
              {
                html: self.methods.message('deforestation') + ' 2017',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#800000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
            ],
          },
          19: {
            description: '2018',
            layer: L.esri.dynamicMapLayer({
              url:     gisServices.desmatamento,
              proxy:   settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 18 ],
              opacity: 0.45,
            }),
            legend: [
              {
                html: self.methods.message('deforestation') + ' 2018',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#800000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
            ],
          },
        },
      },
    };

    return self.methods;
  }

  /**
   * jQuery plugin wrapper.
   */
  $.fn.isaMap = function(options) {
    if (options == undefined) {
      var options = {};
    }
    else if (jQuery.isEmptyObject(options)) {
      options = {};
    }

    // Set current instance
    var current = $(this).attr('id');

    // Initialize and dispatch
    if (instances[current] == null) {
      options.overlaySelector = '#' + current;
      instances[current]      = {};

      // Do a shallow and not a recursive copy, otherwise array parameters would be merged
      //instances[current].settings = $.extend(true, {}, defaultSettings, options);
      instances[current].settings = $.extend({}, defaultSettings, options);

      // Additional defaults
      if (instances[current].settings.overlayMessage == undefined) {
        instances[current].settings.overlayMessage  = '<span><img class="jquery-isamap-mini-loading" src="' + instances[current].settings.imagesUrl + '/misc/mini-loading.gif" /> ';
        instances[current].settings.overlayMessage += messages[instances[current].settings.lang].loading + "</span>";
      }

      // Setup isaMap instance
      instances[current].settings.mapId = current;
      instances[current].methods        = new myIsaMap(instances[current].settings);
      instances[current].methods.initialize();
    }
    else {
      // See http://www.iainjmitchell.com/blog/exposing-jquery-functions/
      //     http://stackoverflow.com/questions/12880256/jquery-plugin-creation-and-public-facing-methods
      if (typeof arguments[0] === 'string') {
        var property = arguments[1];
        var args     = Array.prototype.slice.call(arguments);
        args.splice(0, 1);

        if (instances[current].methods[arguments[0]] != undefined) {
          return instances[current].methods[arguments[0]].apply(this, args);
        }
        else {
          console.error('[jquery.isamap] [#' + current + '] Invalid method ' + arguments[0]);
        }
      }
    }
  };

  // Plugin initialization
  jQuery(document).ready(function() {
    // Block UI
    jQuery('.isamap').block({
      message : '<span><img src="' + defaultSettings.baseUrl + 'images/misc/loading.gif" /></span>',
      css     : blockUIStyle,
    });

    // Load Metadata
    loadMetadata();
  });
}(jQuery));
