'use strict';

jQuery(document).on('isaMapLoaded', function() {
  // API credentials
  var apiBasicAuth   = 'mapa:WQxilp83J/9AQA==';
  var apiKey         = 'oyfMkFqC2CTS6xQzYWWtnZqgjKM9t67I';

  // Custom services
  var gisServer       = 'https://geo.socioambiental.org/webadaptor2/rest/services/PilotoGarimpo/garimpo_ilegal/MapServer';
  var sisRaisgNewsUrl = 'https://api.socioambiental.org/sisraisg/v1/geojson/noticias/1?apikey=' + apiKey;
  var sisArpNewsUrl   = "https://api.socioambiental.org/snippets/garimpo/noticias/";
  var defaultImages   = 'https://mapa.eco.br/images/';
  var customImages    = defaultImages + 'custom/garimpo/';

  // Additional parameters
  var news = {
    sisraisg : {},
    sisarp   : {},
  };

  // Localized messages
  var messages = {
    'pt-br': {
      'disclaimer': 'Esta aplicação é experimental',
      'agree'     : 'Concordo',
    },
    'es': {
      'disclaimer': 'Este sitio es una iniciativa de la RAISG, que busca la difícil tarea de mapear las evidencias de minería ilegal que afectan a los Territorios Indígenas y las Áreas Naturales Protegidas de la Amazonía. Es un mapa en constante actualización, razón por la cual agradecemos la comunicación de omisiones o la disponibilidad de información que pueda ser incorporada para <a href="mailto:raisgmail@socioambiental.org" style="color: #fff;">raisgmail@socioambiental.org</a>.',
      'agree'     : 'De acuerdo',
    },
    'en': {
      'disclaimer': 'This application is experimental',
      'agree'     : 'I agree',
    }
  }

  //function message(key) {
  //  if (messages[settings.lang][key] != undefined) {
  //    return messages[settings.lang][key];
  //  }
  //}

  /**
   * Load metadata, making sure needed resources are available.
   */
  function loadData() {
    $(document).on('isaMapLoadedDataGarimpo', function() {
      // Check if we have all the needed resources.
      //if (Object.getOwnPropertyNames(news.sisraisg).length > 0 && Object.getOwnPropertyNames(news.sisarp).length > 0) {
      if (Object.getOwnPropertyNames(news.sisraisg).length) {
        //jQuery('.isamap').unblock();
        $(document).trigger('isaMapLoadedGarimpo');
      }
    });

    // Retrieve data
    getSisRaisgNewsData();
    //getSisArpNewsData();
  }

  /**
   * Retrieve SisRAISG news data.
   */
  function getSisRaisgNewsData() {
    if (Object.getOwnPropertyNames(news.sisraisg).length > 0) {
      $(document).trigger('isaMapLoadedDataGarimpo');
      return;
    }

    jQuery.ajax({
      //xhrFields: {
      //  withCredentials: true,
      //},
      //headers: {
      //  'Authorization': 'Basic ' + btoa(apiBasicAuth),
      //},
      //data: {
      //  'param': 'value',
      //},
      url: sisRaisgNewsUrl,
    }).done(function(result) {
      news.sisraisg = result;

      $(document).trigger('isaMapLoadedDataGarimpo');
    });
  }

  /**
   * Retrieve SisARP news data.
   */
  function getSisArpNewsData() {
    if (Object.getOwnPropertyNames(news.sisarp).length > 0) {
      $(document).trigger('isaMapLoadedDataGarimpo');
      return;
    }

    jQuery.ajax({
      xhrFields: {
        withCredentials: true,
      },
      headers: {
        'Authorization': 'Basic ' + btoa(apiBasicAuth),
      },
      url: sisArpNewsUrl,
    }).done(function(result) {
      news.sisarp = result;

      $(document).trigger('isaMapLoadedDataGarimpo');
    });
  }

  // Custom data is loaded and we can pass it to the map plugin
  jQuery(document).on('isaMapLoadedGarimpo', function() {
    // Custom layer definitions
    var layers = {
      noticias: {
        groupName: 'Noticias',
        expanded: true,
        layers: {
          sisraisg: {
            description: 'Noticias - SisRAISG',
            layer: L.geoJSON(news.sisraisg, {
              pointToLayer: function (feature, latlng) {
                //var geojsonMarkerOptions = {
                //  radius      : 8,
                //  fillColor   : "#ff7800",
                //  color       : "#000",
                //  weight      : 1,
                //  opacity     : 1,
                //  fillOpacity : 0.8
                //};

                //return L.circleMarker(latlng, geojsonMarkerOptions);

                // See https://github.com/pointhi/leaflet-color-markers
                var icon = new L.Icon({
                  iconUrl     : 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-red.png',
                  shadowUrl   : 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
                  iconSize    : [25, 41],
                  iconAnchor  : [12, 41],
                  popupAnchor : [1, -34],
                  shadowSize  : [41, 41]
                });

                return L.marker(latlng, {
                  icon: icon,
                });
              },
              onEachFeature: function(feature, layer) {
                if (feature.properties) {
                  // Ensure links goes to a new tab, very useful when the map is inside an iframes
                  //var link = jQuery.parseHTML(feature.properties.name).attr('target', '_blank');
                  var link       = jQuery.parseHTML(feature.properties.name);
                  link[0].target = '_blank';

                  // Use mineria.amazoniasocioambiental.org application instead of the backend
                  // This might also be changed in the backend directly, but in that case it would
                  // apply also for content in categories other than illegal mining.
                  link[0].href = link[0].href.replace('sisraisg', 'mineria').replace('node/', 'noticia/#');

                  var content  = '<table><tr><th>Noticia</th></tr>';
                      content += '<tr><td>Título: </td><td>'      + jQuery(link).prop('outerHTML')  + '</td></tr>';
                      content += '<tr><td>Descripción: </td><td>' + feature.properties.description  + '</td></tr>';
                      content += '<tr><td>Fecha: </td><td>'       + feature.properties.field_fecha  + '</td></tr>';
                      content += '<tr><td>Fuente: </td><td>'      + feature.properties.field_fuente + '</td></tr>';
                      content += '</table>';

                  layer.bindPopup(content);
                }
              },
            }),
            legend: [
              {
                label: 'Noticia de SisRAISG',
                html: '<img src="https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-red.png">',
              }
            ],
          },
          //sisarp: {
          //  description: 'Noticias - SisArp',
          //  layer: L.geoJSON(news.sisarp, {
          //    pointToLayer: function (feature, latlng) {
          //      // See https://github.com/pointhi/leaflet-color-markers
          //      var icon = new L.Icon({
          //        iconUrl     : 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-red.png',
          //        shadowUrl   : 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
          //        iconSize    : [25, 41],
          //        iconAnchor  : [12, 41],
          //        popupAnchor : [1, -34],
          //        shadowSize  : [41, 41]
          //      });

          //      return L.marker(latlng, {
          //        icon: icon,
          //      });
          //    },
          //    onEachFeature: function(feature, layer) {
          //      if (feature.properties) {
          //        var content  = '<table><tr><th>Noticia</th></tr>';
          //            content += '<tr><td>Título: </td><td>'      + feature.properties.titulo          + '</td></tr>';
          //            content += '<tr><td>Fecha: </td><td>'       + feature.properties.data_publicacao + '</td></tr>';
          //            content += '<tr><td>Fuente: </td><td>'      + feature.properties.fonte_noticia   + '</td></tr>';
          //            content += '</table>';

          //        layer.bindPopup(content);
          //      }
          //    },
          //  }),
          //  legend: [
          //    {
          //      label: 'Noticia de SisArp',
          //      html: '<img src="https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-red.png">',
          //    }
          //  ],
          //},
        },
      },
      mineriaIlegal: {
        groupName: 'Minería Ilegal',
        expanded: true,
        layers: {
          pontosMineriaIlegal: {
            description: 'Puntos de Minería Ilegal',
            layer: L.esri.Cluster.featureLayer({
            //layer: L.esri.featureLayer({
              url:          gisServer + '/11',
              opacity:      0.8,
              polygonOptions: {
                color       : '#e57b08',
                weight      : 4,
                opacity     : 1,
                fillOpacity : 0.5
              },
              spiderfyOnMaxZoom: false,
              disableClusteringAtZoom: 8,
              iconCreateFunction: function (cluster) {
                var childCount = cluster.getChildCount();
                var c          = ' marker-cluster-';

                if (childCount < 10) {
                  c += 'small';
                } else if (childCount < 100) {
                  c += 'medium';
                } else {
                  c += 'large';
                }

                return new L.DivIcon({ html: '<div><img src="' + customImages + 'pontos/garimpo.png"> <span><strong>' + childCount + '</strong></span></div>', className: 'energy-cluster marker-cluster' + c, iconSize: new L.Point(40, 40) });
              },
              pointToLayer: function (geojson, latlng) {
                if (geojson.properties['leyendagarimpo'] == 'activo') {
                  var icon = 'ativo.png';
                }
                else if (geojson.properties['leyendagarimpo'] == 'inactivo') {
                  var icon = 'inativo.png';
                }
                else if (geojson.properties['leyendagarimpo'] == 'activo (local. aproximada)') {
                  var icon = 'aproximado-inativo.png';
                }
                else if (geojson.properties['leyendagarimpo'] == 'inactivo (local. aproximada)') {
                  var icon = 'aproximado-inativo.png';
                }
                else if (geojson.properties['leyendagarimpo'] == 's.i') {
                  var icon = 'na.png';
                }
                else if (geojson.properties['leyendagarimpo'] == 's.i (local. aproximada)') {
                  var icon = 'aproximado-na.png';
                }

                return L.marker(latlng, {
                  icon: L.icon({
                    iconUrl: customImages + 'pontos/' + icon,
                    iconSize: [20, 20],
                  }),
                });
              },
            }),
            click: function(e) {
              var attributes = e.layer.feature.properties;
              var content    = jQuery('#map').isaMap('htmlTable', 'Punto de Minería Ilegal', {
                'País'                  : attributes.país,
                'Nombre'                : attributes.nombre,
                'Descripción'           : attributes.descripción,
                'Metodo de explotación' : attributes.metodoexplotacion,
                'Substancia minerio'    : attributes.substanciaminerio,
                'Contaminantes'         : attributes.contaminantes,
                'Ator'                  : attributes.ator,
                'Situación'             : attributes.situación,
                'Fecha situación'       : attributes.fecha_situación,
                'Fuente información'    : attributes.fuenteinformación,
                'Observacion'           : attributes.observacion,
                'Instituición RAISG'    : attributes.instituicionraisg,
                'Link'                  : attributes.link,
                'Leyenda'               : attributes.leyendagarimpo,
              });

              e.layer.bindPopup(content);
              e.layer.openPopup();
            },
            legend: [
              {
                label: 'Agrupamiento',
                html: '<img src="' + customImages + 'pontos/garimpo.png">',
              },
              {
                label: 'Activo',
                html: '<img src="' + customImages + 'pontos/ativo.png">',
              },
              {
                label: 'Inactivo',
                html: '<img src="' + customImages + 'pontos/inativo.png">',
              },
              {
                label: 'Sin información',
                html: '<img src="' + customImages + 'pontos/na.png">',
              },
              {
                label: 'Activo - localización aproximada',
                html: '<img src="' + customImages + 'pontos/aproximado-ativo.png">',
              },
              {
                label: 'Inactivo - localización aproximada',
                html: '<img src="' + customImages + 'pontos/aproximado-inativo.png">',
              },
              {
                label: 'Sin información - localización aproximada',
                html: '<img src="' + customImages + 'pontos/aproximado-na.png">',
              },
            ],
          },
          linhasMineriaIlegal: {
            description: 'Ríos con minería ilegal',
            layer: L.esri.dynamicMapLayer({
              url:     gisServer,
              layers:  [ 12 ],
              opacity: 0.50,
            }),
            identify: function (error, featureCollection) {
              if (error || featureCollection.features.length === 0) {
                return false;
              } else {
                var attributes = featureCollection.features[0].properties;
                /**
                 * Handy way to build the attribute array
                 */
                /*
                for (var attr in attributes) {
                  console.debug("'" + attr + "' : attributes['" + attr + "'],");
                }
                */

                // Avoid results without basic info
                //if (attributes['Status'] == null) {
                //  return;
                //}

                var content = jQuery('#map').isaMap('htmlTable', 'Linha de Minería Ilegal', {
                  'País'                  : attributes['País'],
                  'Nombre'                : attributes['Nombre'],
                  'Descripción'           : attributes['Descripción'],
                  'Metodo de Explotación' : attributes['MetodoExplotacion'],
                  'Substancia'            : attributes['SubstanciaMinerio'],
                  'Contaminantes'         : attributes['Contaminantes'],
                  'Ator'                  : attributes['Ator'],
                  'Situación'             : attributes['Situación'],
                  'Fecha_Situación'       : attributes['Fecha_Situación'],
                  'Fuente de Información' : attributes['FuenteInformación'],
                  'Observacion'           : attributes['Observacion'],
                  'instituicion Raisg'    : attributes['Instituicion Raisg'],
                  'Link'                  : attributes['Link'],
                  'Leyenda'               : attributes['Leyenda'],
                });

                return content;
              }
            },
            legend: [
              {
                html: '',
                style: {
                  "opacity"          : "0.5",
                  "background-color" : "#ff00ff",
                  "width"            : "15px",
                  "height"           : "5px"
                },
              }
            ],
          },
          areasMineriaIlegal: {
            description: 'Áreas de Minería Ilegal',
            layer: L.esri.dynamicMapLayer({
              url:     gisServer,
              layers:  [ 13 ],
              opacity: 0.50,
            }),
            identify: function (error, featureCollection) {
              if (error || featureCollection.features.length === 0) {
                return false;
              } else {
                var attributes = featureCollection.features[0].properties;

                // Avoid results without basic info
                //if (attributes['Situación'] == null) {
                //  return;
                //}

                var content = jQuery('#map').isaMap('htmlTable', 'Area de Minería Ilegal', {
                  'País'                  : attributes['País'],
                  'Nombre'                : attributes['Nombre'],
                  'Descripción'           : attributes['Descripción'],
                  'ÁreaAfectada'          : attributes['ÁreaAfectada'],
                  'Metodo de Explotación' : attributes['MetodoExplotacion'],
                  'Substancia'            : attributes['SubstanciaMinerio'],
                  'Contaminantes'         : attributes['Contaminantes'],
                  'Ator'                  : attributes['Ator'],
                  'Situación'             : attributes['Situación'],
                  'Fecha Situación'       : attributes['Fecha Situación'],
                  'Fuente de Información' : attributes['FuenteInformación'],
                  'Observacion'           : attributes['Observacion'],
                  'Instituición RAISG'    : attributes['Instituicion Raisg'],
                  'Link'                  : attributes['Link'],
                  'Leyenda'               : attributes['Leyenda'],
                });

                return content;
              }
            },
            legend: [
              {
                label: 'Activo',
                html: '',
                style: {
                  "opacity"          : "0.5",
                  "background-color" : "#b9309b",
                  "border"           : "1px solid #8e306e",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: 'Inactivo',
                html: '',
                style: {
                  "opacity"          : "0.5",
                  "background-color" : "#eb30b9",
                  "border"           : "1px solid #b9309b",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: 'Sin información',
                html: '',
                style: {
                  "opacity"          : "0.5",
                  "background-color" : "#eccaff",
                  "border"           : "1px solid #e58eff",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
            ],
          },
        },
      },
      tisGarimpo: {
        groupName: 'Territorios Indigenas',
        expanded: true,
        layers: {
          tisGarimpo: {
            description: 'TIs afectados por minería ilegal',
            layer: L.esri.dynamicMapLayer({
              url:     gisServer,
              layers:  [ 16 ],
              opacity: 0.60,
              position: 'back',
            }),
            identify: function (error, featureCollection) {
              if (error || featureCollection.features.length === 0) {
                return false;
              } else {
                var attributes = featureCollection.features[0].properties;
                /**
                 * Handy way to build the attribute array
                 */
                /*
                for (var attr in attributes) {
                  console.debug("'" + attr + "' : attributes['" + attr + "'],");
                }
                */

                // Avoid results without basic info
                //if (attributes['Nombre'] == null) {
                //  return;
                //}

                var content = jQuery('#map').isaMap('htmlTable', 'Territorio Indigena afectado por minería ilegal', {
                  'País'                    : attributes['País'],
                  'Categoría'               : attributes['Categoría'],
                  'Nombre'                  : attributes['Nombre'],
                  'Institución RAISG'       : attributes['Institución RAISG'],
                  'Fecha Atualización Dato' : attributes['Fecha Atualización Dato'],
                  'Status'                  : attributes['Status'],
                  'Afectación'              : attributes['Afectación'],
                });

                return content;
              }
            },
            legend: [
              {
                label: 'TI sin afectación directa conocida',
                //html: '<img src="' + customImages + 'tis/sin-garimpo.png">',
                html: '',
                style: {
                  "opacity"          : "0.6",
                  "background-color" : "#dbd6c5",
                  "border"           : "1px solid #f6f2c1",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: 'Amenaza: garimpo en el límite/entorno',
                //html: '<img src="' + customImages + 'tis/garimpo-limite.png">',
                html: '',
                style: {
                  "opacity"          : "0.6",
                  "background-color" : "#ffcf7a",
                  "border"           : "1px solid #ffd78c",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: 'Amenaza: garimpo inactivo dentro',
                //html: '<img src="' + customImages + 'tis/garimpo-inactivo-dentro.png">',
                html: '',
                style: {
                  "opacity"          : "0.6",
                  "background-color" : "#ffbf00",
                  "border"           : "1px solid #ffb31a",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: 'Presión: balsas dentro/en el límite',
                //html: '<img src="' + customImages + 'tis/garimpo-balsas-dentro.png">',
                html: '',
                style: {
                  "opacity"          : "0.6",
                  "background-color" : "#ff8500",
                  "border"           : "1px solid #e95e1a",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: 'Presión: garimpo activo dentro',
                //html: '<img src="' + customImages + 'tis/garimpo-activo-dentro.png">',
                html: '',
                style: {
                  "opacity"          : "0.6",
                  "background-color" : "#ff5500",
                  "border"           : "1px solid #e91a1a",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              }
            ],
          },
        },
      },
      ucsGarimpo: {
        groupName: 'Áreas Naturales Protegidas',
        expanded: true,
        layers: {
          nacional: {
            description: 'ANP Nacional',
            layer: L.esri.dynamicMapLayer({
              url:     gisServer,
              layers:  [ 18 ],
              opacity: 0.60,
              position: 'back',
            }),
            identify: function (error, featureCollection) {
              if (error || featureCollection.features.length === 0) {
                return false;
              } else {
                var attributes = featureCollection.features[0].properties;
                /**
                 * Handy way to build the attribute array
                 */
                /*
                for (var attr in attributes) {
                  console.debug("'" + attr + "' : attributes['" + attr + "'],");
                }
                */

                // Avoid results without basic info
                //if (attributes['Nombre'] == null) {
                //  return;
                //}

                var content = jQuery('#map').isaMap('htmlTable', 'Áreas Naturales Protegidas', {
                  'País'                            : attributes['País'],
                  'Categoría'                       : attributes['Categoría'],
                  'Nombre'                          : attributes['Nombre'],
                  'Institución RAISG'               : attributes['Institución RAISG'],
                  'Fuente'                          : attributes['Fuente'],
                  'Status'                          : attributes['Status'],
                  'Afectación'                      : attributes['Afectación'],
                });

                return content;
              }
            },
            legend: [
              {
                label: 'ANP sin afectación directa conocida',
                //html: '<img src="' + customImages + 'tis/sin-garimpo.png">',
                html: '',
                style: {
                  "opacity"          : "0.6",
                  "background-color" : "#dbdbd2",
                  "border"           : "1px solid #d2dfb0",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: 'Amenaza: garimpo en el límite/entorno',
                //html: '<img src="' + customImages + 'tis/garimpo-limite.png">',
                html: '',
                style: {
                  "opacity"          : "0.6",
                  "background-color" : "#e2f78f",
                  "border"           : "1px solid #aceb30",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: 'Amenaza: garimpo inactivo dentro',
                //html: '<img src="' + customImages + 'tis/garimpo-inactivo-dentro.png">',
                html: '',
                style: {
                  "opacity"          : "0.6",
                  "background-color" : "#aae657",
                  "border"           : "1px solid #aceb30",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: 'Presión: balsas dentro/en el límite',
                //html: '<img src="' + customImages + 'tis/garimpo-balsas-dentro.png">',
                html: '',
                style: {
                  "opacity"          : "0.6",
                  "background-color" : "#a5d559",
                  "border"           : "1px solid #5eb930",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: 'Presión: garimpo activo dentro',
                //html: '<img src="' + customImages + 'tis/garimpo-activo-dentro.png">',
                html: '',
                style: {
                  "opacity"          : "0.6",
                  "background-color" : "#5eb930",
                  "border"           : "1px solid #4f8e30",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              }
            ],
          },
          departamental: {
            description: 'ANP Departamental',
            layer: L.esri.dynamicMapLayer({
              url:     gisServer,
              layers:  [ 19 ],
              opacity: 0.60,
              position: 'back',
            }),
            identify: function (error, featureCollection) {
              if (error || featureCollection.features.length === 0) {
                return false;
              } else {
                var attributes = featureCollection.features[0].properties;
                /**
                 * Handy way to build the attribute array
                 */
                /*
                for (var attr in attributes) {
                  console.debug("'" + attr + "' : attributes['" + attr + "'],");
                }
                */

                // Avoid results without basic info
                //if (attributes['Nombre'] == null) {
                //  return;
                //}

                var content = jQuery('#map').isaMap('htmlTable', 'ANP Departamental', {
                  'País'                            : attributes['País'],
                  'Categoría'                       : attributes['Categoría'],
                  'Nombre'                          : attributes['Nombre'],
                  'Institución RAISG'               : attributes['Institución RAISG'],
                  'Fuente'                          : attributes['Fuente'],
                  'Status'                          : attributes['Status'],
                  'Afectación'                      : attributes['Afectación'],
                });

                return content;
              }
            },
            legend: [
              {
                label: 'ANP sin afectación directa conocida',
                //html: '<img src="' + customImages + 'tis/sin-garimpo.png">',
                html: '',
                style: {
                  "opacity"          : "0.6",
                  "background-color" : "#dbdbd2",
                  "border"           : "1px solid #d2dfb0",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: 'Amenaza: garimpo en el límite/entorno',
                //html: '<img src="' + customImages + 'tis/garimpo-limite.png">',
                html: '',
                style: {
                  "opacity"          : "0.6",
                  "background-color" : "#e2f78f",
                  "border"           : "1px solid #aceb30",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: 'Amenaza: garimpo inactivo dentro',
                //html: '<img src="' + customImages + 'tis/garimpo-inactivo-dentro.png">',
                html: '',
                style: {
                  "opacity"          : "0.6",
                  "background-color" : "#aae657",
                  "border"           : "1px solid #aceb30",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: 'Presión: balsas dentro/en el límite',
                //html: '<img src="' + customImages + 'tis/garimpo-balsas-dentro.png">',
                html: '',
                style: {
                  "opacity"          : "0.6",
                  "background-color" : "#a5d559",
                  "border"           : "1px solid #5eb930",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: 'Presión: garimpo activo dentro',
                //html: '<img src="' + customImages + 'tis/garimpo-activo-dentro.png">',
                html: '',
                style: {
                  "opacity"          : "0.6",
                  "background-color" : "#5eb930",
                  "border"           : "1px solid #4f8e30",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
            ],
          },
          bosques: {
            description: 'Bosques Protectores (Ecuador)',
            layer: L.esri.dynamicMapLayer({
              url:     gisServer,
              layers:  [ 20 ],
              opacity: 0.60,
              position: 'back',
            }),
            identify: function (error, featureCollection) {
              if (error || featureCollection.features.length === 0) {
                return false;
              } else {
                var attributes = featureCollection.features[0].properties;
                /**
                 * Handy way to build the attribute array
                 */
                /*
                for (var attr in attributes) {
                  console.debug("'" + attr + "' : attributes['" + attr + "'],");
                }
                */

                // Avoid results without basic info
                //if (attributes['Nombre'] == null) {
                //  return;
                //}

                var content = jQuery('#map').isaMap('htmlTable', 'Bosques Protectores (Ecuador)', {
                  'País'                            : attributes['País'],
                  'Categoría'                       : attributes['Categoría'],
                  'Nombre'                          : attributes['Nombre'],
                  'Institución RAISG'               : attributes['Institución RAISG'],
                  'Fuente'                          : attributes['Fuente'],
                  'Status'                          : attributes['Status'],
                  'Afectación'                      : attributes['Afectación'],
                });

                return content;
              }
            },
            legend: [
              {
                label: 'ANP sin afectación directa conocida',
                //html: '<img src="' + customImages + 'tis/sin-garimpo.png">',
                html: '',
                style: {
                  "opacity"          : "0.6",
                  "background-color" : "#dbdbd2",
                  "border"           : "1px solid #d2dfb0",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: 'Amenaza: garimpo en el límite/entorno',
                //html: '<img src="' + customImages + 'tis/garimpo-limite.png">',
                html: '',
                style: {
                  "opacity"          : "0.6",
                  "background-color" : "#e2f78f",
                  "border"           : "1px solid #aceb30",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: 'Amenaza: garimpo inactivo dentro',
                //html: '<img src="' + customImages + 'tis/garimpo-inactivo-dentro.png">',
                html: '',
                style: {
                  "opacity"          : "0.6",
                  "background-color" : "#aae657",
                  "border"           : "1px solid #aceb30",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: 'Presión: balsas dentro/en el límite',
                //html: '<img src="' + customImages + 'tis/garimpo-balsas-dentro.png">',
                html: '',
                style: {
                  "opacity"          : "0.6",
                  "background-color" : "#a5d559",
                  "border"           : "1px solid #5eb930",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: 'Presión: garimpo activo dentro',
                //html: '<img src="' + customImages + 'tis/garimpo-activo-dentro.png">',
                html: '',
                style: {
                  "opacity"          : "0.6",
                  "background-color" : "#5eb930",
                  "border"           : "1px solid #4f8e30",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
            ],
          },
        },
      },
      mineria: {
        groupName: 'Minería: concesiones legales',
        expanded: false,
        layers: {
          mineria: {
            description: 'Minería',
            layer: L.esri.dynamicMapLayer({
              url:     gisServer,
              layers:  [ 14 ],
              opacity: 0.90,
            }),
            identify: function (error, featureCollection) {
              if (error || featureCollection.features.length === 0) {
                return false;
              } else {
                var attributes = featureCollection.features[0].properties;

                // Avoid results without basic info
                //if (attributes['Situación'] == null) {
                //  return;
                //}

                var content = jQuery('#map').isaMap('htmlTable', 'Minería: concesiones legales', {
                  'País'                    : attributes.país,
                  'Nombre'                  : attributes.nombre,
                  'Descripción'             : attributes.descripción,
                  'Cia'                     : attributes.cia,
                  'Substancia minerio'      : attributes.substanciaminerio,
                  'Contaminantes'           : attributes.contaminantes,
                  'Ator'                    : attributes.ator,
                  'Situación'               : attributes.situación,
                  'Fecha situación'         : attributes.fecha_situación,
                  'Fuente información'      : attributes.fuenteinformación,
                  'Observacion'             : attributes.observacion,
                  'Tipo minerio'            : attributes.tipo_minerio,
                  'Fuente fecha'            : attributes.fuente_fecha,
                  'Instituicion RAISG'      : attributes.institucionraisg,
                  'Fecha atualización dato' : attributes.fecha_atualizacion_dato,
                  'Area SIG ha'             : attributes.area_sig_ha,
                  'Leyenda'                 : attributes.leyenda,
                  'Codigo'                  : attributes.codigo,
                  'Amaz'                    : attributes.amaz,
                });

                return content;
              }
            },
            legend: [
              {
                label: 'En exploración',
                html: '',
                style: {
                  "opacity"          : "0.9",
                  "background-color" : "#9dffe9",
                  "border"           : "1px solid #9de9ff",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: 'En exploración/en explotación',
                html: '',
                style: {
                  "opacity"          : "0.9",
                  "background-color" : "#4dd6ff",
                  "border"           : "1px solid #4dc3ee",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
              {
                label: 'En explotación',
                html: '',
                style: {
                  "opacity"          : "0.9",
                  "background-color" : "#4d8dee",
                  "border"           : "1px solid #4d82c2",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              },
            ],
          },
        },
      },
      ciudades: {
        groupName: 'Ciudades',
        expanded: false,
        layers: {
          capitalPais: {
            description: 'Capital de País',
            layer: L.esri.dynamicMapLayer({
              url:     gisServer,
              layers:  [ 1 ],
              opacity: 0.90,
            }),
            identify: function (error, featureCollection) {
              if (error || featureCollection.features.length === 0) {
                return false;
              } else {
                var attributes = featureCollection.features[0].properties;

                // Avoid results without basic info
                //if (attributes['Situación'] == null) {
                //  return;
                //}

                var content = jQuery('#map').isaMap('htmlTable', 'Capital de País', {
                  'País'         : attributes['país'],
                  'Departamento' : attributes['nomedep'],
                  'Municipio'    : attributes['nomemun'],
                  'Capital'      : attributes['nomecap'],
                });

                return content;
              }
            },
            legend: [
              {
                label: 'Capital de País',
                html: '<img src="' + defaultImages + 'cidade/capital.png">',
              }
            ],
          },
          /*
          capitalPais: {
            description: 'Capital de País',
            layer: L.esri.featureLayer({
              url:          gisServer + '/1',
              pointToLayer: function (feature, latlng) {
                // See https://github.com/pointhi/leaflet-color-markers
                var icon = new L.Icon({
                  iconUrl     : 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-violet.png',
                  shadowUrl   : 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
                  iconSize    : [25, 41],
                  iconAnchor  : [12, 41],
                  popupAnchor : [1, -34],
                  shadowSize  : [41, 41]
                });

                return L.marker(latlng, {
                  icon: icon,
                });
              },
            }),
            legend: [
              {
                label: 'Capital de País',
                html: '<img src="https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-violet.png">',
              }
            ],
            click: function(e) {
              var attributes = e.layer.feature.properties;
              var content    = jQuery('#map').isaMap('htmlTable', 'Capital de País', {
                'Nombre'      : attributes.nomcab,
                'País'        : attributes.pais_iso,
              });

              e.layer.bindPopup(content);
              e.layer.openPopup();
            },
          },
          */
          capitalDepartamento: {
            description: 'Capital de Departamento/Estado',
            layer: L.esri.dynamicMapLayer({
              url:     gisServer,
              layers:  [ 2 ],
              opacity: 0.90,
            }),
            identify: function (error, featureCollection) {
              if (error || featureCollection.features.length === 0) {
                return false;
              } else {
                var attributes = featureCollection.features[0].properties;

                // Avoid results without basic info
                //if (attributes['Situación'] == null) {
                //  return;
                //}

                var content = jQuery('#map').isaMap('htmlTable', 'Capital de Departamento/Estado', {
                  'País'         : attributes['país'],
                  'Departamento' : attributes['nomedep'],
                  'Municipio'    : attributes['nomemun'],
                  'Capital'      : attributes['nomecap'],
                });

                return content;
              }
            },
            legend: [
              {
                label: 'Capital de Departamento/Estado',
                html: '<img src="' + defaultImages + 'cidade/estado.png">',
              }
            ],
          },
          /*
          capitalDepartamento: {
            description: 'Capital de Departamento/Estado',
            layer: L.esri.featureLayer({
              url:          gisServer + '/2',
              pointToLayer: function (feature, latlng) {
                // See https://github.com/pointhi/leaflet-color-markers
                var icon = new L.Icon({
                  iconUrl     : 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-yellow.png',
                  shadowUrl   : 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
                  iconSize    : [25, 41],
                  iconAnchor  : [12, 41],
                  popupAnchor : [1, -34],
                  shadowSize  : [41, 41]
                });

                return L.marker(latlng, {
                  icon: icon,
                });
              },
            }),
            legend: [
              {
                label: 'Capital de Departamento/Estado',
                html: '<img src="https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-yellow.png">',
              }
            ],
            click: function(e) {
              var attributes = e.layer.feature.properties;
              var content    = jQuery('#map').isaMap('htmlTable', 'Capital de Departamento/Estado', {
                'Nombre'      : attributes.nomcab,
                'Departamento': attributes.nomdep,
                'País'        : attributes.pais_iso,
              });

              e.layer.bindPopup(content);
              e.layer.openPopup();
            },
          },
          */
          ciudadPrincipal: {
            description: 'Municipio',
            layer: L.esri.dynamicMapLayer({
              url:     gisServer,
              layers:  [ 3 ],
              opacity: 0.90,
            }),
            identify: function (error, featureCollection) {
              if (error || featureCollection.features.length === 0) {
                return false;
              } else {
                var attributes = featureCollection.features[0].properties;

                // Avoid results without basic info
                //if (attributes['Situación'] == null) {
                //  return;
                //}

                var content = jQuery('#map').isaMap('htmlTable', 'Municipio', {
                  'País'         : attributes['país'],
                  'Departamento' : attributes['nomedep'],
                  'Municipio'    : attributes['nomemun'],
                  'Capital'      : attributes['nomecap'],
                });

                return content;
              }
            },
            legend: [
              {
                label: 'Municipio',
                html: '<img src="' + defaultImages + 'cidade/municipio.png">',
              }
            ],
          },
          /*
          ciudadPrincipal: {
            description: 'Capital Municipal',
            layer: L.esri.featureLayer({
              url:          gisServer + '/3',
              pointToLayer: function (feature, latlng) {
                // See https://github.com/pointhi/leaflet-color-markers
                var icon = new L.Icon({
                  iconUrl     : 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-orange.png',
                  shadowUrl   : 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
                  iconSize    : [25, 41],
                  iconAnchor  : [12, 41],
                  popupAnchor : [1, -34],
                  shadowSize  : [41, 41]
                });

                return L.marker(latlng, {
                  icon: icon,
                });
              },
            }),
            legend: [
              {
                label: 'Capital Municipal',
                html: '<img src="https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-orange.png">',
              }
            ],
            click: function(e) {
              var attributes = e.layer.feature.properties;
              var content    = jQuery('#map').isaMap('htmlTable', 'Ciudad Principal', {
                'Nombre'      : attributes.nomcab,
                'Departamento': attributes.nomdep,
                'País'        : attributes.pais_iso,
              });

              e.layer.bindPopup(content);
              e.layer.openPopup();
            },
          },
          */
        },
      },
      vias: {
        groupName: 'Vías',
        expanded: false,
        layers: {
          vias: {
            description: 'Vías',
            layer: L.esri.dynamicMapLayer({
              url:     gisServer,
              layers:  [ 4 ],
              opacity: 0.35,
            }),
            identify: function (error, featureCollection) {
              if (error || featureCollection.features.length === 0) {
                return false;
              } else {
                var attributes = featureCollection.features[0].properties;
                var content    = jQuery('#map').isaMap('htmlTable', 'Vias', {
                  'País'               : attributes.pais,
                  'NM Sigla'           : attributes.nm_sigla,
                  'NM Nome'            : attributes.nm_nome,
                  'Situacion'          : attributes.situacion,
                  'Fuente'             : attributes.fuente,
                  'Instituicion RAISG' : attributes.institucionraisg,
                  'Fecha atual'        : attributes.fecha_atual,
                  'Tipo'               : attributes.tipo,
                  'Leyenda'            : attributes.leyenda,
                  'Amaz'               : attributes.amaz,
                  'Lenght SIG MT'      : attributes.length_sig_mt,
                });

                return content;
              }
            },
            legend: [
              {
                html: '',
                style: {
                  "opacity"          : "0.35",
                  "background-color" : "#A51F7B",
                  "width"            : "15px",
                  "height"           : "2px"
                },
              }
            ],
          }
        },
      },
      limites: {
        groupName: 'Límites referenciales',
        expanded: false,
        layers: {
          internacional: {
            description: 'Límite Internacional',
            layer: L.esri.dynamicMapLayer({
              url:     gisServer,
              layers:  [ 6 ],
              opacity: 0.35,
            }),
            legend: [
              {
                html: '',
                style: {
                  "background-color" : "#ffffff",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              }
            ],
          },
          departamental: {
            description: 'Límite Departamental',
            layer: L.esri.dynamicMapLayer({
              url:     gisServer,
              layers:  [ 7 ],
              opacity: 0.35,
            }),
            legend: [
              {
                html: '',
                style: {
                  "opacity"          : "0.35",
                  "background-color" : "#ffffff",
                  "border"           : "1px dashed #828282",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              }
            ],
          },
          municipal: {
            description: 'Límite Municipal',
            layer: L.esri.dynamicMapLayer({
              url:     gisServer,
              layers:  [ 8 ],
              opacity: 0.35,
            }),
            legend: [
              {
                html: '',
                style: {
                  "opacity"          : "0.35",
                  "background-color" : "#ffffff",
                  "border"           : "1px solid #6e6e6e",
                  "width"            : "10px",
                  "height"           : "10px"
                },
              }
            ],
          },
        },
      },
      amazonia: {
        groupName: 'Amazonía',
        expanded: false,
        layers: {
          limite: {
            description: 'Amazonía',
            layer: L.esri.dynamicMapLayer({
              url:     gisServer,
              layers:  [ 9 ],
              opacity: 0.35,
            }),
            legend: [
              {
                html: '',
                style: {
                  "opacity"          : "0.35",
                  "background-color" : "#000000",
                  "width"            : "15px",
                  "height"           : "2px"
                },
              }
            ],
          },
          /*
          cuenca: {
            description: 'Cuenca Amazónica',
            layer: L.esri.dynamicMapLayer({
              url:     gisServer,
              layers:  [ 11 ],
              opacity: 0.35,
            }),
          },
          subcuencas: {
            description: 'Subcuencas',
            layer: L.esri.dynamicMapLayer({
              url:     gisServer,
              layers:  [ 12 ],
              opacity: 0.35,
            }),
            identify: function (error, featureCollection) {
              if (error || featureCollection.features.length === 0) {
                return false;
              } else {
                var attributes = featureCollection.features[0].properties;
                var content    = jQuery('#map').isaMap('htmlTable', 'Subcuencas', {
                  'Cuenca nivel 6' : attributes.c6nom,
                  'Cuenca nivel 3' : attributes.c3nom,
                });
  
                return content;
              }
            },
          },
          biogeo: {
            description: 'Amazonía Biogeográfica',
            layer: L.esri.dynamicMapLayer({
              url:     gisServer,
              layers:  [ 13 ],
              opacity: 0.35,
            }),
          },
          */
        },
      },
    };

    // Default parameters for custom map
    var params                    = jQuery.deparam.fragment();
    params.about                  = false;
    params.layerDefinitionsExtend = true;
    params.layerDefinitions       = layers;
    params.layerExcluded          = [ 'jurisdiction.amlegal' ];
    params.groupExcluded          = [ 'tis', 'ucs', 'infra', 'ambiente', 'desmatamento', 'jurisdicao', 'queimadas' ];
    params.debug                  = false;
    params.hashChange             = false;
    params.searchControl          = false;
    params.embedControl           = false;
    params.techNoteControl        = false;
    params.baseLayer              = params.baseLayer != undefined ? params.baseLayer : 'base.topographic';
    params.zoom                   = params.zoom      != undefined ? params.zoom      : 5;
    params.minZoom                = params.minZoom   != undefined ? params.minZoom   : 5;
    params.center                 = params.center    != undefined ? params.center    : [ -5, -62 ];
    params.maxBounds              = [ [-32, -98], [30, -20] ];
    params.lang                   = lang;
    params.logo                   = 'https://mapa.eco.br/images/logos/microraisg.jpg';
    params.logo_width             = '60px';
    params.logo_href              = 'https://amazoniasocioambiental.org';
    params.layers                 = params.layers != undefined ? params.layers : [
                                      'mineriaIlegal.pontosMineriaIlegal', 'mineriaIlegal.linhasMineriaIlegal',
                                      'mineriaIlegal.areasMineriaIlegal',
                                      //'tisGarimpo.tisGarimpo',           'ucsGarimpo.nacional',
                                      //'ucsGarimpo.departamental',        'ucsGarimpo.bosques',
                                      //'noticias.sisraisg',
                                      'amazonia.limite', 'vias.vias',
                                      'ciudades.capitalPais', 'ciudades.capitalDepartamento', 'ciudades.ciudadPrincipal',
                                      'limites.internacional',
                                    ];

    // Dispatch
    jQuery('#map').isaMap(params);
  });

  // URL params
  var params   = jQuery.deparam.querystring();
  var fragment = jQuery.deparam.fragment();

  // Convert to new format
  if (jQuery.isEmptyObject(params) && !jQuery.isEmptyObject(fragment)) {
    params               = fragment;
    window.location.hash = '';
  }

  // Language is currently hardcoded
  var lang        = 'es';
  var disclaimer  = '<span>' + messages[lang].disclaimer;
      disclaimer += '</span><p /><button style="color: #000000;" id="disclaimer">' + messages[lang].agree + '</button>';

  if (params.disclaimer == undefined || params.disclaimer == true) {
    // Block UI
    jQuery('.isamap').block({
      message : disclaimer,
      css     : {
        'width'            : '60%',
        'font-size'        : '22px',
        'color'            : '#ffffff',
        'border'           : 'none',
        'backgroundColor'  : 'rgb(255, 255, 255) transparent',
        'text-shadow'      : '1px 1px 1px #000000',
      },
    });

    jQuery('#disclaimer').click(function() {
      jQuery('.isamap').unblock();
    });
  }

  // Load custom metadata
  loadData();
});
