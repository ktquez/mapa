import Vue                  from 'vue';
import * as mapHandler      from '../../../map/instance.js';
import { layerDefinitions } from '../layers.js';
import { getQueryString }   from '../../../common/url_query.js';

export default {
  data   : function() {
    return {
      title  : 'RAISG',
      isamap : null,
    }
  },
  mounted: function() {
    var self = this;

    // Layers object
    var layers = {};

    // URL params
    var params = getQueryString();

    // URL layers
    if (params.layers == undefined) {
      params.layers = [];
    }

    // Custom map parameters
    params.debug                  = true;
    params.about                  = false;
    params.searchControl          = false;
    params.embedControl           = false;
    params.techNoteControl        = false;
    params.localeControl          = true;
    params.legendControl          = true;
    params.printControl           = false;
    params.localizeContent        = false;
    params.layerDefinitionsExtend = false;
    params.layerDefinitions       = layers;
    params.logo                   = 'https://mapa.eco.br/images/logos/microraisg.jpg';
    params.logo_width             = '60px';
    params.logo_href              = 'https://www.amazoniasocioambiental.org';
    params.maxBounds              = [ [-32, -98], [30, -20] ];
    params.layerControlState      = document.documentElement.clientWidth < 992 ? 'closed' : 'opened';

    // Layer definitions
    params.layerDefinitionsExtend = false;
    params.layerDefinitions       = layerDefinitions(this);

    // Config depending on URL params
    params.zoom                   = params.zoom      != undefined ? params.zoom    : 5;
    params.minZoom                = params.minZoom   != undefined ? params.minZoom : 5;
    params.center                 = params.center    != undefined ? params.center  : [ -5, -62 ];

    // Load map metadata
    mapHandler.load();

    jQuery(document).on('isaMapLoaded', function() {
      // Initialize map
      self.isamap = mapHandler.add(self.$el, params);
    });
  },
}
