import EventBus from '../../../../utils/EventBus'

export default {
  name: 'mobile-menu',
  data: function() {
    return {
      controls: {
        'box-layers': false,
        'box-complaint': false,
        'box-alert': false,
        'box-tools': false
      }
    }
  },
  mounted: function() {
    const self = this;
    EventBus.$on('close-controls', function () {
      self.closeControls();
    });
  },
  computed: {},
  methods: {
    toggleControls: function (control) {
      if (this.controls[control]) return this.closeControls()
      this.closeControls()
      this.openControl(control)
    },    
    openControl (control) {
      this.controls[control] = true
      EventBus.$emit('state-' + control, true)
    },
    closeControls () {
      Object.keys(this.controls).forEach(control => {
        this.controls[control] = false
        EventBus.$emit('state-' + control, false)
      })
    }
  }
}
 