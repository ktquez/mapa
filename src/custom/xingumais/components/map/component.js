import Vue                   from 'vue';
import ApolloClient          from 'apollo-boost';
import gql                   from 'graphql-tag';
import VueApollo             from 'vue-apollo';
import tools                 from '../tools/component.vue';
import { providers         } from '../../geocoder.js';
import { drawLocal         } from '../../draw.js';
import * as mapHandler       from '../../../../map/instance.js';
//import L                   from '../../../map/leaflet.js';
import { getQueryString }    from '../../../../common/url_query.js';
import EventBus from '../../../../utils/EventBus'

// Inject VueApollo into Vue
Vue.use(VueApollo)

// Apollo Client config
const apolloClient = new ApolloClient({
  uri: 'https://ox.socioambiental.org/v2/api',
})

// Apollo Provider config
const apolloProvider = new VueApollo({
  defaultClient: apolloClient,
})

export default {
  props: {
    'map': {
      type    : String,
      default : '',
    },
  },
  data   : function() {
    return {
      title  : 'Xingu+',
      geoBase: 'https://geo.socioambiental.org/xingu',
      isamap : null,
    }
  },
  apollo : {
    config: {
      query: gql`query GetMap($map: String!) {
        config: maps(limit: 1, filters: [{key: "field_url_identifier", value: $map}]) {
          total
          items {
            title
            urlIdentifier
            groups: layers {
              id
              groupName: title
              layers: components {
                type
                attribution
                color
                id
                layersIdsService
                popup
                opacity
                overlayPosition
                title
                url
                from
                to
                reverse
                grid
                sliderId
                legendScale
                groupPrefix
                typeSlider
                iconClass
                backgroundIcon
                scale {
                  id
                  title
                  url
                  layersIdsService
                  opacity
                  attribution
                  type
                }
              }
              tilesComponents {
                id
                title
              }
            }
          }
        }
      }`,
      variables: {
        map: '',
      },
    }
  },
  methods: {
    hideControls: function() {
      EventBus.$emit('close-controls');
    },
    applyTemplate: function(template, data) {
      var content = template;

      for (var item in data) {
        //content = content.replace('{{' + item + '}}', data[item]);
        content = content.replace(new RegExp('{{ *' + item + ' *}}', 'i'), data[item]);
      }

      return content;
    },
    normalizeName: function(value) {
      //return String(value);
      return String(value).replace(/ /g, '_').replace(/:/g, '_');
    },
    drawControl: function() {
      // Draw control
      try {
        var drawnItems = L.featureGroup().addTo(this.isamap.map);
        L.drawLocal    = drawLocal;

        this.isamap.map.addControl(new L.Control.Draw({
          edit: {
            featureGroup: drawnItems,
            poly: {
              allowIntersection: false
            }
          },
          draw: {
            polygon: {
              allowIntersection: false,
              showArea: true
            }
          }
        }));

        this.isamap.map.on(L.Draw.Event.CREATED, function (event) {
          var layer = event.layer;

          drawnItems.addLayer(layer);
        });
      } catch (e) {
        console.log(e)
      }
    },
  },
  watch: {
    map: function() {
      this.$apollo.queries.config.refetch({
        map: this.map,
      });
    },
    config: function() {
      var self = this;

      if (this.config.items == undefined || this.config.items[0] == undefined) {
        return;
      }

      if (this.config.items[0].title != undefined && this.config.items[0].title != null) {
        document.title = this.config.items[0].title + ' - ' + this.title; 
      }


      // Layers object
      var layers = {};

      // URL params
      var params = getQueryString();

      // URL layers
      if (params.layers == undefined) {
        params.layers = [];
      }

      // Build layer definitions from config
      for (var item in this.config.items[0].groups) {
        if (this.config.items[0].groups[item].layers == null) {
          continue;
        }

        //var group = String(this.config.items[0].groups[item].id);
        var group     = this.normalizeName(this.config.items[0].groups[item].groupName);
        var groupName = this.config.items[0].groups[item].groupName;

        layers[group] = {
          layers    : {},
          groupName : groupName,
          expanded  : true,
        };

        for (var subitem in this.config.items[0].groups[item].layers) {
          if (this.config.items[0].groups[item].layers[subitem].type == 'toggleSwitchLayer') {
            var current = this.config.items[0].groups[item].layers[subitem];
            //var layer = String(this.config.items[0].groups[item].layers[subitem].id);
            var layer   = this.normalizeName(this.config.items[0].groups[item].layers[subitem].title);

            layers[group].layers[layer] = {
              description        : current.title,
              legendFromMapServer: true,
              //layer            : L.esri.featureLayer({
              layer              : L.esri.dynamicMapLayer({
                format           : 'png32',
                url              : current.url,
                layers           : current.layersIdsService,
                opacity          : current.opacity         != null ? current.opacity         : 1,
                attribution      : current.attribution     != null ? current.attribution     : null,
                position         : current.overlayPosition != null ? current.overlayPosition : 'front',
              }),
            };

            // A closure here preserves the current element for the popup function
            if (current.popup != null) {
              (function() {
                var currentSaved                     = current;
                layers[group].layers[layer].identify = function (error, featureCollection) {
                  if (error || featureCollection.features.length === 0) {
                    return false;
                  } else {
                    var content = self.applyTemplate(currentSaved.popup, featureCollection.features[0].properties);

                    return content;
                  }
                }
              })();
            }
          }
          else if (this.config.items[0].groups[item].layers[subitem].type == 'jsonLayer') {
            // A closure here preserves the current element for the popup function
            (function() {
              var current = self.config.items[0].groups[item].layers[subitem];
              //var layer = String(self.config.items[0].groups[item].layers[subitem].id);
              var layer   = self.normalizeName(self.config.items[0].groups[item].layers[subitem].title);

              var icon        = current.iconClass      != null ? current.iconClass      : 'fa-street-view';
              var markerColor = current.backgroundIcon != null ? current.backgroundIcon : 'red-dark';

              layers[group].layers[layer] = {
                description: current.title,
                layer          : L.layerJSON({
                  url          : current.url,
                  propertyLoc  : 'field_c_coor',
                  propertyTitle: 'title',
                  propertyId   : 'nid',
                  dataToMarker : function (data, latlng) {
                    return L.marker(latlng, {
                      icon: L.ExtraMarkers.icon({
                        icon       : icon, 
                        markerColor: markerColor,
                        shape      : 'square',
                        prefix     : 'fa'
                      }),
                      //}).bindTooltip(self.applyTemplate(current.popup, data));
                    }).bindPopup(self.applyTemplate(current.popup, data));
                  },
                  filterData   : function(json) {
                    for (var item in json) {
                      json[item].field_c_coor = json[item].field_c_coor.split(',');
                    }

                    return json;
                  },
                }),
                legend: [
                  {
                    html: '<div class="leaflet-marker-icon extra-marker extra-marker-square-' + markerColor +
                    ' leaflet-zoom-animated leaflet-interactive"><i style="color: #fff" class=" fa ' + icon + '"></i></div>',
                    label: groupName + ': ' + current.title,
                    style: {
                      "opacity" : "1",
                      "width"   : "40px",
                      "height"  : "40px",
                    },
                  },
                ],
              };
            })();
          }
          else if (this.config.items[0].groups[item].layers[subitem].type == 'sliderLayer') {
            var current          = this.config.items[0].groups[item].layers[subitem];
            layers[group].slider = {
              type             : current.typeSlider != null ? current.typeSlider : 'double',
              //min            : 0,
              //max            : 0,
              from             : current.from != null ? current.from : 0,
              to               : current.to   != null ? current.to   : 0,
              grid             : current.grid != null ? current.grid : true,
              keyboard         : true,
              prettify_enabled : true,
            };

            // Reverse sliders config
            if (current.reverse == true) {
              var regexp = new RegExp('^' + group + '\.[0-9]+$');

              // Only if they're not already present in the URL params
              // Thanks https://stackoverflow.com/questions/7038575/find-element-in-array-using-regex-match-without-iterating-over-array#43825436
              //if (params.layers.indexOf(group + '.' + scale) == -1) {
              if (params.layers.findIndex(value => regexp.test(value)) == -1) {
                if (current.typeSlider == 'single') {
                  var max = current.scale.length - 1;
                  params.layers.push(group + '.' + max);
                }
                else if (current.typeSlider == 'double') {
                  for (var scale in current.scale) {
                    params.layers.push(group + '.' + scale);
                  }
                }
              }
            }

            // Using incremental integers as layer IDs to ensure they're iterated in order
            // https://stackoverflow.com/questions/5525795/does-javascript-guarantee-object-property-order
            var id = 0;

            for (var scale in current.scale) {
              var item  = current.scale[scale];
              var title = null;

              if (item.url == null) {
                item.url = '';
              }

              //var id = item.id;
              //var id = String(item.id);
              //var id = 'id_' + String(item.id);
              //var id = item.title != null ? item.title : 'null';

              // Slider layer titles comes prepended with a string that should be removed
              if (current.groupPrefix != null) {
                //var title = item.title != null ? item.title.substring(1): null;
                title = item.title != null ? item.title.replace(new RegExp(current.groupPrefix, 'i'), ''): null;
              }
              else {
                title = item.title;
              }

              if (item.type == 'emptyLayer') {
                layers[group].layers[id] = {
                  description: title,
                  layer      : L.tileLayer(''),
                };
              }
              else if (item.type == 'tileLayer') {
                layers[group].layers[id] = {
                  description        : title,
                  legendFromMapServer: true,
                  layer              : L.tileLayer(item.url, {
                    opacity     : item.opacity     != null ? item.opacity     : 1,
                    attribution : item.attribution != null ? item.attribution : null,
                  }),
                };
              }
              else if (item.type == 'dynamicLayer') {
                layers[group].layers[id] = {
                  description        : title,
                  legendFromMapServer: true,
                  layer              : L.esri.dynamicMapLayer({
                    format      : 'png32',
                    url         : item.url,
                    layers      : item.layersIdsService,
                    opacity     : item.opacity            != null ? item.opacity            : 1,
                    attribution : item.attribution        != null ? item.attribution        : null,
                    position    : current.overlayPosition != null ? current.overlayPosition : 'front',
                  }),
                };

                if (current.popup != null) {
                  // A closure here preserves the current element for the popup function
                  (function() {
                    var currentSaved                  = current;
                    layers[group].layers[id].identify = function (error, featureCollection) {
                      if (error || featureCollection.features.length === 0) {
                        return false;
                      } else {
                        var content = self.applyTemplate(currentSaved.popup, featureCollection.features[0].properties);

                        return content;
                      }
                    }
                  })();
                };
              }
              else if (item.type == 'heatMap') {
                layers[group].layers[id] = {
                  description        : title,
                  legendFromMapServer: true,
                  layer              : L.esri.Heat.featureLayer({
                    //fields    : ['oid'],
                    url         : item.url,
                    minOpacity  : item.opacity != null ? item.opacity : 1,
                    //maxZoom   : 15,
                    max         : 0.6,
                    radius      : 10,
                    blur        : 8,
                    gradient    : {0.5: 'gold', 0.7: 'orange', 0.9: '#bd0026'},
                    attribution : item.attribution,
                  }),
                };
              }

              id++;
            }
          }
        }
      }

      // Custom map parameters
      params.debug                  = true;
      params.about                  = false;
      params.searchControl          = false;
      params.embedControl           = false;
      params.techNoteControl        = false;
      params.localeControl          = false;
      params.legendControl          = true;
      params.printControl           = false;
      params.localizeContent        = false;
      params.layerDefinitionsExtend = false;
      params.layerDefinitions       = layers;
      params.logo                   = 'https://mapa.eco.br/images/logos/xingumais.png';
      params.logo_width             = '60px';
      params.logo_href              = 'https://xingumais.org.br';
      params.layerControlState      = document.documentElement.clientWidth < 992 ? 'closed' : 'opened';

      // Additional map parameters that might be overrided by URL config
      params.maxBounds      = [ [-32, -98], [30, -20] ];
      params.baseLayer      = params.baseLayer     != undefined ? params.baseLayer : 'base.satellite';
      params.zoom           = params.zoom          != undefined ? params.zoom      : 6;
      params.minZoom        = params.minZoom       != undefined ? params.minZoom   : 5;
      params.center         = params.center        != undefined ? params.center    : [ -8, -53 ];
      params.layers         = params.layers.length  > 0         ? params.layers    : [ 'Limites.Bacia_do_Xingu', ];

      // Initialize map: jQuery approach
      // Dispatch to our map plugin
      //jQuery('#map').isaMap(params);
      //
      //// Get the map self object
      //self.isamap = jQuery('#map').isaMap('getSelf');

      // Load map metadata
      mapHandler.load('https://mapa.eco.br');

      jQuery(document).on('isaMapLoaded', function() {
        // Initialize map
        self.isamap = mapHandler.add(self.$el, params);

        // Geocoder control
        var searchControl = L.esri.Geocoding.geosearch({
          providers              : providers(self.geoBase),
          position               : 'topright',
          useMapBounds           : false,
          collapseAfterResult    : false,
          expanded               : true,
          maxResults             : 5,
          placeholder            : 'Pesquise UCs, TIs ou municípios...',
          title                  : 'Pesquisar unidades de conservação, terras indígenas ou municípios',
          mapAttribution         : null,
          useArcgisWorldGeocoder : false
        }).addTo(self.isamap.map);


        // Draw control
        self.drawControl();

        var options = {
          position: 'bottomright',
        }

        var componentOptions = {
          name         : self.map,
          searchControl: searchControl,
        }

        // Custom control
        L = self.isamap.methods.registerVueControl('xingumais_tools', 'leaflet-xingumais_tools-control', options, tools, componentOptions);
      });
    },
  },
  apolloProvider,
}
