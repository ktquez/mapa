#!/usr/bin/env node
/**
 * Mapa Socioambiental - Aggregate data into static files.
 * Only for API SisARP v2 ownwards.
 */

// This produces weird behaviors.
//'use strict';

// Requirements
const aggregateUfs  = require('../backend/aggregate/ufs.js');
const aggregateUcs  = require('../backend/aggregate/ucs.js');
const aggregateTis  = require('../backend/aggregate/tis.js');
const aggregateArps = require('../backend/aggregate/arps.js');

// Dispatch
aggregateUfs();
aggregateUcs();
aggregateTis();
//aggregateArps();
