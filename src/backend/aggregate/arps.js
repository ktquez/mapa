const output            = __dirname + '/../../../data/';
const fs                = require('fs');
const async             = require('async');
const fetch             = require('node-fetch');
const config            = require('../../../config/api.js');
const processEntityItem = require('../api/entity.js');
const buildUrl          = require('../api/url.js');
const fsWriteError      = require('../fs/error.js');
const numTasks          = require('./tasks.js');
var data                = require('./data.js');

// Aggregator for ARPs
module.exports = async function () {
  var type     = 'arp';
  var tasks    = [];
  var url      = buildUrl('sisarp/v2/' + type);
  var response = await fetch(url).catch(err => console.error(err));;
  data[type]   = await response.json();

  // Ensure no API Key leaks
  delete data[type].meta.apikey;
  delete data[type].meta.endpoint;

  await async.map(Object.keys(data[type].data), function(item) {
    // Hint: Use reflect to continue the execution of other tasks when a task fails
    // See https://caolan.github.io/async/v3/docs.html#reflect
    //     https://caolan.github.io/async/v3/docs.html#parallel
    if (data[type].data[item].plotagem == 'Sim') {
      tasks.push(async.reflect(async function() {
        await processEntityItem(item, type, 'bacia',          'descricao', 'bacia',);
        await processEntityItem(item, type, 'bioma',          'descricao', 'bioma');
        await processEntityItem(item, type, 'fitofisionomia', 'descricao', 'fitofisionomia');
        await processEntityItem(item, type, 'pressao',        'tipo',      'pressao');
      }));
    }
  }, function(err, results) {});

  await async.parallelLimit(tasks, numTasks, function(err, results) {
    if (err) {
      console.log(err);
    }

    var result = {
      arps       : data[type],
      properties : {
        bacias          : data.bacia,
        biomas          : data.biomas,
        fitofisionomias : data.fitofisionomia,
        pressoes        : data.pressao,
      },
    };

    // Write output
    fs.writeFile(output   + "sisarp/v2/arps.json",            JSON.stringify(data[type]                  ), fsWriteError);
    //fs.writeFile(output + "sisarp/v2/arps.json",            JSON.stringify(data[type],          null, 2), fsWriteError);
    //fs.writeFile(output + "sisarp/v2/arps.json",            JSON.stringify(data[type],          null, 2), fsWriteError);
    //fs.writeFile(output + "sisarp/v2/bacias.json",          JSON.stringify(data.bacia,          null, 2), fsWriteError);
    //fs.writeFile(output + "sisarp/v2/biomas.json",          JSON.stringify(data.bioma,          null, 2), fsWriteError);
    //fs.writeFile(output + "sisarp/v2/fitofisionomias.json", JSON.stringify(data.fitofisionomia, null, 2), fsWriteError);
    //fs.writeFile(output + "sisarp/v2/pressoes.json",        JSON.stringify(data.pressao,        null, 2), fsWriteError);
  });
}
