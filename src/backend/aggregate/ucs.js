const output            = __dirname + '/../../../data/';
const fs                = require('fs');
const async             = require('async');
const fetch             = require('node-fetch');
const config            = require('../../../config/api.js');
const processEntityItem = require('../api/entity.js');
const buildUrl          = require('../api/url.js');
const fsWriteError      = require('../fs/error.js');
const numTasks          = require('./tasks.js');
var data                = require('./data.js');
var globals             = require('../../map/globals.js');

// Aggregator for UCs
module.exports = async function () {
  var type     = 'uc';
  var tasks    = [];
  var url      = buildUrl('sisarp/v2/' + type);
  var response = await fetch(url).catch(err => console.error(err));;
  data[type]   = await response.json();

  // Ensure no API Key leaks
  delete data[type].meta.apikey;
  delete data[type].meta.endpoint;

  await async.map(Object.keys(data[type].data), function(item) {
    // Hint: Use reflect to continue the execution of other tasks when a task fails
    // See https://caolan.github.io/async/v3/docs.html#reflect
    //     https://caolan.github.io/async/v3/docs.html#parallel
    if (data[type].data[item].plotagem == 'Sim') {
      tasks.push(async.reflect(async function() {
        await processEntityItem(item, type, 'bacia_'          + type, 'descricao', 'bacia');
        await processEntityItem(item, type, 'bioma_'          + type, 'descricao', 'bioma');
        await processEntityItem(item, type, 'fitofisionomia_' + type, 'descricao', 'fitofisionomia');
        await processEntityItem(item, type, 'pressao_'        + type, 'tipo',      'pressao');
      }));
    }
  }, function(err, results) {});

  await async.parallelLimit(tasks, numTasks, function(err, results) {
    if (err) {
      console.log(err);
    }

    var assembly = data[type].data;

    // Build an array with id_arp as index
    // This code used to live in the frontend, at loaders.js
    for (var i in assembly) {
      // Exclude unplotted UCs
      if (assembly[i].plotagem == 'Não') {
        continue;
      }

      var id               = assembly[i].id;
      globals.arps.ucs[id] = assembly[i];

      // Build properties
      for (var property in globals.arpsProperties.ucs) {
        var currentProperty = globals.arpsProperties.ucs[property];

        if (globals.arpsByProperty.ucs[currentProperty] == undefined) {
          globals.arpsByProperty.ucs[currentProperty] = {};
        }

        if (globals.arps.ucs[id][currentProperty] == undefined || globals.arps.ucs[id][currentProperty] == null) {
          continue;
        }

        // Exception handling
        if (currentProperty == 'categoria' && globals.arps.ucs[id].descricao_categoria != undefined) {
          var propertyName = globals.arps.ucs[id][currentProperty] + ' - ' + globals.arps.ucs[id].descricao_categoria;
        }
        else {
          var propertyName = globals.arps.ucs[id][currentProperty];
        }

        if (globals.arpsByProperty.ucs[currentProperty][propertyName] == undefined) {
          globals.arpsByProperty.ucs[currentProperty][propertyName] = [];
        }

        globals.arpsByProperty.ucs[currentProperty][propertyName].push(id);
      }

      // Remove some data to decrease JSON output
      delete globals.arps.ucs[id].bacia;
      delete globals.arps.ucs[id].bioma;
      delete globals.arps.ucs[id].fitofisionomia;
      delete globals.arps.ucs[id].pressao;
    }

    globals.arpsByProperty.ucs.bacia          = data['bacia_'          + type];
    globals.arpsByProperty.ucs.bioma          = data['bioma_'          + type];
    globals.arpsByProperty.ucs.fitofisionomia = data['fitofisionomia_' + type];
    globals.arpsByProperty.ucs.pressao        = data['pressao_'        + type];

    var result = {
      //ucs      : data.uc,
      ucs        : globals.arps.ucs,
      properties : globals.arpsByProperty.ucs,
    };

    // Write output
    fs.writeFile(output   + "sisarp/v2/ucs.json",                          JSON.stringify(result                                 ), fsWriteError);
    //fs.writeFile(output + "sisarp/v2/ucs.json",                          JSON.stringify(result,                         null, 2), fsWriteError);
    //fs.writeFile(output + "sisarp/v2/ucs.json",                          JSON.stringify(data[type],                     null, 2), fsWriteError);
    //fs.writeFile(output + "sisarp/v2/bacias_"          + type + '.json', JSON.stringify(data['bacia_'          + type], null, 2), fsWriteError);
    //fs.writeFile(output + "sisarp/v2/biomas_"          + type + '.json', JSON.stringify(data['bioma_'          + type], null, 2), fsWriteError);
    //fs.writeFile(output + "sisarp/v2/fitofisionomias_" + type + '.json', JSON.stringify(data['fitofisionomia_' + type], null, 2), fsWriteError);
    //fs.writeFile(output + "sisarp/v2/pressoes_"        + type + '.json', JSON.stringify(data['pressao_'        + type], null, 2), fsWriteError);
  });
}
