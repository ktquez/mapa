var globals = require('../globals');

export function utils(self) {
  return {
    // Debug facility
    debug: function(message) {
      if (self.settings.debug == true) {
        window.console && console.debug('[isamap] [#' + self.settings.mapId + ']', message);
      }
    },

    message: function(key) {
      if (self.messages[self.settings.lang][key] != undefined) {
        return self.messages[self.settings.lang][key];
      }
    },

    getSettings: function() {
      return self.settings;
    },

    dumpSettings: function() {
      self.methods.debug(self.settings);
    },

    getLayers: function() {
      return self.layers;
    },

    getSelf: function() {
      return self;
    },

    // Return locale coded used by Number.prototype.toLocaleString() depending on current language.
    //
    // See https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Number/toLocaleString
    //     https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Intl#Locale_identification_and_negotiation
    //     https://tools.ietf.org/html/rfc5646
    getLocale: function() {
      if (self.settings.lang == 'pt-br') {
        return 'pt-BR';
      }

      return self.settings.lang;
    },

    htmlTableRow: function(key, value) {
      //if (value == null || value == 'null' || value == 'Null') {
      //  return;
      //}

      // Format numbers
      if (!isNaN(value)) {
        value = Number(value);
        value = value.toLocaleString(self.methods.getLocale());
      }

      // Try to translate key and value
      key   = self.methods.message(key)   != null ? self.methods.message(key)   : key;
      value = self.methods.message(value) != null ? self.methods.message(value) : value;

      var content  = "<tr>";
      content     += "<td>" + key + ":</td><td> " + value + "  </td>";
      content     += "</tr>";

      return content;
    },

    htmlTable: function(title, items) {
      var empty   = true;
      var content = '<div>';
      title       = self.methods.message(title) != null ? self.methods.message(title) : title;

      content += '<table class="table">';
      content += "<tr><th colspan=\"2\">" + title + "</th></tr>";

      for (var key in items) {
        if (items[key] != null && items[key] != 'null' && items[key] != 'Null') {
          empty    = false;
          content += self.methods.htmlTableRow(key, items[key]);
        }
      }

      content += '</table>';
      content += '</div></div>';

      if (empty == true) {
        return '';
      }

      return content;
    },

    truncateString: function(string, maxLen) {
      if (string.length > maxLen) {
        return string.slice(0, maxLen) + '...';
      }
      else {
        return string;
      }
    },

    // See https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort?v=example
    sortNumber: function(a, b) {
      return a - b;
    },

    sortArpsByName: function(data) {
      var result = [];

      // Populate a sorted array needed by select boxes
      // See https://stackoverflow.com/questions/1129216/sort-array-of-objects-by-string-property-value-in-javascript#1129270
      //     https://stackoverflow.com/questions/9658690/is-there-a-way-to-sort-order-keys-in-javascript-objects
      Object.keys(data)
        .sort(
          function (a, b) {
            // See https://stackoverflow.com/questions/990904/remove-accents-diacritics-in-a-string-in-javascript#5912746
            //     https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/normalize
            //     https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Collator
            var nameA = data[a].nome_arp.normalize('NFD').replace(/[\u0300-\u036f]/g, "");
            var nameB = data[b].nome_arp.normalize('NFD').replace(/[\u0300-\u036f]/g, "");

            if (nameA < nameB)
              return -1;
            if (nameA > nameB)
              return 1;
            return 0;
          }
        )
        .forEach(function(v, i) {
          result.push(data[v]);
        });

      return result;
    },

    getGlobals: function() {
      return globals;
    },
  }
}
