import 'jquery-blockui';

export function ui(self) {
  return {
    blockUI: function(params) {
      if (jQuery.unblockUI == undefined) {
        return;
      }

      if (params.name == undefined) {
        params.name = 'default';
      }

      if (self.blockUI[params.name] == undefined || self.blockUI[params.name] != true) {
        self.blockUI[params.name] = true;

        if (self.settings.overlaySelector != undefined) {
          jQuery(self.settings.overlaySelector).block(params);
        }
        else {
          jQuery.blockUI(params);
        }
      }
    },

    unblockUI: function(name) {
      if (jQuery.unblockUI == undefined) {
        return;
      }

      if (name == undefined) {
        name = 'default';
      }

      if (self.blockUI[name] != undefined && self.blockUI[name] == true) {
        delete self.blockUI[name];
      }

      if (self.blockUI.length == 0) {
        if (self.settings.overlaySelector != undefined) {
          jQuery(self.settings.overlaySelector).unblock();
        }
        else {
          jQuery.unblockUI();
        }
      }
    },

    showOverlay: function(name) {
      if (jQuery.unblockUI == undefined) {
        return;
      }

      if (self.settings.disableOverlay != undefined && self.settings.disableOverlay == true) {
        return;
      }

      //if (jQuery.browser.msie === false || jQuery.browser.msie === undefined) {
        var css;
        var message = self.settings.overlayMessage;

        if (self.settings.overlayCss != undefined) {
          css = self.settings.overlayCss;
        }
        else {
          css = { };
        }

        var params = {
          name:    name,
          message: message,
          css:     css,
        };

        if (self.settings.overlayTimeout != undefined && self.settings.overlayTimeout != false) {
          params.timeout = timeout;
        }

        self.methods.blockUI(params);
      //}
    },

    statusMessageLoading: function(params) {
      if (params == undefined) {
        params = {};
      }

      if (params.name == undefined) {
        params.name = 'default';
      }

      if (self.statusMessages[params.name] == undefined || self.statusMessages[params.name] != true) {
        self.statusMessages[params.name] = true;

        jQuery('#' + self.settings.mapId + '_status').html(self.settings.overlayMessage);
      }
    },

    statusMessageReady: function(name) {
      if (name == undefined) {
        name = 'default';
      }

      if (self.statusMessages[name] != undefined && self.statusMessages[name] == true) {
        delete self.statusMessages[name];
      }

      if (self.statusMessages.length == 0) {
        jQuery('#' + self.settings.mapId + '_status').html('');
      }
    },

    localeTransition: function(value) {
      self.settings.lang = value;
      self.i18n.locale   = value;

      self.methods.updateEncodedSettings();
      self.bus.$emit('locale-transition', value);
    },
  }
}
