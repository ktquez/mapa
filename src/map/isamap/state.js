import { getQueryString } from '../../common/url_query.js';

export function state(self) {
  return {
    addLayerIntoSettings: function(group, layer) {
      if (group == 'arps') {
        return;
      }

      var entry = group + '.' + layer;

      if (self.settings.layers.indexOf(entry) == -1) {
        self.settings.layers.push(entry);
        //self.activeMapEntities.push(e.layer._leaflet_id);
      }

      // Update encoded settings
      self.methods.updateEncodedSettings();
    },

    hideLayerFromSettings: function(group, layer) {
      var entry = group + '.' + layer;
      var index = self.settings.layers.indexOf(entry);

      if (index != -1) {
        self.settings.layers.splice(index, 1);
        //self.activeMapEntities.splice(activeIndex, 1);
      }

      // Update encoded settings
      self.methods.updateEncodedSettings();
    },

    mapStateTracking: function() {
      // Build table of layer IDs
      //for (var group in self.layers) {
      //  for (var layer in self.layers[group].layers) {
      //    if (self.layers[group].layers[layer].layer != undefined) {
      //      if (self.layers[group].layers[layer].layer._leaflet_id != undefined) {
      //        mainLayerIds[self.layers[group].layers[layer].layer._leaflet_id] = false;
      //      }
      //    }
      //  }
      //}

      // Base layer
      self.trackingEvents.baselayerchange = self.map.on('layeradd', function(e) {
        for (var group in self.geo.layers.base) {
          for (var layer in self.geo.layers.base[group].layers) {
            if (self.geo.layers.base[group].layers[layer].layer._leaflet_id == e.layer._leaflet_id) {
              self.settings.baseLayer = group + '.' + layer;
            }
          }
        }

        // Update encoded settings
        self.methods.updateEncodedSettings();
      });

      // Zoom
      self.trackingEvents.zoomend = self.map.on('zoomend', function(e) {
        self.settings.zoom = self.map.getZoom();

        // Update encoded settings
        self.methods.updateEncodedSettings();
      });

      // Center
      self.trackingEvents.moveend = self.map.on('moveend', function(e) {
        var center      = self.map.getCenter();
        self.settings.center = [ center.lat, center.lng ];

        // Update encoded settings
        self.methods.updateEncodedSettings();
      });

      self.trackingEvents.overlayadd = self.map.on('layeradd', function(e) {
        // Check if is a main layer
        //if (mainLayerIds[e.layer._leaflet_id] == undefined) {
        //  return;
        //}
        //else if (mainLayerIds[e.layer._leaflet_id] == true) {
        //  return;
        //}
        //else {
        //  mainLayerIds[e.layer._leaflet_id] = true;
        //}

        // Use a simple array to speed up search process
        //if (self.activeMapEntities.indexOf(e.layer._leaflet_id) != -1) {
        //  debug('Layer already tracked:' + e.layer._leaflet_id);
        //  return;
        //}

        for (var group in self.layers) {
          // Skip arps group
          if (group == 'arps') {
            continue;
          }

          for (var layer in self.layers[group].layers) {
            if (self.layers[group].layers[layer].layer._leaflet_id == e.layer._leaflet_id) {
              self.methods.addLayerIntoSettings(group, layer);
            }
          }
        }

        // Update encoded settings
        //updateEncodedSettings();
      });

      // Layer remove
      self.trackingEvents.overlayremove = self.map.on('layerremove', function(e) {
        // Check if is a main layer
        //if (mainLayerIds[e.layer._leaflet_id] == undefined) {
        //  return;
        //}
        //else if (mainLayerIds[e.layer._leaflet_id] == false) {
        //  return;
        //}
        //else {
        //  mainLayerIds[e.layer._leaflet_id] = false;
        //}

        // Use a simple array to speed up search process
        //var activeIndex = self.activeMapEntities.indexOf(e.layer._leaflet_id);
        //if (activeIndex == -1) {
        //  return;
        //}

        for (var group in self.layers) {
          // Skip arps group
          if (group == 'arps') {
            continue;
          }

          for (var layer in self.layers[group].layers) {
            if (self.layers[group].layers[layer].layer._leaflet_id == e.layer._leaflet_id) {
              self.methods.hideLayerFromSettings(group, layer);
            }
          }
        }

        // Update encoded settings
        //updateEncodedSettings();
      });
    },

    updateEncodedSettings: function() {
      // Encode just some settings
      var options = [ 'lang', 'arps', 'layers', 'baseLayer', 'center', 'zoom', 'minZoom', 'maxZoom' ]
      var encoded = getQueryString();
      var title   = self.i18n.messages[self.settings.lang].name;
      var url     = window.location.href;

      for (var option in options) {
        if (self.settings[options[option]] != undefined) {
          encoded[options[option]] = self.settings[options[option]];
        }
      }

      self.encodedSettings = jQuery.param(encoded);

      // Refresh control
      if (self.controls.embed_code != undefined) {
        self.controls.embed_code.update();
      }

      // Notify existing parent instances about the new params
      if (window.parent != window) {
        window.parent.postMessage({
          updateEncodedSettings: encoded,
        }, '*');
      }

      // Update location: hash version
      //if (self.settings.hashChange == true) {
      //  window.location.hash = self.encodedSettings;
      //}
      // Update location: replaceState version
      if (self.settings.urlChange == true) {
        var title = self.i18n.messages[self.settings.lang].name;
        var url   = window.location.protocol + '//' + window.location.hostname + window.location.pathname + '?' + self.encodedSettings;

        // See https://stackoverflow.com/questions/824349/modify-the-url-without-reloading-the-page#3354511
        //     https://stackoverflow.com/questions/2494213/changing-window-location-without-triggering-refresh
        //window.location.href = url;
        window.history.replaceState({}, title, url);
      }
    },
  }
}
