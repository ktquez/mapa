var globals = require('../globals');

export function geocode(self) {
  return {
    geocode: function(params) {
      if (params.address == undefined) {
        return false;
      }

      if (globals.geocode[params.address] != undefined) {
        return globals.geocode[params.address];
      }

      $.ajax({
        dataType: 'json',
        url:       self.settings.geocodeUrl + '/' + params.address,
        success:   function(response) {
          if (response.status != "OK") {
            return;
          }

          globals.geocode[params.address] = response.results;

          if (params.callback != undefined) {
            params.callback(params, response.results);
          }
        }
      });
    },
  }
}
