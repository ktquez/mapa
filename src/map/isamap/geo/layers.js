//import L from '../../leaflet.js';
//import omnivore from '@mapbox/leaflet-omnivore';

var globals = require('../../globals');

export function layers(self) {
  return {
    'base': {
      'base': {
        groupName: 'basemap',
        expanded: false,
        localize: true,
        layers: {
          topographic: {
            description: 'topographic',
            layer: L.esri.basemapLayer('Topographic', {
              minZoom      : self.minZoom,
              maxZoom      : self.maxZoom,
              detectRetina : true,
              attribution  : '&copy; <a href="https://www.arcgis.com/home/index.html">Esri</a>'
            }),
          },
          gray: {
            description: 'gray',
            layer: L.esri.basemapLayer('Gray', {
              minZoom      : self.minZoom,
              maxZoom      : self.maxZoom,
              detectRetina : true,
              attribution  : '&copy; <a href="https://www.arcgis.com/home/index.html">Esri</a>'
            }),
          },
          darkgray: {
            description: 'darkgray',
            layer: L.esri.basemapLayer('DarkGray', {
              minZoom      : self.minZoom,
              maxZoom      : self.maxZoom,
              detectRetina : true,
              attribution  : '&copy; <a href="https://www.arcgis.com/home/index.html">Esri</a>'
            }),
          },
          imagery: {
            description: 'imagery',
            layer: L.esri.basemapLayer('Imagery', {
              minZoom      : self.minZoom,
              maxZoom      : self.maxZoom,
              detectRetina : true,
              attribution  : '&copy; <a href="https://www.arcgis.com/home/index.html">Esri</a>'
            }),
          },
          terrain: {
            description: 'terrain',
            layer: L.esri.basemapLayer('Terrain', {
              minZoom      : self.minZoom,
              maxZoom      : self.maxZoom,
              detectRetina : true,
              attribution  : '&copy; <a href="https://www.arcgis.com/home/index.html">Esri</a>'
            }),
          },
          community: {
            description: 'communitary',
            layer: L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
              minZoom      : self.minZoom,
              maxZoom      : self.maxZoom,
              detectRetina : true,
              attribution  : '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors'
              }
            ),
          },
          streets: {
            description: 'streets',
            layer: L.esri.basemapLayer('Streets', {
              minZoom      : self.minZoom,
              maxZoom      : self.maxZoom,
              detectRetina : true,
              attribution  : '&copy; <a href="https://www.arcgis.com/home/index.html">Esri</a>'
            }),
          },
          humanitary: {
            description: 'humanitary',
            layer: L.tileLayer('https://tile-{s}.openstreetmap.fr/hot/{z}/{x}/{y}.png', {
              minZoom      : self.minZoom,
              maxZoom      : self.maxZoom,
              detectRetina : true,
              attribution  : '&copy; <a href="https://hotosm.org/">HOT</a>'
            }),
          },
          satellite: {
            description: 'satelitte',
            layer: L.esri.basemapLayer('Imagery', {
              minZoom      : self.minZoom,
              maxZoom      : self.maxZoom,
              detectRetina : true,
              attribution  : '&copy; <a href="https://www.arcgis.com/home/index.html">Esri</a>'
            }),
          },
          /**
           * https://stackoverflow.com/questions/9394190/leaflet-map-api-with-google-satellite-layer
           * https://github.com/Leaflet/Leaflet/blob/master/FAQ.md#data-providers
           * https://developers.google.com/maps/terms#10-license-restrictions
           */
          /*
          googleSatellite: {
            description: 'satelitte' + ' (Google)',
            layer: L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',{
              minZoom      : self.minZoom,
              maxZoom      : self.maxZoom,
              detectRetina : true,
              subdomains   : ['mt0','mt1','mt2','mt3'],
              attribution  : '&copy; <a href="https://maps.google.com">Google</a>'
            }),
          },
          googleStreets: {
            description: 'streets' + ' (Google)',
            layer: L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',{
              minZoom      : self.minZoom,
              maxZoom      : self.maxZoom,
              subdomains   : ['mt0','mt1','mt2','mt3'],
              detectRetina : true,
              attribution  : '&copy; <a href="https://maps.google.com">Google</a>'
            }),
          },
          googleHybrid: {
            description: 'hybrid' + ' (Google)',
            layer: L.tileLayer('http://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}',{
              minZoom      : self.minZoom,
              maxZoom      : self.maxZoom,
              subdomains   : ['mt0','mt1','mt2','mt3'],
              detectRetina : true,
              attribution  : '&copy; <a href="https://maps.google.com">Google</a>'
            }),
          },
          googleTerrain: {
            description: 'terrain' + ' (Google)',
            layer: L.tileLayer('http://{s}.google.com/vt/lyrs=p&x={x}&y={y}&z={z}',{
              minZoom      : self.minZoom,
              maxZoom      : self.maxZoom,
              subdomains   : ['mt0','mt1','mt2','mt3'],
              detectRetina : true,
              attribution  : '&copy; <a href="https://maps.google.com">Google</a>'
            }),
          },
          */
        },
      },
    },
    default: {
      tis: {
        groupName: 'indigenous_lands',
        expanded: false,
        layers: {
          limits: {
            description: 'indigenous_lands_limits',
            layer: L.esri.dynamicMapLayer({
              disableCache: false,
              format: 'png32',
              url:     self.geo.services.gisLayers.tisLimits.url,
              proxy:   self.settings.proxyUrl,
              //token:   apiKey,
              layers:  [ self.geo.services.gisLayers.tisLimits.layer ],
              opacity: 1,
            }),
            identify: function (error, featureCollection) {
              if (error || featureCollection.features.length === 0) {
                return false;
              } else {
                var content = self.methods.arpPopupContents(featureCollection.features[0]);

                return content;
              }
            },
            legend: [
              {
                html: '',
                style: {
                  "opacity"          : "1",
                  "background-color" : "#ff5500",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              }
            ],
          },
          markers: {
            description: 'indigenous_lands_points',
            layer: L.esri.featureLayer({
              url:          self.methods.gisLayerUrl('tisMarkers'),
              proxy:        self.settings.proxyUrl,
              //token:        apiKey,
              attribution:  'attribution',
              pointToLayer: function (geojson, latlng) {
                return L.marker(latlng, {
                  icon: L.icon({
                    //iconUrl: self.settings.imagesUrl + '/ti/ti_feather.png',
                    //iconSize: [22, 28],
                    iconUrl:  self.methods.tiIcon(globals.arps.tis[geojson.properties.id_arp]),
                    iconSize: [32, 32],
                  }),
                }).bindTooltip(self.methods.arpPopupContents(geojson, false));
              },
            }),
            click: function(e) {
              var content = self.methods.arpPopupContents(e.layer.feature);

              e.layer.bindPopup(content);
              e.layer.openPopup();
            },
            //legend: [
            //  {
            //    //label: 'indigenous_lands' + self.methods.message('points'),
            //    //html: '<img src="'+ self.settings.imagesUrl + '/ti/ti_feather.png" />',
            //    html: '',
            //    style: {
            //      //"background-image" : "url('" + self.settings.imagesUrl + "/ti/ti_feather.png')",
            //      //"width"            : "22px",
            //      //"height"           : "28px"
            //      "background-image" : "url('" + self.settings.imagesUrl + "/ti/ti_square.png')",
            //      "width"            : "32px",
            //      "height"           : "32px"
            //    },
            //  }
            //],
            legend: [
              {
                label: 'tis.identificacao',
                html: '<img src="' + self.settings.imagesUrl + '/ti/ti_identificacao.png">',
              },
              {
                label: 'tis.identificada',
                html: '<img src="' + self.settings.imagesUrl + '/ti/ti_identificada.png">',
              },
              {
                label: 'tis.declarada',
                html: '<img src="' + self.settings.imagesUrl + '/ti/ti_declarada.png">',
              },
              {
                label: 'tis.reservada',
                html: '<img src="' + self.settings.imagesUrl + '/ti/ti_reservada.png">',
              },
              {
                label: 'tis.restricao',
                html: '<img src="' + self.settings.imagesUrl + '/ti/ti_restricao.png">',
              },
              {
                label: 'tis.homologada',
                html: '<img src="' + self.settings.imagesUrl + '/ti/ti_homologada.png">',
              },
              {
                label: 'no_info',
                html: '<img src="' + self.settings.imagesUrl + '/ti/ti_square.png">',
              }
            ],
          },
        },
      },
      ucs: {
        groupName: 'conservation_areas',
        expanded: false,
        layers: {
          limitsEstaduais: {
            description: 'cus_state_limits',
            layer: L.esri.dynamicMapLayer({
              disableCache: false,
              format: 'png32',
              url:     self.geo.services.gisLayers.ucsLimitsEstaduais.url,
              proxy:   self.settings.proxyUrl,
              //token:   apiKey,
              layers:  [ self.geo.services.gisLayers.ucsLimitsEstaduais.layer ],
              opacity: 1,
            }),
            identify: function (error, featureCollection) {
              if (error || featureCollection.features.length === 0) {
                return false;
              } else {
                var content = self.methods.arpPopupContents(featureCollection.features[0]);

                return content;
              }
            },
            legend: [
              {
                html: '',
                style: {
                  "opacity"          : "1",
                  "background-color" : "#ff73df",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              }
            ],
          },
          limitsFederais: {
            description: 'cus_federal_limits',
            layer: L.esri.dynamicMapLayer({
              disableCache: false,
              format: 'png32',
              url:     self.geo.services.gisLayers.ucsLimitsFederais.url,
              proxy:   self.settings.proxyUrl,
              //token:   apiKey,
              layers:  [ self.geo.services.gisLayers.ucsLimitsFederais.layer ],
              opacity: 1,
            }),
            identify: function (error, featureCollection) {
              if (error || featureCollection.features.length === 0) {
                return false;
              } else {
                var content = self.methods.arpPopupContents(featureCollection.features[0]);

                return content;
              }
            },
            legend: [
              {
                html: '',
                style: {
                  "opacity"          : "1",
                  "background-color" : "#ffff00",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              }
            ],
          },
          markersEstaduais: {
            description:  'cus_state_points',
            layer: L.esri.featureLayer({
              url:          self.methods.gisLayerUrl('ucsMarkersEstaduais'),
              proxy:        self.settings.proxyUrl,
              //token:        apiKey,
              // Restrict markers only to State UCs inside the Legal Amazon
              //where:        'CONSEST_ISA_PL.amazonia = 1',
              attribution:  'attribution',
              pointToLayer: function (geojson, latlng) {
                // This layer had a weird field naming scheme and we fixed it manually to adapt to our standard
                //geojson.properties.id_arp = geojson.properties['pontos_aps_isa_pt.id_arp'];

                return L.marker(latlng, {
                  icon: L.icon({
                    iconUrl: self.settings.imagesUrl + '/uc/estadual.png',
                    iconSize: [16, 24],
                  }),
                }).bindTooltip(self.methods.arpPopupContents(geojson, false));
              },
            }),
            click: function(e) {
              // This layer had a weird field naming scheme and we fixed it manually to adapt to our standard
              //e.layer.feature.properties.id_arp = e.layer.feature.properties['pontos_aps_isa_pt.id_arp'];
              var content = self.methods.arpPopupContents(e.layer.feature);

              e.layer.bindPopup(content);
              e.layer.openPopup();
            },
            legend: [
              {
                //label: 'cus' + ' ' + self.methods.message('state') + self.methods.message('points'),
                //html: '<img src="'+ self.settings.imagesUrl + '/uc/estadual.png" />',
                html: '',
                style: {
                  "background-image" : "url('" + self.settings.imagesUrl + "/uc/estadual.png')",
                  "width"            : "16px",
                  "height"           : "24px"
                },
              }
            ],
          },
          markersFederais: {
            description: 'cus_federal_points',
            layer: L.esri.featureLayer({
              url:          self.methods.gisLayerUrl('ucsMarkersFederais'),
              proxy:        self.settings.proxyUrl,
              //token:        apiKey,
              attribution:  'attribution',
              pointToLayer: function (geojson, latlng) {
                return L.marker(latlng, {
                  icon: L.icon({
                    iconUrl: self.settings.imagesUrl + '/uc/federal.png',
                    iconSize: [16, 24],
                  }),
                }).bindTooltip(self.methods.arpPopupContents(geojson, false));
              },
            }),
            click: function(e) {
              var content = self.methods.arpPopupContents(e.layer.feature);

              e.layer.bindPopup(content);
              e.layer.openPopup();
            },
            legend: [
              {
                //label: 'cus' + ' ' + self.methods.message('federal') + self.methods.message('points'),
                //html: '<img src="'+ self.settings.imagesUrl + '/uc/federal.png" />',
                html: '',
                style: {
                  "background-image" : "url('" + self.settings.imagesUrl + "/uc/federal.png')",
                  "width"            : "16px",
                  "height"           : "24px"
                },
              }
            ],
          },
      /*
        },
      },
      mosaics: {
        groupName: 'mosaics_corridors',
        expanded: false,
        layers: {
      */
          /*
          ucsMosaicosOutros: {
            //description: 'mosaics_corridors',
            description: 'mosaics',
            layer: L.esri.dynamicMapLayer({
              disableCache: false,
              format: 'png32',
              url:     self.geo.services.gisLayers.ucsMosaicosCorredores.url,
              proxy:   self.settings.proxyUrl,
              //token:   apiKey,
              layers:  [ self.geo.services.gisLayers.ucsMosaicosCorredores.layer ],
              opacity: 1,
            }),
            identify: function (error, featureCollection) {
              if (error || featureCollection.features.length === 0) {
                return false;
              } else {
                var attributes = featureCollection.features[0].properties;

                if (attributes.nome_mos == undefined) {
                  return '';
                }

                var content = self.methods.htmlTable('mosaic', {
                  'name'    : attributes.nome_mos,
                  'category': attributes.categoria,
                  'area_ha' : attributes.area_ha,
                  'status'  : attributes.status,
                  //'ID'    : attributes.id_mos,
                });

                return '<div class="isamap-identify isamap-identify-mosaicos">' + content + '</div>';
              }
            },
            legend: [
              {
                label: 'mosaic',
                html: '',
                style: {
                  "opacity"          : "1",
                  "background-color" : "#ff0000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
              {
                label: 'corridor',
                html: '',
                style: {
                  "opacity"          : "1",
                  "background-color" : "#0000ff",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
            ],
          },
          */
          rbiosf: {
            //description: 'mosaics_corridors',
            description: 'rbiosf',
            layer: L.esri.dynamicMapLayer({
              disableCache: false,
              format: 'png32',
              url:     self.geo.services.gisLayers.rbiosf.url,
              proxy:   self.settings.proxyUrl,
              //token:   apiKey,
              layers:  [ self.geo.services.gisLayers.rbiosf.layer ],
              opacity: 1,
            }),
            identify: function (error, featureCollection) {
              if (error || featureCollection.features.length === 0) {
                return false;
              } else {
                var attributes = featureCollection.features[0].properties;

                if (attributes.NOME_UC == undefined) {
                  return '';
                }

                var content = self.methods.htmlTable('rbiosf', {
                  'name'       : attributes.NOME_UC,
                  'category'   : attributes.categoria,
                  'area_ha'    : attributes.area_ha,
                  'status'     : attributes.status,
                  'description': attributes['Histórico'],
                  'source'     : attributes.Fonte,
                });

                return '<div class="isamap-identify isamap-identify-rbiosf">' + content + '</div>';
              }
            },
            legend: [
              {
                label: 'rbiosf',
                html: '',
                style: {
                  "opacity"          : "1",
                  "background-color" : "#8400a8",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
            ],
          },
          mosaics: {
            //description: 'mosaicss_corridors',
            description: 'mosaics',
            layer: L.esri.dynamicMapLayer({
              disableCache: false,
              format: 'png32',
              url:     self.geo.services.gisLayers.mosaics.url,
              proxy:   self.settings.proxyUrl,
              //token:   apiKey,
              layers:  [ self.geo.services.gisLayers.mosaics.layer ],
              opacity: 1,
            }),
            identify: function (error, featureCollection) {
              if (error || featureCollection.features.length === 0) {
                return false;
              } else {
                var attributes = featureCollection.features[0].properties;

                if (attributes["Nome da Unidade de Conservação"] == undefined) {
                  return '';
                }

                var content = self.methods.htmlTable('mosaics', {
                  'name'       : attributes["Nome da Unidade de Conservação"],
                  'category'   : attributes.categoria,
                  'area_ha'    : attributes["Área GIS (ha)"],
                  'status'     : attributes.status,
                  'description': attributes.historico,
                  'source'     : attributes.Fonte,
                });

                return '<div class="isamap-identify isamap-identify-mosaics">' + content + '</div>';
              }
            },
            legend: [
              {
                label: 'mosaics',
                html: '',
                style: {
                  "opacity"          : "1",
                  "background-color" : "#ff0000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
            ],
          },
          corridors: {
            //description: 'corridorss_corridors',
            description: 'corridors',
            layer: L.esri.dynamicMapLayer({
              disableCache: false,
              format: 'png32',
              url:     self.geo.services.gisLayers.corridors.url,
              proxy:   self.settings.proxyUrl,
              //token:   apiKey,
              layers:  [ self.geo.services.gisLayers.corridors.layer ],
              opacity: 1,
            }),
            identify: function (error, featureCollection) {
              if (error || featureCollection.features.length === 0) {
                return false;
              } else {
                var attributes = featureCollection.features[0].properties;

                if (attributes["Nome da Unidade de Conservação"] == undefined) {
                  return '';
                }

                var content = self.methods.htmlTable('corridors', {
                  'name'       : attributes["Nome da Unidade de Conservação"],
                  'category'   : attributes.categoria,
                  'area_ha'    : attributes["Área GIS (ha)"],
                  'status'     : attributes.status,
                  'description': attributes.historico,
                  'source'     : attributes.Fonte,
                });

                return '<div class="isamap-identify isamap-identify-corridors">' + content + '</div>';
              }
            },
            legend: [
              {
                label: 'corridors',
                html: '',
                style: {
                  "opacity"          : "1",
                  "background-color" : "#0000ff",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
            ],
          },
          /*
          ramsar: {
            description: 'Sítios RAMSAR',
            layer: omnivore.kml(self.settings.baseUrl + 'data/ramsar/ramsar.kml').on('click', function(e) {
              self.methods.popup(e.latlng, e.layer.feature.properties.description);
            }),
          },
          */
          ramsar: {
            //description: 'mosaics_corridors',
            description: 'ramsar',
            layer: L.esri.dynamicMapLayer({
              disableCache: false,
              format: 'png32',
              url:     self.geo.services.gisLayers.ramsar.url,
              proxy:   self.settings.proxyUrl,
              //token:   apiKey,
              layers:  [ self.geo.services.gisLayers.ramsar.layer ],
              opacity: 1,
            }),
            identify: function (error, featureCollection) {
              if (error || featureCollection.features.length === 0) {
                return false;
              } else {
                var attributes = featureCollection.features[0].properties;

                if (attributes.NOME_UC == undefined) {
                  return '';
                }

                var content = self.methods.htmlTable('ramsar', {
                  'name'       : attributes.NOME_UC,
                  'category'   : attributes.categoria,
                  'area_ha'    : attributes.area_ha,
                  'status'     : attributes.status,
                  'description': attributes['Histórico'],
                  'source'     : attributes.Fonte,
                });

                return '<div class="isamap-identify isamap-identify-ramsar">' + content + '</div>';
              }
            },
            legend: [
              {
                label: 'ramsar',
                html: '',
                style: {
                  "opacity"          : "1",
                  "background-color" : "#a87000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
            ],
          },
        }
      },
      infra: {
        groupName: 'infrastructure',
        expanded: false,
        layers: {
          energia: {
            description: 'energy',
            layer: L.esri.Cluster.featureLayer({
              url:          self.methods.gisLayerUrl('energia'),
              proxy:        self.settings.proxyUrl,
              //token:        apiKey,
              attribution   : 'ANEEL / MME',
              opacity:      0.8,
              polygonOptions: {
                color       : '#e57b08',
                weight      : 4,
                opacity     : 1,
                fillOpacity : 0.5
              },
              spiderfyOnMaxZoom: false,
              disableClusteringAtZoom: 9,
              iconCreateFunction: function (cluster) {
                var childCount = cluster.getChildCount();
                var c          = ' marker-cluster-';

                if (childCount < 10) {
                  c += 'small';
                } else if (childCount < 100) {
                  c += 'medium';
                } else {
                  c += 'large';
                }

                return new L.DivIcon({ html: '<div><img src="' + self.settings.imagesUrl + '/misc/energia.png"> <span><strong>' + childCount + '</strong></span></div>', className: 'energy-cluster marker-cluster' + c, iconSize: new L.Point(40, 40) });
              },
              pointToLayer: function (geojson, latlng) {
                return L.marker(latlng, {
                  icon: L.icon({
                    iconUrl: self.settings.imagesUrl + '/misc/energia.png',
                    iconSize: [22, 25],
                  }),
                });
              },
            }),
            click: function(e) {
              var attributes = e.layer.feature.properties;
              var content    = self.methods.htmlTable('energy', {
                'category'      : attributes.categoria,
                'name'          : attributes.nomeobra,
                'fuel'          : attributes.tipocombustivel,
                'stage'         : attributes.estagio,
                'power'         : attributes.potenciakw,
                'river'         : attributes.rio,
                'coordinates'   : attributes.latitudegms + " " + attributes.longitudegms,
                'municipality'  : attributes.municipio1,
                'legal_act'     : attributes.atolegal,
                'aneel_process' : attributes.processoaneel,
                'owner'         : attributes.proprietario,
              });

              e.layer.bindPopup(content);
              e.layer.openPopup();
            },
            legend: [
              {
                //label: 'energy',
                //html: '<img src="'+ self.settings.imagesUrl + '/misc/energia.png" />',
                html: '',
                style: {
                  "opacity"          : "0.8",
                  "background-image" : "url('" + self.settings.imagesUrl + "/misc/energia.png')",
                  "width"            : "22px",
                  "height"           : "25px"
                },
              }
            ],
          },
          mineracao: {
            description: 'mining',
            layer: L.esri.dynamicMapLayer({
              disableCache: false,
              format: 'png32',
              url:     self.geo.services.gisLayers.mineracao.url,
              proxy:   self.settings.proxyUrl,
              //token:   apiKey,
              layers:  [ self.geo.services.gisLayers.mineracao.layer ],
              opacity: 0.50,
            }),
            identify: function (error, featureCollection) {
              if (error || featureCollection.features.length === 0) {
                return false;
              } else {
                var attributes = featureCollection.features[0].properties;
                var content    = self.methods.htmlTable('mining', {
                  'process'   : attributes.processo,
                  'phase'     : attributes.fase,
                  'year'      : attributes.ano,
                  'area_ha'   : attributes.area_ha,
                  'substance' : attributes.subs,
                  'usage'     : attributes.uso,
                  'state'     : attributes.uf,
                });

                return '<div class="isamap-identify isamap-identify-mineracao">' + content + '</div>';
              }
            },
            legend: [
              {
                label: 'mining_interest',
                html: '',
                style: {
                  "opacity"          : "0.5",
                  "background-color" : "#ffff00",
                  "border"           : "1px solid #ffff8f",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
              {
                label: 'mining_availability',
                html: '',
                style: {
                  "opacity"          : "0.5",
                  "background-color" : "#e58fff",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
              {
                label: 'mining_request',
                html: '',
                style: {
                  "opacity"          : "0.5",
                  "background-color" : "#bbff33",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
              {
                label: 'mining_authorized',
                html: '',
                style: {
                  "opacity"          : "0.5",
                  "background-color" : "#ff3333",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
              /*
              {
                label: 'mining_interest',
                html: '<img src="' + self.settings.imagesUrl + '/mineracao/interesse-em-pesquisar.png">',
              },
              {
                label: 'mining_availability',
                html: '<img src="' + self.settings.imagesUrl + '/mineracao/em-pesquisa-disponibilidade.png">',
              },
              {
                label: 'mining_request',
                html: '<img src="' + self.settings.imagesUrl + '/mineracao/solicitacao-de-extracao.png">',
              },
              {
                label: 'mining_authorized',
                html: '<img src="' + self.settings.imagesUrl + '/mineracao/autorizacao-para-extracao.png">',
              }
              */
            ],
          },
          oleogas: {
            description: 'petroleum',
            layer: L.esri.dynamicMapLayer({
              disableCache: false,
              format: 'png32',
              url:     self.geo.services.gisLayers.petroleo.url,
              proxy:   self.settings.proxyUrl,
              //token:   apiKey,
              layers:  [ self.geo.services.gisLayers.petroleo.layer ],
              opacity: 0.45,
            }),
            identify: function (error, featureCollection) {
              if (error || featureCollection.features.length === 0) {
                return false;
              } else {
                var attributes = featureCollection.features[0].properties;
                var content    = self.methods.htmlTable('petroleum', {
                  'area_ha'   : attributes.area_oficial_ha,
                  'basin'     : attributes.bacia,
                  'company'   : attributes.cia,
                  'fluid'     : attributes.fluido,
                  'name'      : attributes.nome,
                  'turn'      : attributes.rodada,
                  'situation' : attributes.situacao,
                });

                return '<div class="isamap-identify isamap-identify-petroleo">' + content + '</div>';
              }
            },
            legend: [
              {
                label: 'petroleum_prod',
                html: '',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "rgb(255, 0, 0)",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
              {
                label: 'petroleum_exp',
                html: '',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "rgb(230, 230, 0)",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
              {
                label: 'petroleum_dev',
                html: '',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "rgb(255, 170, 0)",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
              {
                label: 'petroleum_devo',
                html: '',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "rgb(190, 255, 232)",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
              /*
              {
                label: 'petroleum_lic',
                html: '',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#ffff00",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
              */
              /*
              {
                label: 'petroleum_dev',
                html: '<img src="' + self.settings.imagesUrl + '/petroleo/petroleo-dev.png">',
              },
              {
                label: 'petroleum_prod',
                html: '<img src="' + self.settings.imagesUrl + '/petroleo/petroleo-prod.png">',
              },
              {
                label: 'petroleum_exp',
                html: '<img src="' + self.settings.imagesUrl + '/petroleo/petroleo-exp.png">',
              },
              {
                label: 'petroleum_lic',
                html: '<img src="' + self.settings.imagesUrl + '/petroleo/petroleo-lic.png">',
              }
              */
            ],
          },
        },
      },
      jurisdicao: {
        groupName: 'jurisdiction',
        expanded: false,
        layers: {
          amlegal: {
            //layer: L.esri.featureLayer({
            //  url:   self.geo.services.gisServices.tematicos + '/LimiteAmazonia/MapServer/0',
            //  //token: apiKey,
            //  proxy: self.settings.proxyUrl,
            //}),
            description: 'amlegal',
            layer: L.esri.dynamicMapLayer({
              disableCache: false,
              format: 'png32',
              url:     self.geo.services.gisLayers.amlegal.url,
              proxy:   self.settings.proxyUrl,
              //token:   apiKey,
              layers:  [ self.geo.services.gisLayers.amlegal.layer ],
              opacity: 0.35,
            }),
            legend: [
              {
                html: '',
                style: {
                  "opacity"          : "0.35",
                  "background-color" : "#b0f58d",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              }
            ],
          },
          mata_atlantica: {
            //layer: L.esri.featureLayer({
            //  url:   self.geo.services.gisServices.tematicos + '/LimiteAmazonia/MapServer/0',
            //  //token: apiKey,
            //  proxy: self.settings.proxyUrl,
            //}),
            description: 'mata_atlantica',
            layer: L.esri.dynamicMapLayer({
              disableCache: false,
              format: 'png32',
              url:     self.geo.services.gisLayers.mata_atlantica.url,
              proxy:   self.settings.proxyUrl,
              //token:   apiKey,
              layers:  [ self.geo.services.gisLayers.mata_atlantica.layer ],
              opacity: 0.35,
            }),
            legend: [
              {
                html: '',
                style: {
                  "opacity"          : "0.35",
                  "background-color" : "#267300",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              }
            ],
          },
        },
      },
      ambiente: {
        groupName: 'environment',
        expanded: false,
        layers: {
          biomas: {
            description: 'biomes',
            layer: L.esri.dynamicMapLayer({
              disableCache: false,
              format: 'png32',
              url:     self.geo.services.gisLayers.biomas.url,
              proxy:   self.settings.proxyUrl,
              //token:   apiKey,
              layers:  [ self.geo.services.gisLayers.biomas.layer ],
              opacity: 0.35,
            }),
            //bindPopup: function (error, featureCollection) {
            //  if (error || featureCollection.features.length === 0) {
            //    return false;
            //  } else {
            //    var attributes = featureCollection.features[0].properties;
            //    var content    = self.methods.htmlTable('Bioma', {
            //      'Nome':      attributes.nome,
            //      'Área (ha)': attributes.area_ha,
            //    });

            //    return content;
            //  }
            //},
            identify: function (error, featureCollection) {
              if (error || featureCollection.features.length === 0) {
                return false;
              } else {
                for (var i in featureCollection.features) {
                  if (featureCollection.features[i].properties.nome != undefined) {
                    var content = self.methods.htmlTable('biome', {
                      'name'   : featureCollection.features[i].properties.nome,
                      'area_ha': featureCollection.features[i].properties.area_ha,
                    });

                    return '<div class="isamap-identify isamap-identify-biomas">' + content + '</div>';
                  }
                }
              }
            },
            legend: [
              {
                label: 'biomes_amazon',
                html: '',
                style: {
                  "opacity"          : "0.35",
                  "background-color" : "#267300",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
              {
                label: 'biomes_caatinga',
                html: '',
                style: {
                  "opacity"          : "0.35",
                  "background-color" : "#e6e600",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
              {
                label: 'biomes_cerrado',
                html: '',
                style: {
                  "opacity"          : "0.35",
                  "background-color" : "#ffaa00",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
              {
                label: 'biomes_atlantic',
                html: '',
                style: {
                  "opacity"          : "0.35",
                  "background-color" : "#38a800",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
              {
                label: 'biomes_pampa',
                html: '',
                style: {
                  "opacity"          : "0.35",
                  "background-color" : "#a3ff73",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
              {
                label: 'biomes_wetland',
                html: '',
                style: {
                  "opacity"          : "0.35",
                  "background-color" : "#00a9e6",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
              /*
              {
                label: 'biomes_amazon',
                html: '<img src="' + self.settings.imagesUrl + '/biomas/amazonia.png">',
              },
              {
                label: 'biomes_caatinga',
                html: '<img src="' + self.settings.imagesUrl + '/biomas/caatinga.png">',
              },
              {
                label: 'biomes_cerrado',
                html: '<img src="' + self.settings.imagesUrl + '/biomas/cerrado.png">',
              },
              {
                label: 'biomes_atlantic',
                html: '<img src="' + self.settings.imagesUrl + '/biomas/mata_atlantica.png">',
              },
              {
                label: 'biomes_pampa',
                html: '<img src="' + self.settings.imagesUrl + '/biomas/pampa.png">',
              },
              {
                label: 'biomes_wetland',
                html: '<img src="' + self.settings.imagesUrl + '/biomas/pantanal.png">',
              },
              */
            ],
          },
          cavernas: {
            description: 'caves',
            layer: L.esri.featureLayer({
              url:          self.methods.gisLayerUrl('cavernas'),
              proxy:        self.settings.proxyUrl,
              //token:        apiKey,
              attribution:  'CECAV / ICMBio / MMA',
              opacity:      0.45,
              pointToLayer: function (geojson, latlng) {
                return L.marker(latlng, {
                  icon: L.icon({
                    iconUrl: self.settings.imagesUrl + '/misc/caverna.png',
                    iconSize: [12, 11],
                  }),
                });
              },
            }),
            click: function(e) {
              var attributes = e.layer.feature.properties;
              var content    = self.methods.htmlTable('cave', {
                'name' :    attributes.caverna,
                'state':    attributes.uf,
                'locality': attributes.localidade,
              });

              e.layer.bindPopup(content);
              e.layer.openPopup();
            },
            legend: [
              {
                //label: 'caves',
                //html: '<img src="' + self.settings.imagesUrl + '/misc/caverna.png">',
                html: '',
                style: {
                  "opacity"          : "0.45",
                  "background-image" : "url('" + self.settings.imagesUrl + "/misc/caverna.png')",
                  "width"            : "12px",
                  "height"           : "11px"
                },
              }
            ],
          },
          fitofisionomia: {
            description: 'vegetation',
            layer: L.esri.dynamicMapLayer({
              disableCache: false,
              format: 'png32',
              url:     self.geo.services.gisLayers.fitofisionomia.url,
              proxy:   self.settings.proxyUrl,
              //token:   apiKey,
              layers:  [ self.geo.services.gisLayers.fitofisionomia.layer ],
              opacity: 0.45,
            }),
            //bindPopup: function (error, featureCollection) {
            //  if (error || featureCollection.features.length === 0) {
            //    return false;
            //  } else {
            //    var attributes = featureCollection.features[0].properties;
            //    var content    = self.methods.htmlTable('Fitofisionomia', {
            //      'Fitofisionomia': attributes.grupo1,
            //      'Fitofisionomia': attributes.Fitofisionomia,
            //    });

            //    return content;
            //  }
            //},
            identify: function (error, featureCollection) {
              if (error || featureCollection.features.length === 0) {
                return false;
              } else {
                var content = '';
                var levels  = {};

                /*
                var content_nv0 = null;
                var content_nv1 = null;
                var content_nv3 = null;
                */

                for (var i in featureCollection.features) {
                  //content += self.methods.htmlTable('Fitofisionomia', {
                  //  'Nível 0': featureCollection.features[i].properties.Legenda_nv0,
                  //  'Nível 1': featureCollection.features[i].properties.Legenda_nv1,
                  //  'Nível 3': featureCollection.features[i].properties.Legenda_nv3,
                  //});

                  /*
                  if (featureCollection.features[i].properties.Legenda_nv0 != undefined) {
                    content_nv0 = featureCollection.features[i].properties.Legenda_nv0;
                  }

                  if (featureCollection.features[i].properties.Legenda_nv1 != undefined) {
                    content_nv1 = featureCollection.features[i].properties.Legenda_nv1;
                  }

                  if (featureCollection.features[i].properties.Legenda_nv3 != undefined) {
                    content_nv3 = featureCollection.features[i].properties.Legenda_nv3;
                  }
                  */

                  if (featureCollection.features[i].properties.Fitofisionomia != undefined) {
                    levels['level' + i] = featureCollection.features[i].properties.Fitofisionomia;
                  }
                }

                /*
                content += self.methods.htmlTable('Fitofisionomia', {
                  'Nível 0': content_nv0,
                  'Nível 1': content_nv1,
                  'Nível 3': content_nv3,
                });
                */

                content += self.methods.htmlTable('vegetation', levels);

                return '<div class="isamap-identify isamap-identify-fitofisionomia">' + content + '</div>';
              }
            },
            legend: [
              {
                label: 'vegetation_campinarana',
                html: '',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#d7995e",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
              {
                label: 'vegetation_contact',
                html: '',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#fffe39",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
              {
                label: 'vegetation_veld',
                html: '',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#ffb898",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
              {
                label: 'vegetation_decidual',
                html: '',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#b9b933",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
              {
                label: 'vegetation_semidecidual',
                html: '',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#8f9033",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
              {
                label: 'vegetation_open_ombrophylous',
                html: '',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#79fe35",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
              {
                label: 'vegetation_dense_ombrophylous',
                html: '',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#6ec664",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
              {
                label: 'vegetation_mixed_ombrophylous',
                html: '',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#b5ff8e",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
              {
                label: 'vegetation_pioneer_formations',
                html: '',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#3373b9",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
              {
                label: 'vegetation_ecological_refugees',
                html: '',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#f0f7a9",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
              {
                label: 'vegetation_savanna',
                html: '',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#ffbb34",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
              {
                label: 'vegetation_veldt_savanna',
                html: '',
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#b98d34",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
              /*
              {
                label: 'vegetation_campinarana',
                html: '<img src="' + self.settings.imagesUrl + '/fitofisionomia/campinarana.png">',
              },
              {
                label: 'vegetation_contact',
                html: '<img src="' + self.settings.imagesUrl + '/fitofisionomia/contato.png">',
              },
              {
                label: 'vegetation_veld',
                html: '<img src="' + self.settings.imagesUrl + '/fitofisionomia/estepe.png">',
              },
              {
                label: 'vegetation_decidual',
                html: '<img src="' + self.settings.imagesUrl + '/fitofisionomia/floresta-estacional-decidual.png">',
              },
              {
                label: 'vegetation_semidecidual',
                html: '<img src="' + self.settings.imagesUrl + '/fitofisionomia/floresta-estacional-semidecidual.png">',
              },
              {
                label: 'vegetation_open_ombrophylous',
                html: '<img src="' + self.settings.imagesUrl + '/fitofisionomia/floresta-ombrofila-aberta.png">',
              },
              {
                label: 'vegetation_dense_ombrophylous',
                html: '<img src="' + self.settings.imagesUrl + '/fitofisionomia/floresta-ombrofila-densa.png">',
              },
              {
                label: 'vegetation_mixed_ombrophylous',
                html: '<img src="' + self.settings.imagesUrl + '/fitofisionomia/floresta-ombrofila-mista.png">',
              },
              {
                label: 'vegetation_pioneer_formations',
                html: '<img src="' + self.settings.imagesUrl + '/fitofisionomia/formacoes-pioneiras.png">',
              },
              {
                label: 'vegetation_ecological_refugees',
                html: '<img src="' + self.settings.imagesUrl + '/fitofisionomia/refugio-ecologico.png">',
              },
              {
                label: 'vegetation_savanna',
                html: '<img src="' + self.settings.imagesUrl + '/fitofisionomia/savana.png">',
              },
              {
                label: 'vegetation_veldt_savanna',
                html: '<img src="' + self.settings.imagesUrl + '/fitofisionomia/savana-estepica.png">',
              },
              */
            ],
          },
          /*
          bacias: {
            description: 'watersheds',
            layer: L.esri.dynamicMapLayer({
              disableCache: false,
              format: 'png32',
              url:     self.geo.services.gisLayers.bacias.url,
              proxy:   self.settings.proxyUrl,
              //token:   apiKey,
              layers:  [ self.geo.services.gisLayers.bacias.layer ],
              opacity: 0.45,
            }),
            //bindPopup: function (error, featureCollection) {
            //  if (error || featureCollection.features.length === 0) {
            //    return false;
            //  } else {
            //    var attributes = featureCollection.features[0].properties;
            //    var content    = self.methods.htmlTable('Bacia', {
            //      'Bacia':  attributes.bacia,
            //      'Bacia':  attributes['BACIA (NIVEL 2)'],
            //      'Região': attributes.rg_hidro,
            //      'Região': attributes['REGIÃO HIDROGRÁFICA (NIVEL 1)'],
            //    });

            //    return content;
            //  }
            //},
            identify: function (error, featureCollection) {
              if (error || featureCollection.features.length === 0) {
                return false;
              } else {
                var content = '';

                for (var i in featureCollection.features) {
                  if (featureCollection.features[i].properties.nunivotto1 != undefined) {
                    content += self.methods.htmlTable('Bacia Nível 1', {
                      'Nome':      featureCollection.features[i].properties['Nome da Bacia'],
                      'Área (ha)': featureCollection.features[i].properties.area_ha,
                    });
                  }
                  else if (featureCollection.features[i].properties['Nível Otto 2'] != undefined) {
                    content += self.methods.htmlTable('Bacia Nível 2', {
                      'Nome':      featureCollection.features[i].properties['Nome da Bacia'],
                      'Área (ha)': featureCollection.features[i].properties.area_ha,
                    });
                  }
                  else if (featureCollection.features[i].properties.nunivotto3 != undefined) {
                    content += self.methods.htmlTable('Bacia Nível 3', {
                      'Nome':      featureCollection.features[i].properties['Nome da Bacia'],
                      'Área (ha)': featureCollection.features[i].properties.area_ha,
                    });
                  }
                }

                return '<div class="isamap-identify isamap-identify-bacias">' + content + '</div>';
              }
            },
            legend: [
              {
                label: 'basin_amazon',
                html: '<img src="' + self.settings.imagesUrl + '/bacias/bacia-amazonica.png">',
              },
              {
                label: 'basin_araguaia_tocantins',
                html: '<img src="' + self.settings.imagesUrl + '/bacias/bacia-araguaia-tocantins.png">',
              },
              {
                label: 'basin_east_atlantic',
                html: '<img src="' + self.settings.imagesUrl + '/bacias/bacia-atlantico-leste.png">',
              },
              {
                label: 'basin_western_northeast',
                html: '<img src="' + self.settings.imagesUrl + '/bacias/bacia-atlantico-nordeste-ocidental.png">',
              },
              {
                label: 'basin_atlantic_southeast',
                html: '<img src="' + self.settings.imagesUrl + '/bacias/bacia-atlantico-sudeste.png">',
              },
              {
                label: 'basin_south_atlantico',
                html: '<img src="' + self.settings.imagesUrl + '/bacias/bacia-atlantico-sul.png">',
              },
              {
                label: 'basin_paraguay',
                html: '<img src="' + self.settings.imagesUrl + '/bacias/bacia-paraguai.png">',
              },
              {
                label: 'basin_parana',
                html: '<img src="' + self.settings.imagesUrl + '/bacias/bacia-parana.png">',
              },
              {
                label: 'basin_parnaiba',
                html: '<img src="' + self.settings.imagesUrl + '/bacias/bacia-parnaiba.png">',
              },
              {
                label: 'basin_sao_francisco',
                html: '<img src="' + self.settings.imagesUrl + '/bacias/bacia-sao-francisco.png">',
              },
              {
                label: 'basin_uruguay',
                html: '<img src="' + self.settings.imagesUrl + '/bacias/bacia-uruguai.png">',
              },
            ],
          },
          */
          ottobacias: {
            description: 'otto',
            layer: L.esri.dynamicMapLayer({
              disableCache: false,
              format: 'png32',
              url:     self.geo.services.gisLayers.ottobacias.url,
              proxy:   self.settings.proxyUrl,
              //token:   apiKey,
              layers:  [ self.geo.services.gisLayers.ottobacias.layer ],
              opacity: 0.45,
            }),
            identify: function (error, featureCollection) {
              if (error || featureCollection.features.length === 0) {
                return false;
              } else {
                var content = '';
                var levels  = {};

                for (var i in featureCollection.features) {
                  if (featureCollection.features[i].properties.nunivotto1 != undefined) {
                    content += self.methods.htmlTable('basin', {
                      'level'  : '1',
                      'name'   : featureCollection.features[i].properties['Nome da Bacia'],
                      'area_ha': featureCollection.features[i].properties.area_ha,
                    });
                  }

                  if (featureCollection.features[i].properties['Nível Otto 2'] != undefined) {
                    content += self.methods.htmlTable('basin', {
                      'level'  : '2',
                      'name'   : featureCollection.features[i].properties['Nome da Bacia'],
                      'area_ha': featureCollection.features[i].properties.area_ha,
                    });
                  }

                  if (featureCollection.features[i].properties.nunivotto3 != undefined) {
                    content += self.methods.htmlTable('basin', {
                      'level'  : '3',
                      'name'   : featureCollection.features[i].properties['Nome da Bacia'],
                      'area_ha': featureCollection.features[i].properties.area_ha,
                    });
                  }
                }

                /*
                for (var i in featureCollection.features) {
                  if (featureCollection.features[i].properties.nunivotto1 != undefined) {
                    levels['level'] = self.methods.htmlTable('1', {
                      'name'   : featureCollection.features[i].properties['Nome da Bacia'],
                      'area_ha': featureCollection.features[i].properties.area_ha,
                    });
                  }

                  if (featureCollection.features[i].properties['Nível Otto 2'] != undefined) {
                    levels['level'] = self.methods.htmlTable('2', {
                      'name'   : featureCollection.features[i].properties['Nome da Bacia'],
                      'area_ha': featureCollection.features[i].properties.area_ha,
                    });
                  }

                  if (featureCollection.features[i].properties.nunivotto3 != undefined) {
                    levels['level'] = self.methods.htmlTable('3', {
                      'name'   : featureCollection.features[i].properties['Nome da Bacia'],
                      'area_ha': featureCollection.features[i].properties.area_ha,
                    });
                  }
                }

                content += self.methods.htmlTable('otto', levels);
                */

                return '<div class="isamap-identify isamap-identify-ottobacias">' + content + '</div>';
              }
            },
            legend: [
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(230, 152, 0)",
                },
                "label": "Amazônica",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(173, 39, 36)",
                },
                "label": "Araguaia-Tocantins",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(166, 73, 112)",
                },
                "label": "Atlântico Norte",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(69, 140, 168)",
                },
                "label": "Costa Atlântica",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(230, 230, 0)",
                },
                "label": "Marajó",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(136, 62, 173)",
                },
                "label": "Orinoco",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(47, 79, 161)",
                },
                "label": "Prata",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(152, 173, 45)",
                },
                "label": "Alto Araguaia",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(137, 44, 163)",
                },
                "label": "Alto Orinoco",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(169, 173, 81)",
                },
                "label": "Alto Paraná",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(42, 130, 64)",
                },
                "label": "Alto Rio Amazonas",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(173, 150, 87)",
                },
                "label": "Baixo Araguaia",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(49, 176, 108)",
                },
                "label": "Baixo Paraná",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(153, 44, 79)",
                },
                "label": "Baixo Rio Orinoco",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(47, 126, 130)",
                },
                "label": "Baixo Rio Orinoco II",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(39, 176, 169)",
                },
                "label": "Canal das Tartarugas",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(128, 37, 40)",
                },
                "label": "Foz Rio Cuyuni-Rupununi",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(114, 46, 140)",
                },
                "label": "Foz do Rio Jari",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(100, 75, 163)",
                },
                "label": "Foz do Rio Orinoco",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(45, 150, 166)",
                },
                "label": "Litoral Guiana",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(168, 39, 41)",
                },
                "label": "Litoral Ilha de Marajó",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(145, 36, 96)",
                },
                "label": "Litoral Ilha de Marajó II",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(176, 111, 46)",
                },
                "label": "Litoral Ilha de Marajó III",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(138, 75, 52)",
                },
                "label": "Litoral Leste",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(168, 64, 67)",
                },
                "label": "Litoral Nordeste Ocidental",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(150, 56, 44)",
                },
                "label": "Litoral Nordeste Ocidental II",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(143, 103, 70)",
                },
                "label": "Litoral Nordeste Ocidental III",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(80, 171, 92)",
                },
                "label": "Litoral Nordeste Oriental",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(37, 46, 168)",
                },
                "label": "Litoral Sudeste",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(61, 128, 108)",
                },
                "label": "Litoral Suriname",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(123, 133, 33)",
                },
                "label": "Médio Araguaia ",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(156, 72, 119)",
                },
                "label": "Médio Araguaia II",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(76, 158, 131)",
                },
                "label": "Médio Madeira",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(128, 60, 61)",
                },
                "label": "Médio Orinoco",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(163, 94, 82)",
                },
                "label": "Médio Orinoco II",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(163, 41, 145)",
                },
                "label": "Médio Paraná",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(40, 153, 121)",
                },
                "label": "Rio Anajás-Cururu",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(40, 88, 156)",
                },
                "label": "Rio Apure",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(138, 43, 120)",
                },
                "label": "Rio Aramá-Anajás",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(88, 173, 176)",
                },
                "label": "Rio Bermejo",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(130, 104, 43)",
                },
                "label": "Rio Cajari",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(118, 67, 143)",
                },
                "label": "Rio Caroni",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(67, 145, 96)",
                },
                "label": "Rio Courantyne",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(78, 54, 128)",
                },
                "label": "Rio Cuyuni",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(39, 171, 61)",
                },
                "label": "Rio Doce",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(81, 133, 166)",
                },
                "label": "Rio Escuro-Formoso-Javés",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(77, 168, 35)",
                },
                "label": "Rio Ganhoão",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(100, 40, 173)",
                },
                "label": "Rio Guaviare",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(67, 163, 65)",
                },
                "label": "Rio Jari",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(70, 128, 59)",
                },
                "label": "Rio Madeira",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(127, 168, 79)",
                },
                "label": "Rio Maroni",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(32, 92, 128)",
                },
                "label": "Rio Mearim",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(173, 36, 123)",
                },
                "label": "Rio Meta",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(168, 84, 102)",
                },
                "label": "Rio Negro",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(173, 83, 50)",
                },
                "label": "Rio Oiapoque",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(166, 159, 38)",
                },
                "label": "Rio Paraguai",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(64, 87, 130)",
                },
                "label": "Rio Parnaíba",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(151, 61, 161)",
                },
                "label": "Rio Paru",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(94, 150, 51)",
                },
                "label": "Rio Pará-Pacajá",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(83, 98, 166)",
                },
                "label": "Rio Pilcomayo",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(166, 139, 41)",
                },
                "label": "Rio Rupununi",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(62, 64, 163)",
                },
                "label": "Rio Salado",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(176, 81, 162)",
                },
                "label": "Rio São Francisco",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(48, 128, 28)",
                },
                "label": "Rio Tapajós",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(168, 35, 64)",
                },
                "label": "Rio Tocantins",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(153, 92, 49)",
                },
                "label": "Rio Trombetas",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(76, 34, 138)",
                },
                "label": "Rio Uruguai",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(121, 138, 66)",
                },
                "label": "Rio Xingu",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(127, 173, 57)",
                },
                "label": "Rio da Prata",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(36, 64, 135)",
                },
                "label": "Rio das Mortes",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(152, 173, 45)",
                },
                "label": "Alto Araguaia",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(137, 44, 163)",
                },
                "label": "Alto Orinoco",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(169, 173, 81)",
                },
                "label": "Alto Paraná",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(42, 130, 64)",
                },
                "label": "Alto Rio Amazonas",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(173, 150, 87)",
                },
                "label": "Baixo Araguaia",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(49, 176, 108)",
                },
                "label": "Baixo Paraná",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(153, 44, 79)",
                },
                "label": "Baixo Rio Orinoco",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(47, 126, 130)",
                },
                "label": "Baixo Rio Orinoco II",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(39, 176, 169)",
                },
                "label": "Canal das Tartarugas",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(128, 37, 40)",
                },
                "label": "Foz Rio Cuyuni-Rupununi",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(114, 46, 140)",
                },
                "label": "Foz do Rio Jari",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(100, 75, 163)",
                },
                "label": "Foz do Rio Orinoco",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(45, 150, 166)",
                },
                "label": "Litoral Guiana",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(168, 39, 41)",
                },
                "label": "Litoral Ilha de Marajó",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(145, 36, 96)",
                },
                "label": "Litoral Ilha de Marajó II",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(176, 111, 46)",
                },
                "label": "Litoral Ilha de Marajó III",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(138, 75, 52)",
                },
                "label": "Litoral Leste",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(168, 64, 67)",
                },
                "label": "Litoral Nordeste Ocidental",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(150, 56, 44)",
                },
                "label": "Litoral Nordeste Ocidental II",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(143, 103, 70)",
                },
                "label": "Litoral Nordeste Ocidental III",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(80, 171, 92)",
                },
                "label": "Litoral Nordeste Oriental",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(37, 46, 168)",
                },
                "label": "Litoral Sudeste",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(61, 128, 108)",
                },
                "label": "Litoral Suriname",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(123, 133, 33)",
                },
                "label": "Médio Araguaia ",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(156, 72, 119)",
                },
                "label": "Médio Araguaia II",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(76, 158, 131)",
                },
                "label": "Médio Madeira",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(128, 60, 61)",
                },
                "label": "Médio Orinoco",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(163, 94, 82)",
                },
                "label": "Médio Orinoco II",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(163, 41, 145)",
                },
                "label": "Médio Paraná",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(40, 153, 121)",
                },
                "label": "Rio Anajás-Cururu",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(40, 88, 156)",
                },
                "label": "Rio Apure",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(138, 43, 120)",
                },
                "label": "Rio Aramá-Anajás",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(88, 173, 176)",
                },
                "label": "Rio Bermejo",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(130, 104, 43)",
                },
                "label": "Rio Cajari",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(118, 67, 143)",
                },
                "label": "Rio Caroni",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(67, 145, 96)",
                },
                "label": "Rio Courantyne",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(78, 54, 128)",
                },
                "label": "Rio Cuyuni",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(39, 171, 61)",
                },
                "label": "Rio Doce",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(81, 133, 166)",
                },
                "label": "Rio Escuro-Formoso-Javés",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(77, 168, 35)",
                },
                "label": "Rio Ganhoão",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(100, 40, 173)",
                },
                "label": "Rio Guaviare",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(67, 163, 65)",
                },
                "label": "Rio Jari",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(70, 128, 59)",
                },
                "label": "Rio Madeira",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(127, 168, 79)",
                },
                "label": "Rio Maroni",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(32, 92, 128)",
                },
                "label": "Rio Mearim",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(173, 36, 123)",
                },
                "label": "Rio Meta",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(168, 84, 102)",
                },
                "label": "Rio Negro",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(173, 83, 50)",
                },
                "label": "Rio Oiapoque",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(166, 159, 38)",
                },
                "label": "Rio Paraguai",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(64, 87, 130)",
                },
                "label": "Rio Parnaíba",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(151, 61, 161)",
                },
                "label": "Rio Paru",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(94, 150, 51)",
                },
                "label": "Rio Pará-Pacajá",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(83, 98, 166)",
                },
                "label": "Rio Pilcomayo",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(166, 139, 41)",
                },
                "label": "Rio Rupununi",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(62, 64, 163)",
                },
                "label": "Rio Salado",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(176, 81, 162)",
                },
                "label": "Rio São Francisco",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(48, 128, 28)",
                },
                "label": "Rio Tapajós",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(168, 35, 64)",
                },
                "label": "Rio Tocantins",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(153, 92, 49)",
                },
                "label": "Rio Trombetas",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(76, 34, 138)",
                },
                "label": "Rio Uruguai",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(121, 138, 66)",
                },
                "label": "Rio Xingu",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(127, 173, 57)",
                },
                "label": "Rio da Prata",
              },
              {
                style: {
                  "opacity"         : "1",
                  "width"           : "10px",
                  "height"          : "10px",
                  "background-color": "rgb(36, 64, 135)",
                },
                "label": "Rio das Mortes",
              },
            ],
          },
        },
      },
      queimadas: {
        groupName: 'fires',
        expanded: false,
        layers: {
          focos: {
            description: 'fire',
            layer: L.esri.Cluster.featureLayer({
              url:          self.methods.gisLayerUrl('focos'),
              proxy:        self.settings.proxyUrl,
              //token:        apiKey,
              attribution:  '<a href="http://www.inpe.br/">INPE</a>',
              // Using only the reference satelite
              // See http://www.inpe.br/queimadas/portal/informacoes/perguntas-frequentes
              where:        "satelite = 'AQUA_M-T'",
              polygonOptions: {
                color       : 'red',
                weight      : 4,
                opacity     : 1,
                fillOpacity : 0.5
              },
              spiderfyOnMaxZoom: false,
              disableClusteringAtZoom: 7,
              iconCreateFunction: function (cluster) {
                var childCount = cluster.getChildCount();
                var c          = ' marker-cluster-';

                if (childCount < 10) {
                  c += 'small';
                } else if (childCount < 100) {
                  c += 'medium';
                } else {
                  c += 'large';
                }

                return new L.DivIcon({ html: '<div><img src="' + self.settings.imagesUrl + '/misc/focos.png"> <span><strong>' + childCount + '</strong></span></div>', className: 'fire-cluster energy-cluster marker-cluster' + c, iconSize: new L.Point(40, 40) });
              },
              pointToLayer: function (geojson, latlng) {
                return L.marker(latlng, {
                  icon: L.icon({
                    iconUrl: self.settings.imagesUrl + '/misc/focos.png',
                    iconSize: [22, 23],
                  }),
                });
              },
            }),
            click: function(e) {
              var lang       = self.settings.lang;
              var attributes = e.layer.feature.properties;

              // Date is UTC unix timestamp with miliseconds
              // We have to convert it to human-readable date and sum up with the timezone offset
              //
              // See http://forums.esri.com/Thread.asp?c=158&f=2397&t=299440
              var zone    = new Date();
              var offset  = zone.getTimezoneOffset() * 60 * 1000;
              var date    = new Date(attributes.data + offset);
              var day     = date.getDate();
              var month   = date.getMonth() + 1;
              var year    = date.getFullYear();

              // Simple locale formatting
              if (lang == 'en') {
                var format = month + '/' + day + '/' + year;
              }
              else {
                var format = day + '/' + month + '/' + year;
              }

              var content = self.methods.htmlTable('fire', {
                'date'         : format,
                'coordinates'  : attributes.latgd + ' - ' + attributes.longgd,
                //'satelitte'  : attributes.satelite,
                //'vegetation' : attributes.vegetacao,
              });

              e.layer.bindPopup(content);
              e.layer.openPopup();
            },
            legend: [
              {
                //label: 'fire',
                //html: '<img src="'+ self.settings.imagesUrl + '/misc/focos.png" />',
                html: '',
                style: {
                  "opacity"          : "1",
                  "background-image" : "url('" + self.settings.imagesUrl + "/misc/focos.png')",
                  "width"            : "22px",
                  "height"           : "23px"
                },
              }
            ],
          },
          focosDensidade: {
            description: 'fire_density',
            layer: L.esri.Heat.featureLayer({
              url         : self.methods.gisLayerUrl('focos'),
              proxy       : self.settings.proxyUrl,
              //token       : apiKey,
              // Using only the reference satelite
              // See http://www.inpe.br/queimadas/portal/informacoes/perguntas-frequentes
              where       : "satelite = 'AQUA_M-T'",
              fields      : ['oid'],
              minOpacity  : 0.4,
              maxZoom     : 15,
              max         : 0.9,
              radius      : 20,
              blur        : 10,
              gradient    : {0.5: 'gold', 0.7: 'orange', 0.9: '#bd0026'},
              /*
              gradient    : {
                0.1: '#999900',
                0.3: '#997300',
                0.5: '#994d00',
                0.7: '#992600',
                0.9: '#990000',
              },
              */
              attribution : '<a href="http://www.inpe.br/">INPE</a>'
            }),
            legend: [
              {
                html: '',
                style: {
                  "background-color" : "#bd0026",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              }
            ],
          },
        },
      },
      desmatamento: {
        groupName: 'deforestation',
        expanded: false,
        // This will be autoset by the group ID during initialization
        //sliderId: 'desmatamento',
        slider: {
          type             : 'single',
          min              : 0,
          max              : 2017,
          from             : 0,
          to               : 0,
          grid             : true,
          keyboard         : true,
          prettify_enabled : true,
        },
        layers: {
          0: {
            description: '0000',
            layer: L.tileLayer(''),
            //layer: L.esri.dynamicMapLayer({
              disableCache: false,
            //  format: 'png32',
            //  url:     self.geo.services.gisServices.desmatamento,
            //  proxy:   self.settings.proxyUrl,
            //  //token:   apiKey,
            //  layers:  [ 9999 ],
            //  opacity: 0.45,
            //}),
          },
          1: {
            description: '2000',
            layer: L.esri.dynamicMapLayer({
              disableCache: false,
              format: 'png32',
              url:     self.geo.services.gisServices.desmatamento,
              proxy:   self.settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 0 ],
              opacity: 0.45,
            }),
            legend: [
              {
                label: 'deforestation_accumulated_until',
                args : { year: '2000' },
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#800000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
            ],
          },
          2: {
            description: '2001',
            layer: L.esri.dynamicMapLayer({
              disableCache: false,
              format: 'png32',
              url:     self.geo.services.gisServices.desmatamento,
              proxy:   self.settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 1 ],
              opacity: 0.45,
            }),
            legend: [
              {
                label: 'deforestation_accumulated_until',
                args : { year: '2001' },
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#800000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
            ],
          },
          3: {
            description: '2002',
            layer: L.esri.dynamicMapLayer({
              disableCache: false,
              format: 'png32',
              url:     self.geo.services.gisServices.desmatamento,
              proxy:   self.settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 2 ],
              opacity: 0.45,
            }),
            legend: [
              {
                label: 'deforestation_accumulated_until',
                args : { year: '2002' },
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#800000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
            ],
          },
          4: {
            description: '2003',
            layer: L.esri.dynamicMapLayer({
              disableCache: false,
              format: 'png32',
              url:     self.geo.services.gisServices.desmatamento,
              proxy:   self.settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 3 ],
              opacity: 0.45,
            }),
            legend: [
              {
                label: 'deforestation_accumulated_until',
                args : { year: '2003' },
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#800000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
            ],
          },
          5: {
            description: '2004',
            layer: L.esri.dynamicMapLayer({
              disableCache: false,
              format: 'png32',
              url:     self.geo.services.gisServices.desmatamento,
              proxy:   self.settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 4 ],
              opacity: 0.45,
            }),
            legend: [
              {
                label: 'deforestation_accumulated_until',
                args : { year: '2004' },
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#800000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
            ],
          },
          6: {
            description: '2005',
            layer: L.esri.dynamicMapLayer({
              disableCache: false,
              format: 'png32',
              url:     self.geo.services.gisServices.desmatamento,
              proxy:   self.settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 5 ],
              opacity: 0.45,
            }),
            legend: [
              {
                label: 'deforestation_accumulated_until',
                args : { year: '2005' },
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#800000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
            ],
          },
          7: {
            description: '2006',
            layer: L.esri.dynamicMapLayer({
              disableCache: false,
              format: 'png32',
              url:     self.geo.services.gisServices.desmatamento,
              proxy:   self.settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 6 ],
              opacity: 0.45,
            }),
            legend: [
              {
                label: 'deforestation_accumulated_until',
                args : { year: '2006' },
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#800000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
            ],
          },
          8: {
            description: '2007',
            layer: L.esri.dynamicMapLayer({
              disableCache: false,
              format: 'png32',
              url:     self.geo.services.gisServices.desmatamento,
              proxy:   self.settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 7 ],
              opacity: 0.45,
            }),
            legend: [
              {
                label: 'deforestation_accumulated_until',
                args : { year: '2007' },
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#800000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
            ],
          },
          9: {
            description: '2008',
            layer: L.esri.dynamicMapLayer({
              disableCache: false,
              format: 'png32',
              url:     self.geo.services.gisServices.desmatamento,
              proxy:   self.settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 8 ],
              opacity: 0.45,
            }),
            legend: [
              {
                label: 'deforestation_accumulated_until',
                args : { year: '2008' },
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#800000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
            ],
          },
          10: {
            description: '2009',
            layer: L.esri.dynamicMapLayer({
              disableCache: false,
              format: 'png32',
              url:     self.geo.services.gisServices.desmatamento,
              proxy:   self.settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 9 ],
              opacity: 0.45,
            }),
            legend: [
              {
                label: 'deforestation_accumulated_until',
                args : { year: '2009' },
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#800000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
            ],
          },
          11: {
            description: '2010',
            layer: L.esri.dynamicMapLayer({
              disableCache: false,
              format: 'png32',
              url:     self.geo.services.gisServices.desmatamento,
              proxy:   self.settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 10 ],
              opacity: 0.45,
            }),
            legend: [
              {
                label: 'deforestation_accumulated_until',
                args : { year: '2010' },
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#800000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
            ],
          },
          12: {
            description: '2011',
            layer: L.esri.dynamicMapLayer({
              disableCache: false,
              format: 'png32',
              url:     self.geo.services.gisServices.desmatamento,
              proxy:   self.settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 11 ],
              opacity: 0.45,
            }),
            legend: [
              {
                label: 'deforestation_accumulated_until',
                args : { year: '2011' },
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#800000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
            ],
          },
          13: {
            description: '2012',
            layer: L.esri.dynamicMapLayer({
              disableCache: false,
              format: 'png32',
              url:     self.geo.services.gisServices.desmatamento,
              proxy:   self.settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 12 ],
              opacity: 0.45,
            }),
            legend: [
              {
                label: 'deforestation_accumulated_until',
                args : { year: '2012' },
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#800000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
            ],
          },
          14: {
            description: '2013',
            layer: L.esri.dynamicMapLayer({
              disableCache: false,
              format: 'png32',
              url:     self.geo.services.gisServices.desmatamento,
              proxy:   self.settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 13 ],
              opacity: 0.45,
            }),
            legend: [
              {
                label: 'deforestation_accumulated_until',
                args : { year: '2013' },
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#800000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
            ],
          },
          15: {
            description: '2014',
            layer: L.esri.dynamicMapLayer({
              disableCache: false,
              format: 'png32',
              url:     self.geo.services.gisServices.desmatamento,
              proxy:   self.settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 14 ],
              opacity: 0.45,
            }),
            legend: [
              {
                label: 'deforestation_accumulated_until',
                args : { year: '2014' },
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#800000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
            ],
          },
          16: {
            description: '2015',
            layer: L.esri.dynamicMapLayer({
              disableCache: false,
              format: 'png32',
              url:     self.geo.services.gisServices.desmatamento,
              proxy:   self.settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 15 ],
              opacity: 0.45,
            }),
            legend: [
              {
                label: 'deforestation_accumulated_until',
                args : { year: '2015' },
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#800000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
            ],
          },
          17: {
            description: '2016',
            layer: L.esri.dynamicMapLayer({
              disableCache: false,
              format: 'png32',
              url:     self.geo.services.gisServices.desmatamento,
              proxy:   self.settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 16 ],
              opacity: 0.45,
            }),
            legend: [
              {
                label: 'deforestation_accumulated_until',
                args : { year: '2016' },
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#800000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
            ],
          },
          18: {
            description: '2017',
            layer: L.esri.dynamicMapLayer({
              disableCache: false,
              format: 'png32',
              url:     self.geo.services.gisServices.desmatamento,
              proxy:   self.settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 17 ],
              opacity: 0.45,
            }),
            legend: [
              {
                label: 'deforestation_accumulated_until',
                args : { year: '2017' },
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#800000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
            ],
          },
          19: {
            description: '2018',
            layer: L.esri.dynamicMapLayer({
              disableCache: false,
              format: 'png32',
              url:     self.geo.services.gisServices.desmatamento,
              proxy:   self.settings.proxyUrl,
              //token:   apiKey,
              layers:  [ 18 ],
              opacity: 0.45,
            }),
            legend: [
              {
                label: 'deforestation_accumulated_until',
                args : { year: '2018' },
                style: {
                  "opacity"          : "0.45",
                  "background-color" : "#800000",
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
            ],
          },
        },
      },
    },
  };
}
