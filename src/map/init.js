import { loaders } from './loaders.js';

var globals = require('./globals');

// Plugin initialization
export function init(baseUrl = '') {
  // Block UI
  jQuery('.isamap').block({
    message : '<span><img src="' + globals.defaultSettings.baseUrl + 'images/misc/loading.gif" /></span>',
    css     : globals.blockUIStyle,
  });

  // Load Metadata
  var loader = loaders();
  loader.loadMetadata(loader, baseUrl);
}
