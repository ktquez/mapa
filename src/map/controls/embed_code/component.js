//import L from '../../leaflet.js';

var Clipboard = require('clipboard');
var globals   = require('../../globals');

export default {
  data: function() {
    return {
      name     : 'embed-code',
      icon     : 'fa-share',
      state    : 'closed',
      collapsed: true,
      url      : '',
      code     : '',
      embed    : '',

      // Standard isaMap properties
      isamap : null,
      locale : null,
      map    : null,
      control: null,
      id     : null,
    }
  },
  mounted: function() {
    var self = this;

    this.stopPropagation();

    //if (this.clipboard == undefined && Clipboard.isSupported()) {
    if (Clipboard.isSupported()) {
      //this.clipboard = new Clipboard('.isamap-embed-control-clipboard');

      //this.clipboard.on('success', function(e) {
      //  self.isamap.methods.debug('Clipboard action ' + e.action + ', text ' + e.text + ', trigger ' + e.trigger);
      //  e.clearSelection();
      //});

      //this.clipboard.on('error', function(e) {
      //  self.isamap.methods.debug('Clipboard error ' + e.action + ', trigger ' + e.trigger);
      //});

      // Implementation via clipboard.js is not working as the click event is not being fired:
      // just try debugging 'listenClick'and 'onClick' keys from _createClass at Clipboard
      // variable from clipboard.js
      //
      // So here we use a small workaround.
      //
      // https://stackoverflow.com/questions/400212/how-do-i-copy-to-the-clipboard-in-javascript
      jQuery('.isamap-embed-control-clipboard').on('click', function() {
        var target       = jQuery(this).attr('data-clipboard-target');
        var copyTextarea = document.querySelector(target);

        copyTextarea.select();

        try {
          var successful = document.execCommand('copy');
          var msg        = successful ? 'successful' : 'unsuccessful';

          self.isamap.methods.debug('Copying text command was ' + msg);
        } catch (err) {
          self.isamap.methods.debug('Oops, unable to copy to clipboard');
        }
      });
    }

    // Twitter widget
    if (window.twttr == undefined) {
      window.twttr = (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0], t = window.twttr || {};
        if (d.getElementById(id)) return t;

        js     = d.createElement(s);
        js.id  = id;
        js.src = "https://platform.twitter.com/widgets.js";

        fjs.parentNode.insertBefore(js, fjs);

        t._e    = [];
        t.ready = function(f) {
          t._e.push(f);
        };

        return t;
      }(document, "script", "twitter-wjs"));
    }

    // Facebook SDK
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];

      if (d.getElementById(id)) return;

      js     = d.createElement(s); js.id = id;
      js.src = "https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.5";

      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
  },
  computed: {
    containerClass: function() {
      return 'leaflet-control leaflet-small-widget leaflet-bar leaflet-' + this.name + ' ' + this.state;
    },
    controlClass: function() {
      return 'leaflet-small-widget-toggle ' + this.state;
    },
    iconClass: function() {
      return 'fa ' + this.icon;
    },
    titleClass: function() {
      return this.collapsed == true ? 'legend-title closed' : 'legend-title';
    },
    twitterShareHref: function() {
      return 'https://twitter.com/intent/tweet?text=' + this.isamap.methods.message('share_text_title');
    },
    fbLikeId: function() {
      return this.isamap.settings.mapId + '-isamap-embed-code-fb';
    },
    embedUrlTextareaId: function() {
      return this.isamap.settings.mapId + '-isamap-embed-code-textarea-url';
    },
    embedUrlTextareaTarget: function() {
      return '#' + this.embedUrlTextareaId;
    },
    embedCodeTextareaId: function() {
      return this.isamap.settings.mapId + '-isamap-embed-code-textarea-code';
    },
    embedCodeTextareaTarget: function() {
      return '#' + this.embedCodeTextareaId;
    },
    embedUrlLinkId: function() {
      return this.isamap.settings.mapId + '-isamap-embed-code-link';
    },
  },
  methods: {
    stopPropagation: function() {
      L.DomEvent.disableScrollPropagation(this.$el);
      L.DomEvent.disableClickPropagation(this.$el);
      L.DomEvent.on(this.$el, 'wheel', L.DomEvent.stopPropagation);
      L.DomEvent.on(this.$el, 'click', L.DomEvent.stopPropagation);
    },
    toggleControl: function() {
      if (this.state == 'closed') {
        this.state = 'opened';
      }
      else {
        this.state = 'closed';
      }
    },
    toggleItem: function(event) {
      jQuery(event.target).parent().toggleClass('closed');
      jQuery(event.target).parent().siblings('.legend-elements').toggleClass('closed');
    },
    buildUrl: function() {
      return this.isamap.settings.baseUrl + 'v' +  globals.majorVersion + '/' + ((this.isamap.encodedSettings != '') ? '?' + this.isamap.encodedSettings : '');
    },
    buildCode: function() {
      return '<iframe src="' + decodeURIComponent(this.url) + '" height="300px" width="100%" allowfullscreen frameborder="0" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>';
    },
    buildOEmbed: function(format) {
      if (format == null) {
        var format = 'json';
      }

      var url = encodeURIComponent(this.url);

      return this.isamap.settings.baseUrl + 'oembed/?url=' + url + '&maxheight=300&maxwidth=500&format=' + format;
    },
    update: function() {
      this.url  = this.buildUrl();
      this.code = this.buildCode();

      // oEmbed support
      jQuery('#oembed-json').attr('href', this.buildOEmbed());
      jQuery('#oembed-xml').attr('href',  this.buildOEmbed('xml'));
    },
  },
}
