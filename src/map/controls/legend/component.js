//import L from '../../leaflet.js';

export default {
  data: function() {
    return {
      // UI
      name     : 'legend',
      icon     : 'fa-map-o',
      collapsed: true,

      // State
      state          : 'closed',
      items          : [ ],
      additionalItems: [ ],

      // Options
      options  : {
        localize         : true,
        collapseSimple   : true,
        detectStretched  : true,
        collapsedOnInit  : true,
        collapsed        : true,
        position         : 'topright',
        visibleIcon      : 'icon icon-eye',
        hiddenIcon       : 'icon icon-eye-slash',
        //defaultOpacity : 0.5,
      },

      // Standard isaMap properties
      isamap : null,
      locale : null,
      map    : null,
      control: null,
      id     : null,
    }
  },
  mounted: function() {
    this.stopPropagation();
    this.localeTransition();
    this.build();
  },
  computed: {
    containerClass: function() {
      return 'leaflet-control leaflet-small-widget leaflet-bar leaflet-' + this.name + ' ' + this.state;
    },
    controlClass: function() {
      return 'leaflet-small-widget-toggle ' + this.state;
    },
    iconClass: function() {
      return 'fa ' + this.icon;
    },
    titleClass: function() {
      return this.collapsed == true ? 'legend-title closed' : 'legend-title';
    },
  },
  methods: {
    itemFromMapServer: function(layer, name, url) {
      var self = this;

      // Fetch data
      jQuery.ajax({
        url      : url + '/legend',
        data     : { f: 'pjson' },
        dataType : 'jsonp',
      }).then(function(response) {
        if (response.drawingInfo               != undefined &&
          response.drawingInfo.renderer        != undefined &&
          response.drawingInfo.renderer.symbol != undefined) {

          var item = {
            //name: name + ': ' + response.name,
            //name: response.name,
            name  : name,
            layer : layer,
          }

          if (response.drawingInfo.renderer.symbol.imageData != undefined) {
            item.legend = [
              {
                // Thanks https://github.com/w8r/esri-leaflet-legend
                html: L.Util.template(
                  '<div class="legend-row"><img src="data:{contentType};base64,{imageData}"></div>',
                  response.drawingInfo.renderer.symbol
                ),
                //label: name + ': ' + response.name,
                //label: response.name,
                label  : name,
                style  : {
                  "opacity" : "1",
                  "width"   : "40px",
                  "height"  : "40px",
                },
              },
            ];
          }
          else if (response.drawingInfo.renderer.symbol.color != undefined) {
            var color   = response.drawingInfo.renderer.symbol.color;
            item.legend = [
              {
                html  : '',
                //label: name + ': ' + response.name,
                //label: response.name,
                label : name,
                style : {
                  "opacity"          : color[3] / 255,
                  "background-color" : 'rgb(' + color[0] + ', ' + color[1] + ', ' + color[2] + ')',
                  "border"           : "1px solid #000000",
                  "width"            : "10px",
                  "height"           : "10px",
                },
              },
            ];
          }

          //self.$set(self.items, self.items.lenght + 1, {
          self.items.push(item);
        }
      });
    },
    addItems: function(items) {
      this.additionalItems = this.additionalItems.concat(items);
      this.build()
    },
    build: function() {
      //var items = [ ];
      this.items  = [ ];
      var self    = this;

      for (var group in self.isamap.layers) {
        for (var layer in self.isamap.layers[group].layers) {
          if (self.isamap.layers[group].layers[layer].layer != undefined) {

            // Build legend using server info
            if (self.isamap.layers[group].layers[layer].legendFromMapServer == true) {
              if (self.isamap.layers[group].layers[layer].layer.options.url != undefined) {
                var baseUrl      = self.isamap.layers[group].layers[layer].layer.options.url;
                var currentLayer = self.isamap.layers[group].layers[layer].layer;
                var name         = self.isamap.layers[group].layers[layer].description;

                if (self.isamap.layers[group].layers[layer].layer.options.layers != undefined) {
                  // Build legend definition for each layer
                  for (var sublayer in self.isamap.layers[group].layers[layer].layer.options.layers) {
                    var url = baseUrl + self.isamap.layers[group].layers[layer].layer.options.layers[sublayer];

                    self.itemFromMapServer(currentLayer, name, url);
                  }
                }
                // Fetch only once using L.esri API
                // Alternative approach: use self.isamap.layers[group].layers[layer].layer.metadata()
                else {
                  self.itemFromMapServer(currentLayer, name, baseUrl);
                }
              }
            }
            else if (self.isamap.layers[group].layers[layer].legend != undefined) {
              //items.push({
              self.items.push({
                name  : self.isamap.layers[group].layers[layer].description,
                layer : self.isamap.layers[group].layers[layer].layer,
                legend: self.isamap.layers[group].layers[layer].legend,
              });
            }
          }
        }
      }

      if (self.isamap.settings.additionalLegend.length > 0) {
        self.items = self.items.concat(self.isamap.settings.additionalLegend)
      }

      if (self.additionalItems.length > 0) {
        self.items = self.items.concat(self.additionalItems)
      }

      //self.items = items;
      //return items;
    },
    stopPropagation: function() {
      L.DomEvent.disableScrollPropagation(this.$el);
      L.DomEvent.disableClickPropagation(this.$el);
      L.DomEvent.on(this.$el, 'wheel', L.DomEvent.stopPropagation);
      L.DomEvent.on(this.$el, 'click', L.DomEvent.stopPropagation);
    },
    localeTransition() {
      var self = this;

      self.isamap.bus.$on('locale-transition', function(value) {
        self.locale = value;
      });
    },
    toggleControl: function() {
      if (this.state == 'closed') {
        this.state = 'opened';
      }
      else {
        this.state = 'closed';
      }
    },
    toggleItem: function(event) {
      jQuery(event.target).parent().toggleClass('closed');
      jQuery(event.target).parent().siblings('.legend-elements').toggleClass('closed');
    },
  },
}
