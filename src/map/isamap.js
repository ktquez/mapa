import Vue     from 'vue';
import VueI18n from 'vue-i18n';
Vue.use(VueI18n);

import { messages                } from './messages.js';
import { utils                   } from './isamap/utils.js';
import { ui                      } from './isamap/ui.js';
import { map                     } from './isamap/map.js';
import { controls                } from './isamap/controls.js';
import { state                   } from './isamap/state.js';
import { popup                   } from './isamap/popup.js';
import { bounds                  } from './isamap/bounds.js';
import { init                    } from './isamap/init.js';
import { layers                  } from './isamap/layers.js';
import { arps                    } from './isamap/arps.js';
import { geocode                 } from './isamap/geocode.js';
import { services as geoServices } from './isamap/geo/services.js';
import { layers   as geoLayers   } from './isamap/geo/layers.js';
import L                           from './leaflet.js';

import '../css/isamap.css';
import '../less/slider.less';
import 'font-awesome/css/font-awesome.css';

export function isaMap(settings) {
  // Self reference
  var self      = this;
  self.methods  = {};
  self.geo      = {};
  self.settings = settings;

  // Ensure we have window.L
  if (!window.L) {
    window.L = L;
  }

  // Instance variables
  self.embedControl        = null;
  self.searchControl       = null;
  self.layers              = null;
  self.encodedSettings     = '';
  self.map                 = {};
  self.controls            = {};
  self.initialSettings     = jQuery.extend(true, {}, settings);
  self.mapCenter           = settings.center;
  self.mapZoom             = settings.zoom;
  self.minZoom             = settings.minZoom;
  self.maxZoom             = settings.maxZoom;
  self.maxBounds           = L.latLngBounds(settings.maxBounds);
  self.blockUI             = [];
  self.statusMessages      = [];
  //self.activeMapEntities = [];
  //self.mainLayerIds      = {};
  self.trackingEvents      = {};

  // Intercommunication bus
  self.bus                 = new Vue();

  // Internationalization
  if (self.settings.customMessages == null) {
    self.messages = messages;
  }
  else {
    self.messages = {
      'en': {
        ...messages.en,
        ...self.settings.customMessages.en,
      },
      'pt-br': {
        ...messages['pt-br'],
        ...self.settings.customMessages['pt-br'],
      },
      'es': {
        ...messages.es,
        ...self.settings.customMessages.es,
      },
    }
  }

  self.i18n = new VueI18n({
    locale  : self.settings.lang,
    messages: self.messages,
  });

  // Load submodules
  var modules = {
    utils    : utils,
    ui       : ui,
    map      : map,
    controls : controls,
    state    : state,
    popup    : popup,
    bounds   : bounds,
    init     : init,
    layers   : layers,
    arps     : arps,
    geocode  : geocode,
  }

  // Process submodules
  for (var module in modules) {
    // Initialize each submodule with the instance object
    var methods = modules[module](self);

    // Include each submodule into the methods object
    for (var method in methods) {
      self.methods[method] = methods[method];
    }
  }

  // Load and process services and layers configuration
  self.geo.services = geoServices(self);
  self.geo.layers   = geoLayers(self);

  return self.methods;
}
