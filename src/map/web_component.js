import Vue               from 'vue';
import vueCustomElement  from 'vue-custom-element'
import * as mapHandler   from './instance.js';
import { varType       } from '../common/types.js';

var globals = require('./globals');

// Register plugin
// See https://github.com/karol-f/vue-custom-element
//     https://karol-f.github.io/vue-custom-element/
//     https://www.npmjs.com/package/vue-custom-element
Vue.use(vueCustomElement);

// Setup component
var vm = {
  props: {
    height: {
      type   : String,
      default: '400px',
    },
    width : {
      type   : String,
      default: '400px',
    },
  },
  data: function() {
    return {
      isamap : null,
      params : {},
    }
  },
  template: '<div class="isamap" v-bind:style="getStyle"></div>',
  mounted: function() {
    this.setParams();
    this.initMap();
  },
  methods: {
    setParams: function() {
      // Cast params passed to the component
      // Make a copy instead of referencing the instance properties
      // See https://github.com/karol-f/vue-custom-element/issues/124
      //this.params = this._props;
      this.params = jQuery.extend(true, {}, this._props);

      // Some properties might need conversion
      for (var prop in this.params) {
        if (varType(globals.defaultSettings[prop]) == Array && varType(this.params[prop]) != Array) {
          this.params[prop] = JSON.parse(this.params[prop]);
        }
      }

      // Some need to be converted manually
      if (this.params.arps != null) {
        this.params.arps = JSON.parse(this.params.arps);
      }
    },
    initMap: function() {
      var self = this;

      mapHandler.load('https://mapa.eco.br');

      jQuery(document).on('isaMapLoaded', function() {
        self.isamap = mapHandler.add(self.$el, self.params);
      });
    },
  },
  computed: {
    getStyle: function() {
      return 'height: ' + this.height + '; width: ' + this.width + '; margin: 0; padding: 0; overflow: hidden;';
    },
  },
}

// Register properties with their type and default values
for (var prop in globals.defaultSettings) {
  // Handle null values as strings by default
  if (globals.defaultSettings[prop] == null) {
    vm.props[prop] = {
      default: globals.defaultSettings[prop],
      type   : String,
    };
  }
  // Arrays and objects need to set their default values as functions
  // https://vuejs.org/v2/guide/components-props.html
  else if (Array.isArray(globals.defaultSettings[prop])) {
    vm.props[prop] = {
      // A closure here preserves the current element
      default: (function() {
        var item = prop;

        return function() {
          return globals.defaultSettings[item];
        }
      })(),
      type: Array,
    };
  }
  else if (typeof(globals.defaultSettings[prop]) == 'object') {
    vm.props[prop] = {
      // A closure here preserves the current element
      default: (function() {
        var item = prop;

        return function() {
          return globals.defaultSettings[item];
        }
      })(),
      type: Object,
    };
  }
  else {
    vm.props[prop] = {
      default: globals.defaultSettings[prop],
      type   : varType(globals.defaultSettings[prop]),
    };
  }
}

// Initialize component
Vue.customElement('mapa-eco', vm, {
  shadow: false,
});
