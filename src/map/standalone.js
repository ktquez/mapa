import Vue from 'vue';
import * as mapHandler                 from './instance.js';
import { getQueryString, getFragment } from '../common/url_query.js';

import '../css/map.css';

// Wrapper
var map = new Vue({
  el     : '#map',
  data   : {
    isamap: null,
  },
  mounted: function() {
    var self = this;

    mapHandler.load();

    jQuery(document).on('isaMapLoaded', function() {
      var params   = getQueryString();
      var fragment = getFragment();

      // Convert to new format
      if (jQuery.isEmptyObject(params) && !jQuery.isEmptyObject(fragment)) {
        params               = fragment;
        window.location.hash = '';
      }

      self.isamap = mapHandler.add(self.$el, params);
    });
  },
});
