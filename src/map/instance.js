import { v4 as uuidv4 } from 'uuid';
import { messages     } from './messages.js';
import { isaMap       } from './isamap.js';
import { init         } from './init.js';

var globals = require('./globals');

export function setId(element) {
  var id = jQuery(element).attr('id');

  // Check and set ID if needed
  if (id == undefined || id == null) {
    // See https://gist.github.com/gordonbrander/2230317
    // https://github.com/makeable/uuid-v4.js
    // https://www.npmjs.com/package/uuid
    //current = 'isamap_' + (new Date().getUTCMilliseconds().toString() + new Date().getTime().toString()).toString();
    id = 'isamap_' + uuidv4();

    jQuery(element).attr('id', id);
  }

  return id;
}

export function add(element, options) {
  var id                  = setId(element);
  options.overlaySelector = '#' + id;
  globals.instances[id]   = {};

  // Do a shallow and not a recursive copy, otherwise array parameters would be merged
  //globals.instances[id].settings = jQuery.extend(true, {}, globals.defaultSettings, options);
  globals.instances[id].settings = jQuery.extend({}, globals.defaultSettings, options);

  // Additional defaults
  if (globals.instances[id].settings.overlayMessage == undefined) {
    globals.instances[id].settings.overlayMessage  = '<span><img class="jquery-isamap-mini-loading" src="' + globals.instances[id].settings.imagesUrl + '/misc/mini-loading.gif" /> ';
    globals.instances[id].settings.overlayMessage += messages[globals.instances[id].settings.lang].loading + "</span>";
  }

  // Setup isaMap instance
  globals.instances[id].settings.mapId = id;
  globals.instances[id].methods        = new isaMap(globals.instances[id].settings);
  globals.instances[id].methods.initialize();

  return globals.instances[id].methods.getSelf();
}

export function get(element) {
  var id = setId(element);

  return globals.instances[id].methods.getSelf();
}

export function load(baseUrl = '') {
  init(baseUrl);
}
