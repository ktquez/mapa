# Mapa Eco BR - A Portable Socioenvironmental Map!

* [Live Instance](https://mapa.eco.br).
* [Examples](https://mapa.socioambiental.org/pages/examples).
* [About](https://mapa.socioambiental.org/about).

## Usage

* Iframe.
* Standalone.
* jQuery Plugin: [code](https://gitlab.com/socioambiental/mapa/-/blob/master/pages/examples/jquery/index.html),    [demo](https://mapa.eco.br/pages/examples/jquery/).
* WebComponent:  [code](https://gitlab.com/socioambiental/mapa/-/blob/master/pages/examples/component/index.html), [demo](https://mapa.eco.br/pages/examples/component/).

API
---

* Check `self.methods.*` at `jquery.isamap.js` for a full list of callable methods.
* Check available settings using

    jQuery('#map').isaMap('dumpSettings');

## References

Plugins
-------

Some widgets that can be used along with this plugin:

* https://github.com/perliedman/leaflet-control-geocoder
* https://github.com/stefanocudini/leaflet-search
* https://github.com/8to5Developer/leaflet-custom-searchbox
* https://github.com/digidem/leaflet-side-by-side
* https://yigityuce.github.io/Leaflet.Control.Custom/
* https://github.com/makinacorpus/Leaflet.FileLayer
* https://github.com/Leaflet/Leaflet.draw
* https://github.com/jtreml/leaflet.measure
* https://github.com/makinacorpus/Leaflet.MeasureControl
* https://github.com/MazeMap/Leaflet.TileLayer.PouchDBCached
* https://github.com/tinuzz/leaflet-messagebox
* https://cdnjs.com/libraries/leaflet-plugins
* https://www.mapbox.com/mapbox.js/example/v1.0.0/leaflet-minimap/
* https://github.com/Leaflet/Leaflet.markercluster
* https://github.com/Leaflet/Leaflet.heat
* https://github.com/pointhi/leaflet-color-markers
* https://github.com/jseppi/Leaflet.MakiMarkers
* https://github.com/hiasinho/Leaflet.vector-markers

Projections
-----------

* https://kartena.github.io/Proj4Leaflet/
* http://proj4js.org/

Behavior
--------

Cross-iframe communication (using hash change or postMessage):

* http://mattsnider.com/hash-hack-for-cross-domain-iframe-communication/
* https://stackoverflow.com/questions/4122422/cross-domain-hash-change-communication
* https://stackoverflow.com/questions/999010/why-cant-an-iframe-set-its-parents-location-hash
* http://blog.apps.npr.org/pym.js/

Webcomponents:

* https://www.webcomponents.org/
* https://developer.mozilla.org/en-US/docs/Web/Web_Components
* https://css-tricks.com/modular-future-web-components/
* https://en.wikipedia.org/wiki/Web_Components
* https://www.w3.org/TR/components-intro/
* https://www.w3.org/wiki/WebComponents/

Responsiveness:

* https://npr.github.io/responsiveiframe/
* https://benmarshall.me/responsive-iframes/
* https://github.com/jcharlesworthuk/reactive.iframes
* https://www.smashingmagazine.com/2014/02/making-embedded-content-work-in-responsive-design/

Misc
----

* [The Power of Maps - El Poder del los Mapas - YouTube](https://www.youtube.com/watch?time_continue=2&v=TIVDPP9IJPo)

Credits
-------

* Newer versions use icons from [Ajaxload](http://ajaxload.info/).

* Previous versions used loading icon from [Preloaders.net](https://preloaders.net/),
  which allows free-of-charge use for non-commercial projects,
  see [terms](https://preloaders.net/en/terms_of_use).
